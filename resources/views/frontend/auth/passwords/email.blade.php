@extends('layouts.app')

<!-- Main Content -->
@section('content')
  <div class="mid-scn-main">
    @if(Request::path() == "checkout")
      <div class="website-bnr-inner">
          <div class="container">
              <div class="bnr-inner-cont">
                  <h2>Checkout</h2>
              </div>
          </div>
      </div>
    @endif
      <section class="checkoutsignin-scn register-form-scn">
          <div class="container">
              <div class="register-form-main">
                  <h3>Reset Password</h3>
                  <form method="POST" role="form" action="{{ url('/password/email') }}">
                      {{ csrf_field() }}
                      <div class="form-group">
                          @include('frontend.includes.errors')
                          @if (session('status'))
                              <div class="alert alert-success">
                                  {{ session('status') }}
                              </div>
                          @endif
                      </div>
                      <div class="form-group">
                          <label class="inputlabel">E-Mail Address</label>
                          <input type="text" name="email" class="form-control" placeholder="E-Mail Address" onfocus="this.placeholder=''" value="{{ old('email') }}" onblur="this.placeholder='E-Mail Address'">
                      </div>
                      <div class="rgister-login-btn clear">
                          <button type="submit" class="cmn-btn clear">Send Password Reset Link</button>
                      </div>
                  </form>
              </div>
          </div>
      </section>
  </div>
{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
