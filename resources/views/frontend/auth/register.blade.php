@extends('layouts.app')

@section('content')

<div class="mid-scn-main">
  @if(Request::path() == "checkout")
    <div class="website-bnr-inner">
        <div class="container">
            <div class="bnr-inner-cont">
                <h2>Checkout</h2>
            </div>
        </div>
    </div>
  @endif
    <section class="checkoutsignin-scn register-form-scn">
        <div class="container">
            <div class="register-form-main">
                <h3>Register</h3>
                <form method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        @include('frontend.includes.errors')
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">First Name *</label>
                        <input type="text" name="firstname" class="form-control" placeholder="Your First Name" onfocus="this.placeholder=''" value="{{ old('firstname') }}" onblur="this.placeholder='Your email address'">
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Last Name *</label>
                        <input type="text" name="lastname" class="form-control" placeholder="Your last name" onfocus="this.placeholder=''" value="{{ old('lastname') }}" onblur="this.placeholder='Your last name'">
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Business Name</label>
                        <input type="text" name="businessname" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" value="{{ old('businessname') }}" >
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Website URL(if relevant)</label>
                        <input type="url" name="website_url" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" value="{{ old('website_url') }}" >
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Email Address *</label>
                        <input type="email" name="email" class="form-control" placeholder="Your email address" onfocus="this.placeholder=''" onblur="this.placeholder='Your email address'" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Password *</label>
                        <input type="password" name="password" class="form-control" placeholder="Your password" onfocus="this.placeholder=''" onblur="this.placeholder='Your password'">
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Retype Password *</label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Your password" onfocus="this.placeholder=''" onblur="this.placeholder='Your password'">
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Mobile Number</label>
                        <input type="text" name="mobilenumber" class="form-control" placeholder="Your Mobile Number" onfocus="this.placeholder=''" onblur="this.placeholder='Your Mobile Number'" value="{{ old('mobilenumber') }}">
                    </div>
                    <div class="form-group rgtr-birthday-group">
                        <label class="inputlabel">Birthday </label>
                        <div class="default-select-box month-select-box">
                            <select name="month">
                                <option value="">Month</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="default-select-box">
                            <select name="day">
                                <option value="">Day</option>
                                @for ($j=01; $j <= 31; $j++)
                                  <option value="{{$j}}">{{$j}}</option>
                                @endfor
                                {{-- <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option> --}}
                            </select>
                        </div>
                        <div class="default-select-box">
                            <select name="year">
                                <option value="">Year</option>
                                @for ($i=1970; $i < 2000; $i++)
                                  <option value="{{$i}}">{{$i}}</option>
                                @endfor
                                {{-- <option value="2001">2001</option>
                                <option value="2002">2002</option>
                                <option value="2003">2003</option>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option> --}}
                            </select>
                        </div>
                    </div>
                    <div class="register-checkbox">
                        <div class="custom_checkbox">
                            <input type="checkbox" id="registerSign" name="newsletter">
                            <label for="registerSign">Sign up for our Newsletters to be the first to know about our great deals</label>
                        </div>
                        <div class="custom_checkbox">
                            <input type="checkbox" id="receiveSpecial" name="sms">
                            <label for="receiveSpecial">Receive special offers from us via SMS</label>
                        </div>
                    </div>
                    <div class="rgister-nowclick-txt">By clicking on “Register now” you agree to Incubeta’s <a href="#" id="rgterWelcome">Terms and Conditions</a></div>
                    <div class="rgister-login-btn clear">
                        <button type="submit" class="cmn-btn clear">Register now</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection
