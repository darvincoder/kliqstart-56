@extends('layouts.app')

@section('content')
  @if(Request::path() == "checkout")
    <div class="website-bnr-inner">
        <div class="container">
            <div class="bnr-inner-cont">
                <h2>Checkout</h2>
                <p>Target customers with the right message at the right time.</p>
            </div>
        </div>
    </div>
  @endif
    <section class="checkoutsignin-scn">
        <div class="container">
            <div class="checkoutsignin-dtl">
                <div class="checkoutsignin-lft fl-left">
                    <h3>Sign In</h3><span class="fl-right verified-secured"><img src="images/verified-secured.png" alt=""></span>
                    <form method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="inputlabel">Enter Your Email Address</label>
                            <input type="email" class="form-control" placeholder="Your email address" onfocus="this.placeholder=''" onblur="this.placeholder='Your email address'" name="email" value="{{ old('email') }}" required>
                        </div>
                        <div class="form-group">
                            <label class="inputlabel">Enter your Password</label>
                            <input type="password" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" name="password" required>
                        </div>
                        <div class="forget-reset">Forgot your Password? <a href="{{ url('/password/reset') }}">Click Here to Reset</a></div>
                        <div class="rgister-login-btn clear">
                            <button type="submit" class="cmn-btn clear">login</button>
                        </div>
                        <div class="form-group">
                          @include('frontend.includes.errors')
                        </div>
                    </form>
                </div>
                <div class="checkoutsignin-rft fl-right">
                    <h3>Don’t have an <br>account with us?</h3>
                    <a href="{{ url('/register') }}" class="checkout-register">Register</a>
                </div>
            </div>
        </div>
    </section>
@endsection
