@extends('layouts.app')

@section('content')
<div class="mid-scn-main">
	<section class="my-account-mid">
		<div class="container">
			<h2>My sites</h2>
			<div class="my-account-dtl">
				<div class="lifeofusher-prt">
					<h3>Lifeofusher.co.za <span class="lifeofusher-arrow-down"></span></h3>
					<div class="my-account-pic"><img src="images/my-account-pic.jpg" alt=""></div>
					<div class="accrount-dtl-info">
						<ul>
							<li>
								<div class="acc-info-lft"><strong>Domain:</strong> www.lifeofusher.co.za</div>
								<div class="acc-info-right"><a href="#">Manage</a></div>
							</li>
							<li>
								<div class="acc-info-lft"><strong>Plan:</strong> Website Package 1</div>
								<div class="acc-info-right"><a href="#">Billing & Payments</a><a href="#">Manage</a></div>
							</li>
							<li>
								<div class="acc-info-lft"><strong>Mailboxes:</strong> 2 Mailboxes Added</div>
								<div class="acc-info-right"><a href="#">Manage</a></div>
							</li>
						</ul>
					</div>
					<div class="acc-tips-update">
						<h3>Tips and Updates</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<a href="#" class="btn-defualt findoutmore">Find Out More</a>
					</div>
				</div>
				<div class="addextra-prt">
					<h3>Add Extra to Your Site</h3>
					<div class="what-k-list">
						<div class="what-k-item">
							<div class="what-k-icon">
								<i><img src="images/account-email-icon.png" alt=""></i>
							</div>
							<h3>Email Marketing</h3>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
							<a href="#" class="discover-more">Add to Site</a>
						</div>
						<div class="what-k-item">
							<div class="what-k-icon">
								<i><img src="images/seo-icon.png" alt=""></i>
							</div>
							<h3>Get Found</h3>
							<p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
							<a href="#" class="discover-more">Discover more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection