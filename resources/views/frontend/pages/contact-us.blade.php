@extends('layouts.app')

@section('content')
<!--Banner Section Start Here-->
<div class="banner-main banner-contactus" style="background-image: url('images/contact-hero.jpg')">
	<div class="demand-form-bnr">
		<h3>Get in touch with our super friendly team</h3>
		<p>We don’t need coffee or cupcakes to want to speak with you.
			<br>For assistance or just a quick chat, we’re happy to help.</p>
	</div>
	<div class="about-bnr-social">
		<a href="#" class="about-fb"></a>
		<a href="#" class="about-gp"></a>
		<a href="#" class="about-tw"></a>
		<a href="#" class="about-yt"></a>
		<a href="#" class="about-be"></a>
	</div>
</div>
<!--Middle Section Start Here-->
<div class="mid-scn-main">
	<section class="reach-touch-scn">
		<div class="container">
			<h2>Reach out and touch us</h2>
			<ul class="reach-touch-contact">
				<li>
					<span class="reach-touch-icon cont-phone-icn"></span>
					<a href="tel:+27 11 628-9700">+27 11 628-9700</a>
				</li>
				<li>
					<span class="reach-touch-icon cont-eamil-icn"></span>
					<a href="mailto:hello@kliqstart.co.za">hello@kliqstart.co.za</a>
				</li>
				<li>
					<span class="reach-touch-icon cont-message-icn"></span>
					<a href="tel:+27 11 628-9700">+27 11 628-9700</a>
				</li>
			</ul>
			<div class="contact-form-dtl">
				<h2>Leave us a message</h2>
				<form action="">
					<div class="form-group form-group-two">
						<input type="text" class="form-control" placeholder="Name" onfocus="this.placeholder=''" onblur="this.placeholder='Name'">
					</div>
					<div class="form-group form-group-two">
						<input type="text" class="form-control" placeholder="Surname" onfocus="this.placeholder=''" onblur="this.placeholder='Surname'">
					</div>
					<div class="form-group">
						<input type="email" class="form-control" placeholder="Email Address" onfocus="this.placeholder=''" onblur="this.placeholder='Email Address'">
					</div>
					<div class="form-group">
						<textarea class="form-control f-control-message" placeholder="Message" onfocus="this.placeholder=''" onblur="this.placeholder='Message'"></textarea>
					</div>
					<div class="form-group contcheck-group">
						<div class="custom_checkbox">
							<input type="checkbox" id="contcheck" name="package1">
							<label for="contcheck">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</label>
						</div>
					</div>
					<button type="submit" class="lets-go-btn send-message-btn">Send Message</button>
				</form>
			</div>
		</div>
	</section>
</div>
@endsection