@extends('layouts.app')

@section('content')
<!--Banner Section Start Here-->
<div class="banner-main banner-about" style="background-image: url('images/who-uses-hero.jpg')">
	<div class="demand-form-bnr">
		<h3>The perfect way to KliqStart <br> your next business idea.</h3>
		<p>Speed to market is often the big difference between success and failure. We created this platform to help you get up and running, faster.</p>
	</div>
	<div class="about-bnr-social">
		<a href="#" class="about-fb"></a>
		<a href="#" class="about-gp"></a>
		<a href="#" class="about-tw"></a>
		<a href="#" class="about-yt"></a>
		<a href="#" class="about-be"></a>
	</div>
</div>
<!--Middle Section Start Here-->
<div class="mid-scn-main">
	<section class="philosophy-business-scn">
		<div class="container">
			<h2>What do you want to do today?</h2>
			<ul class="philosophy-list">
				<li>Build a new Website</li>
				<li>Improve my SEO</li>
				<li>Promote with Google</li>
				<li>Connect on Facebook</li>
			</ul>
		</div>
	</section>
	<section class="be-inspired-scn build-who-user" style="background-image: url('images/build-who-user.jpg')">
		<div class="container">
			<div class="be-inspired-cont fl-right">
				<h2>Build a <br> new website</h2>
				<p>Every creative idea needs a worthy platform from where it can be sold to the world. Use KliqStart to get a great-looking website that does justice to your talents. It’s simple, quick and cost-effective. Just Kliq.</p>
				<a href="#" class="discover-more itstart-here-btn">It starts here</a>
			</div>
		</div>
	</section>
	<section class="what-doget-scn">
		<div class="container">
			<div class="what-doget-dtl">
				<h2>What do you get?</h2>
				<p>You get access to a variety of WordPress website templates that are easy to setup and manage – giving you
					<br> a platform from where you can attract, engage and convert more customers.</p>
				<ul class="what-doget-list">
					<li>Your own domain</li>
					<li>12 months hosting</li>
					<li>25 email addresses</li>
					<li>Studio time for custom work (package dependent)</li>
				</ul>
				<a href="#" class="starthire-btn">View all packages</a>
			</div>
		</div>
	</section>
	<section class="we-got-sorted">
		<div class="container">
			<div class="got-sorted-info">
				<h2>We’ve got you, sorted.</h2>
				<p>From hosting to content creation, web design, Google and Social Media, KliqStart puts you in the driving seat. You make the call, we make it happen, together be build great things.</p>
				<strong>What do you want to do today?</strong>
			</div>
			<ul class="got-sorted-social">
				<li><span class="got-sorted-icn"><img src="images/build-new-website-icon.png" alt="" /></span>
					<h4>Build a new Website</h4>
				</li>
				<li><span class="got-sorted-icn"><img src="images/improve-my-seo.png" alt="" /></span>
					<h4>Improve my SEO</h4>
				</li>
				<li><span class="got-sorted-icn"><img src="images/promote-google.png" alt="" /></span>
					<h4>Promote with Google</h4>
				</li>
				<li><span class="got-sorted-icn"><img src="images/connect-facebook.png" alt="" /></span>
					<h4>Connect on Facebook</h4>
				</li>
			</ul>
		</div>
	</section>
</div>
@endsection