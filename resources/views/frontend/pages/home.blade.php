@extends('layouts.app')

@section('content')
<!--Banner Section Start Here-->
<div class="banner-main" style="background-image: url('images/hero-image.jpg')">
	<div class="demand-form-bnr">
		<h3>On-demand digital for in-demand people</h3>
		<p>Create websites, run campaigns, build great brands. KliqStart is online marketing made easy</p>
		<form class="build-website-form" name="" id="build-website-form" method="POST" action="{{ url('wantto') }}">
			{!! csrf_field() !!}
			<label>I want to</label>
			<div class="form-group">
				<select class="form-control buildW-control input-arrpw" name="want_to" id="want-to">
					<option value="{{ url('/purchase-a-website') }}" selected=selected>Build a new website for my business...</option>
					<option value="{{ url('/seo') }}">SEO</option>
					<option value="{{ url('/google-adwords') }}">Advertise on Google</option>
					<option value="{{ url('/facebook') }}">Reach out on social media</option>
				</select>
			</div>
			<button type="submit" class="lets-go-btn">Let’s go!</button>
		</form>
	</div>
</div>
<!--Middle Section Start Here-->
<div class="mid-scn-main">
	<section class="what-kliqstart-scn">
		<div class="container">
			<h2>What you can do with KliqStart</h2>
			<div class="what-k-list">
				<div class="what-k-item">
					<div class="what-k-icon">
						<i><img src="images/wordpress-icon.png" alt=""></i>
					</div>
					<h3>Say Hello</h3>
					<h4>Create your own website</h4>
					<p>Choose a modern and mobile responsive template to create a website that is quick, easy, affordable and suited to your specific business needs.</p>
					<a href="{{ url('/purchase-a-website') }}" class="discover-more">Discover more</a>
				</div>
				<div class="what-k-item">
					<div class="what-k-icon">
						<i><img src="images/seo-icon.png" alt=""></i>
					</div>
					<h3>Get Found</h3>
					<h4>Increase your website ranking</h4>
					<p>Your organic ranking is where your website appears on search engines like Google. The higher your rank, the more quality leads your website will generate.</p>
					<a href="{{ url('/seo') }}" class="discover-more">Discover more</a>
				</div>
				<div class="what-k-item">
					<div class="what-k-icon">
						<i><img src="images/google-icon.png" alt=""></i>
					</div>
					<h3>Win Customers</h3>
					<h4>Advertise on Google</h4>
					<p>Advertising on Google is a great way to quickly get your company in front of the right people. You can target your ideal audience based on location and other demographics.</p>
					<a href="{{ url('/google-adwords') }}" class="discover-more">Discover more</a>
				</div>
				<div class="what-k-item">
					<div class="what-k-icon">
						<i><img src="images/facebook-icon.png" alt=""></i>
					</div>
					<h3>Connect</h3>
					<h4>Reach out on social media</h4>
					<p>We do not go online, we live online. Your customers interact, rate, review and buy though social media channels such as Facebook. Be a part of the conversation.</p>
					<a href="{{ url('/facebook') }}" class="discover-more">Discover more</a>
				</div>
			</div>
		</div>
	</section>
	<section class="be-inspired-scn" style="background-image: url('images/be-inspired-bg.jpg')">
		<div class="be-inspired-mac fl-left"><img src="images/purechase-system.png" alt=""></div>
		<div class="be-inspired-cont fl-right">
			<h2>Be inspired. <br> Live unrestricted.</h2>
			<p>Built for startups, business owners and entrepreneurs, KliqStart’s web, marketing, and communications services are simple to use and quick to set up giving you insights, freedom and control over your marketing function.</p>
			<a href="#" class="discover-more">Discover more</a>
		</div>
	</section>
	<section class="about-kliq-scn">
		<div class="container">
			<div class="about-kliq-dtl">
				<div class="about-kliq-cont">
					<h2>About KliqStart</h2>
					<p>We understand your world. There are more things to do than hours in a day yet you also need to ensure your online presence works harder, and smarter, for your business. Your company’s digital identity must look, feel, sound and act as professional as you are. Its customers, investors or potential partners’ first port of call – it’s your elevator pitch and it’s got to sell. KliqStart removes the red tape, hassle and unwanted costs associated with digital marketing services. A few kliqs are all you need to create a professional website, run an effective marketing campaign and build a great brand.</p>
					<h4 class="we-understand-txt">We understand your world.</h4>
					<a href="#" class="get-started-btn">Get Started</a>
				</div>
			</div>
			<div class="about-project-info">
				<ul>
					<li>
						<h3>40</h3>
						<h4>Projects</h4>
					</li>
					<li>
						<h3>300</h3>
						<h4>Customers</h4>
					</li>
					<li>
						<h3>8</h3>
						<h4>Awards Won</h4>
					</li>
					<li>
						<h3>2M</h3>
						<h4>Lines of Code</h4>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<section class="clients-say-scn">
		<div class="container">
			<h2>What Our Clients Say About Us</h2>
			<div class="clients-say-main">
				<div class="slider ClientsSay">
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/altmetric-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/ascap-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/altmetric-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/ascap-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/altmetric-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
