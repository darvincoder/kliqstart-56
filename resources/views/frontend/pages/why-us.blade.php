@extends('layouts.app')

@section('content')

<div class="container">
	<div class="what-k-list">
		<div class="why-k-item">
<h1>Inventive People</h1>
The heartbeat of any good company is its people. Our teams are constantly evolving, leaning new techniques, mastering the latest technology, upskilling through a variety of in-house and third party colleges. With over 250 team members our goal is for everyone, from CEO to secretary, to live, breathe and fully understand all aspects of your digital world.
</div>
<div class="why-k-item">
<h1>Resourceful Partners</h1>
KliqStart is part of the incuBeta Group. This gives us access to partner companies that offer acknowledged industry expertise in new technologies such as Augmented Reality (AR), Google DoubleClick, Certified Analytics, App development and a host of other services – managed centrally from our South African base for optimal campaign performance.
</div>
<div class="why-k-item">
<h1>Progressive Performance</h1>
We drive your business as we do ours, forward. We understand what it takes to build brands through digital marketing. Back in 1994 our own company was started by 4 people in a living room – today we boast over 3,000 customers and offices around the country. We’ve learnt lessons, we’ve bumped our heads, we’ve amended systems and processes – everything geared to help you grow. It’s our honour to be of service.
		</div>
	</div>
</div>
@endsection