@extends('layouts.app')

@section('content')
<div class="container">
	<div class="what-k-list">
		<div class="p-content">
<h1 class="privacy-heading">Privacy</h1>
This website collects and uses non-identifiable information to analyse site activity to improve the website. You have control over how this information is collected and used.
Privacy matters to us and on this page we explain how we are handling this for our clients and the visitors of our websites. We want to provide consumers and our clients notice about the use of data and choice about participating in behavioral advertising. To act as a digital marketing agency we use enormous amounts of digital collected data. For our corporate clients we want to be clear about how we are using and collecting data. In this Privacy Policy we will address our corporate clients as consumers, because don’t we all want to know what happens with our (personal) data and for what purpose the data is collected? Our privacy policy is inline with the IABs cross-industry Self-Regulatory Program for Online Behavioral Advertising and was developed by leading industry associations to apply consumer-friendly standards to online behavioral advertising across the Internet. Therefore our policy reflects industries best practice. If you have any questions please contact us.
</div>

		<div class="p-content">

<h1>We can modify this privacy policy</h1>
We reserve the right to modify, add or delete provisions of this Privacy Policy at any time and from time to time. We urge you to come back to this web page and review this Privacy Policy regularly so that you remain aware of the terms and conditions that apply to you.
</div>
		<div class="p-content">

<h1>Cookies in general</h1>
Cookies are small text files web servers send to your computer when you visit a website. These cookies are stored as text files on your hard drive to be accessed by web servers when you return to a website or go to another website. Cookies are used to collect and send information about website visits, including how many visits the website receives, how long visitors spend on the site, what pages are viewed, how visitors navigate the website and other statistics. Many of which are used to monitor the website performance, its ease of use and the effectiveness of its promotions. Cookies cannot be used to access any other data on your hard drive, or to personally identify you. If you prefer not to accept cookies you can set your Internet browser (via settings) to notify you when you receive a cookie or prevent cookies from being placed on your hard drive.
</div>

		<div class="p-content">

<h1>Cookies on our own incuBeta sites</h1>
We use Google Analytics tracking cookies to collect anonymous traffic data about your use of this website. The purpose of this information is to help us improve the site for future visitors. The social plug-ins for Google+, LinkedIn, Twitter and Facebook may also set or retrieve cookies on your machine, if you are logged in to these websites, or have previously downloaded cookies controlled by these sites.
</div>
		<div class="p-content">

<h1>What data & information do we collect and why do we do this</h1>
We collect data, cookies and information to provide better services to all of our clients and serve banners ads that are relevant. We like to set a frequency capping so you will not get spammed with the same banners over and over again. For this we set cookies on a publisher’s domain (a first-party cookie) as well as cookies set on the domain of our ad server (DoubleClick). The collection of cookies also helps us to figure out which ads you will find most useful. We may collect information about when you visit a website that uses our advertising services or how you view and interact with our ads and content. This information includes device information. We may collect device-specific information (such as your hardware model, operating system version, unique device identifiers and mobile network information including phone number). With this information it is not possible to identify you as an individual. We use this information to help make the advertising you see more interesting and relevant to you. The information we collect relates to how a device is being used to look at websites and NOT about the person using the device.
</div>
		<div class="p-content">

<h1>Mobile possibilities</h1>
When you use a location-enabled service on your mobile device, we may collect and process information about your actual location, like GPS signals sent by a mobile device. This enables us to target you with geo-fencing. We may also use various technologies to determine location, such as sensor data from your device that may, for example, provide information on nearby Wi-Fi access points and cell towers. This helps us to show you advertising and promotions that are relevant to you at that exact location.
</div>
		<div class="p-content">

<h1>Edit your settings</h1>
View and edit your preferences about the ads shown to you across the web using Ads Settings in your browser. You may also set your browser to block all cookies. However, it’s important to remember that many of your online preferences may not function properly if your cookies are disabled. For example, your language preferences.
</div>

	</div>	
</div>

@endsection