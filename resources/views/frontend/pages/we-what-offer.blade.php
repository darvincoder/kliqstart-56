@extends('layouts.app')

@section('content')
<!--Banner Section Start Here-->
<div class="banner-main banner-about" style="background-image: url('images/we-what-offer-hero.jpg')">
	<div class="demand-form-bnr">
		<h3>Take control of your marketing <br> with KliqStart </h3>
		<p>It’s easy to setup a world class digital presence for your brand.
			<br> Manage your websites, Google, Social Media and more with a few Kliqs.</p>
	</div>
	<div class="about-bnr-social">
		<a href="#" class="about-fb"></a>
		<a href="#" class="about-gp"></a>
		<a href="#" class="about-tw"></a>
		<a href="#" class="about-yt"></a>
		<a href="#" class="about-be"></a>
	</div>
</div>
<!--Middle Section Start Here-->
<div class="mid-scn-main">
	<section class="philosophy-business-scn marketing-madeeasy-scn">
		<div class="container">
			<h2>Our products and solutions stack</h2>
			<div class="p-business-info">
				<p>You’ve got a great idea, talent, brand or service that needs to be shared with the
					<br> world and you need things to happen quickly. KliqStart gets you going with fast, professional,
					<br>services – delivered to your exact specifications.</p>
				<strong>KliqStart is online marketing made easy</strong>
			</div>
		</div>
	</section>
	<section class="what-kliqstart-scn  other-offerings-scn">
		<div class="container">
			<div class="other-offerings-dtl">
				<h2>Other Offerings</h2>
				<div class="what-k-list">
					<div class="what-k-item">
						<div class="what-k-icon">
							<i><img src="images/wordpress-icon.png" alt=""></i>
						</div>
						<h3>Say Hello</h3>
						<h4>Create your own website</h4>
						<a href="{{ url('/purchase-a-website') }}" class="discover-more">Discover more</a>
					</div>
					<div class="what-k-item">
						<div class="what-k-icon">
							<i><img src="images/seo-icon.png" alt=""></i>
						</div>
						<h3>Get Found</h3>
						<h4>Increase your website ranking</h4>
						<a href="{{ url('/seo') }}" class="discover-more">Discover more</a>
					</div>
					<div class="what-k-item">
						<div class="what-k-icon">
							<i><img src="images/google-icon.png" alt=""></i>
						</div>
						<h3>Win Customers</h3>
						<h4>Advertise on Google</h4>
						<a href="{{ url('/google-adwords') }}" class="discover-more">Discover more</a>
					</div>
					<div class="what-k-item">
						<div class="what-k-icon">
							<i><img src="images/facebook-icon.png" alt=""></i>
						</div>
						<h3>Connect</h3>
						<h4>Reach out on social media</h4>
						<a href="{{ url('/facebook') }}" class="discover-more">Discover more</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="clients-say-scn">
		<div class="container">
			<h2>What Our Clients Say About Us</h2>
			<div class="clients-say-main">
				<div class="slider ClientsSay">
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/altmetric-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/ascap-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/altmetric-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/ascap-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
					<div>
						<div class="ClientsSay-dtl">
							<span class="ClientsSay-logo"><img src="images/altmetric-logo.png" alt=""></span>
							<h4>Clients Name goes here</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection