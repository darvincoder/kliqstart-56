@extends('layouts.app')
@section('css')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
<!--Banner Section Start Here-->
<div class="website-bnr-inner">
  <div class="container">
    <div class="bnr-inner-cont">
      <h2>Google AdWords</h2>
      <p>Target customers with the right message at the right time.</p>
    </div>
  </div>
@include('frontend.includes.cart')
</div>
<!--Middle Section Start Here-->
<div class="mid-scn-main">
  <section class="google-adwords-setupscn">
    <div class="container">
      <form id="msform" class="website-setupscn">
        <!-- progressbar -->
        <div class="progressbar-main">
          <ul id="progressbar" class="progressbar"  id="google-step-1">
            <li class="active" data-page="page-1">
              <a href="#"><h2><span>Step 1</span></h2></a>
            </li>
            <li data-page="page-2" id="google-step-2">
              <h2><span>Step 2</span></h2>
            </li>
            <li data-page="page-3" id="google-step-3">
              <h2><span>Step 3</span></h2>
            </li>
            <li data-page="page-4" id="google-step-4">
              <h2><span>Step 4</span></h2>
            </li>
            <li data-page="page-5" id="google-step-5">
              <h2><span>Done</span></h2>
            </li>
          </ul>
        </div>
        <div class="back_btn"><a id="previous-btn" data-currentstatus="hi" href="#">Previous</a></div>
        <div class="website-setup-dtl">
          @include('frontend.google-adwords.partials.step1')
          @include('frontend.google-adwords.partials.step2')
          @include('frontend.google-adwords.partials.step3')
          @include('frontend.google-adwords.partials.step4')
          @include('frontend.google-adwords.partials.step5')
        </div>
      </form>
      </div>
  </section>
  </div>
@endsection
@section('js')

  <script type="text/javascript">
      controller.initGoogleAdWords();
      $("#header-cart-button").off('click');
  </script>
@endsection
