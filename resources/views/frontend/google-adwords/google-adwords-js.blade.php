@section('js')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/setup.js"></script>
  <script src="js/custom.js"></script>
  <script type="text/javascript">
    var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('mapme'), {
          zoom: 2,
          center: new google.maps.LatLng(2.8,-187.3),
          mapTypeId: 'terrain'
        });
        // Create a <script> tag and set the USGS URL as the source.
        var script = document.createElement('script');
        // This example uses a local copy of the GeoJSON stored at
        // http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojsonp
        script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
        document.getElementsByTagName('head')[0].appendChild(script);
      }
      // Loop through the results array and place a marker for each
      // set of coordinates.
      window.eqfeed_callback = function(results) {
        for (var i = 0; i < results.features.length; i++) {
          var coords = results.features[i].geometry.coordinates;
          var latLng = new google.maps.LatLng(coords[1],coords[0]);
          var marker = new google.maps.Marker({
            position: latLng,
            map: map
          });
        }
      }
  </script>
  </script>
  <script type="text/javascript">
  var cartOrder = {};
  $(document).ready(function() {
      cartOrder.orderId = "wlDwCAz44h";
      cartOrder.service = "goolge"
      window.selectedCountries = [];
      window.data = {};
      window.peoplecount = 0;

      // prograce bar jump
      var current_page_fs = 'page-1';
      $('#progressbar li').click(function () {
        if ($(this).attr('class') == 'active') {
          if(current_page_fs == $(this).data('page')) {
            return false;
          }
          changeFs($(this).data('page'), current_page_fs);
          current_page_fs = $(this).data('page');
        }
      });
      function changeFs(show_fs_id, current_fs_id = "page-5") {
        var left, opacity, scale;
        current_fs = $("#"+current_fs_id);
        next_fs = $("#"+ show_fs_id);
        // console.log(current_fs);
        // console.log(next_fs);
        // console.log(show_fs_id);

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
          step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
              'position': 'relative'
            });
            next_fs.css({'left': left, 'opacity': opacity, 'transform' : 'none', 'position' : 'relative'});
          },
          duration: 800,
          complete: function(){
            current_fs.hide();
            animating = false;
          },
          //this comes from the custom easing plugin
          easing: 'easeInOutBack'
        });
    };
      //language autocomplete

      $('#language').autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{ url('/getLanguage') }}",
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function(data) {
                    var tempArr = data.map((item) => {
                        return {
                            label: item.LanguageName,
                            value: item.LanguageName,
                            id: item.CriterionID
                        }
                    });
                    response(tempArr);
                }
            });
        },
        minLength: 2,
        response: function(event, ui) {
            if (!ui.content.length) {
                var noResult = { value:"",label:"No results found" };
                ui.content.push(noResult);
                //$("#message").text("No results found");
            } else {
                $("#message").empty();
            }
        },
        select: function( event, ui ) {
          if(ui.item.value != ''){
          $('#language-tags').append('<i class="tag" data-criteriaid='+ui.item.id+' >'+ui.item.value+'<i class="tag-close" id="language-tags-remove"></i></i>');
          setTimeout(function(){

            $('input[name="language"]').val('');
          },100);
          // console.log($('input[name="language"]').val());
          window.data.language = getCriteriaidOfTags("#language-tags");
          get_search_volume(window.data);
        }
        }
      });
      $(document).on('click', '.tag-close#language-tags-remove', function () {
        window.data.language = getCriteriaidOfTags("#language-tags");
        get_search_volume(window.data);
      });
      $('#locations').autocomplete({
          source: function(request, response) {
              $.ajax({
                  url: "{{ url('/getlocations') }}",
                  dataType: "json",
                  data: {
                      term: request.term
                  },
                  success: function(data) {
                      var tempArr = data.map((item) => {
                          return {
                              label: item.canonicalName,
                              value: item.canonicalName,
                              id: item.criteriaID
                          }
                      });
                      response(tempArr);
                  }
              });
          },
          select: function(event, ui) {

              //get selected country lat long
              $.ajax({
                  url: "https://maps.googleapis.com/maps/api/geocode/json?address="+ ui.item.value +"&key=AIzaSyBSTBZbCaCE1trqgmiUPCKdovtuPVtVdO4",
                  dataType: "json",
                  success: function(data) {
                    $('#locations-tags').append('<span class="tag" data-latlng='+JSON.stringify(data.results[0].geometry.location)+' data-criteriaID='+ui.item.id+' data-canonicalname="'+ui.item.value+'" >'+ui.item.value+'<span class="tag-close" id="location-tagremove"></span></span>');
                      addNewCountryMap(data.results[0].geometry.location);
                      $('#locations').val('');
                      window.data.location = getCriteriaidOfTags("#locations-tags");
                      get_search_volume(window.data);
                  }
              });
          },
          minLength: 4,
      });

      $(document).on('click', '.tag-close#location-tagremove', function () {
        window.selectedCountries = getLocationTagsName("#locations-tags");
        loadGoogleMaps();
      });


      window.gMapsCallback = function() {
          $(window).trigger('gMapsLoaded');
      }


      function initialize() {
          var uluru = window.selectedCountries;
          var uluru1 = {
              lat: -25.363,
              lng: 131.044
          };
          var mapOptions = {
              zoom: 2,
              panControl: true,
              // center: new google.maps.LatLng(47.3239, 5.0428),
              center: uluru1,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
          var marker = {};
          var bounds = {};
          var loc = {};
          for (var i = 0; i < window.selectedCountries.length; i++) {
              // data[i]
              bounds = new google.maps.LatLngBounds();
              loc = new google.maps.LatLng(window.selectedCountries[i].lat, window.selectedCountries[i].lng);
              bounds.extend(loc);
              marker = new google.maps.Marker({
                  position: loc,
                  // position: uluru,
                  map: map
              });
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                      infowindow.setContent(window.selectedCountries[i]);
                      infowindow.open(map, marker);
                  }
              })(marker, i));
          }
          // map.fitBounds(bounds);
          // map.panToBounds(bounds);
      }

      function loadGoogleMaps() {
          var script_tag = document.createElement('script');
          script_tag.setAttribute("type", "text/javascript");
          script_tag.setAttribute("src", "https://maps.googleapis.com/maps/api/js?key=AIzaSyBSTBZbCaCE1trqgmiUPCKdovtuPVtVdO4&callback=gMapsCallback");
          (document.getElementsByTagName("body")[0] || document.documentElement).appendChild(script_tag);
      }
      // $(document).ready(function(){
      $(window).bind('gMapsLoaded', initialize);
      loadGoogleMaps();

      function addNewCountryMap(countryLatLong){
        window.selectedCountries.push(countryLatLong);
        loadGoogleMaps();
      }
  });
  </script>
  <script type = "text/javascript" >
      // var data = {};

  function get_search_volume(data) {
      $.ajax({
          url: '{{ url('/getsearchvolume') }}',
          type: 'POST',
          dataType: 'json',
          data: {
              data: data
          },
          success: function(result) {
              // setTimeout(function() {
                  $('.audience-count').text(result.count);
                  var cpc = '';
                  $(result.cpc).each(function(index, el) {
                    cpc += '<p><strong>'+el.keyword+'</strong> '+el.cpc+'(cpc)</p>';
                  });
                  cpc += '<div class="estimated-performance-row"><p>Total Searches per month for above Keywords</p><p style="margin-top:  12px;"><span>'+result.count+'</span></p></div>';
                  // console.log(cpc);
                  $('.estimated-performance-dtl').html(cpc);
                  window.peoplecount = result.count;
              // }, 1000);
          }
      });

  }
  $(document).ready(function(e) {
      $('#keyword').keypress(function(e) {
          if (e.which == 13) {
              // productsandservices-add
              // keyword-tags
              // console.log($('#keyword').val());
              if ($('#keyword').val() == "") {
                  return false;
              }
              $('#keyword-tags').append('<span class="tag">' + $('#keyword').val() + '<span class="tag-close" id="keywordremove"></span></span>');
              $('#keyword').val('');
              window.data.keyword = getTags("#keyword-tags");
              $('#audience-count').html('<img src="images/loader.gif">');
              // console.log(getTags("#keyword-tags"));
              get_search_volume(data);
          }
      });
      // keyword remove
      $(document).on('click', '.tag-close#keywordremove', function() {
          $(this).parent().remove();
          window.data.keyword = getTags("#keyword-tags");
          console.log(getTags("#keyword-tags"));
          if (getTags("#keyword-tags").length !== 0) {
              get_search_volume(window.data);
          } else {
              $('#audience-count').text(0);
          }
      });
      // product-promote-tags
      $('#product-promote').keypress(function(e) {
          if (e.which == 13) {
            if ($('#product-promote').val() == "") {
                return false;
            }
            $('#product-promote-tags').append('<i class="tag">' + $('#product-promote').val() + '<i class="tag-close" ></i></i>');
            $('#product-promote').val('');
          }
      });
        // business-unique-tags
        $('#business-unique').keypress(function(e) {
            if (e.which == 13) {
              if ($('#business-unique').val() == "") {
                  return false;
              }
              $('#business-unique-tags').append('<i class="tag">' + $('#business-unique').val() + '<i class="tag-close" ></i></i>');
              $('#business-unique').val('');
            }
        });
        // Name three of your main competitors
        $('#competitors').keypress(function(e) {
            if (e.which == 13) {
              if ($('#competitors').val() == "") {
                  return false;
              }
              $('#competitors-tags').append('<i class="tag">' + $('#competitors').val() + '<i class="tag-close" ></i></i>');
              $('#competitors').val('');
            }
        });
        // Anything else you’d like to add?
        $('#extra-details').keypress(function(e) {
            if (e.which == 13) {
              if ($('#extra-details').val() == "") {
                  return false;
              }
              $('#extra-details-tags').append('<i class="tag">' + $('#extra-details').val() + '<i class="tag-close" ></i></i>');
              $('#extra-details').val('');
            }
        });
        // people-looking
        $('#people-looking').keypress(function(e) {
            if (e.which == 13) {
              if ($('#people-looking').val() == "") {
                  return false;
              }
              $('#people-looking-tags').append('<i class="tag">' + $('#people-looking').val() + '<i class="tag-close" ></i></i>');
              $('#people-looking').val('');
            }
        });
      //addition x11 - end
    $('li').find('.upload-txt-arrow').click(function(e) {
      $('.upload-txt-arrow').removeClass('upload-txt-current')
      $(this).addClass('upload-txt-current')
      $('.content-editer-comment').removeClass('page-editer-current');
      $(this).next('.content-editer-comment').addClass('page-editer-current');
    });
    //Accordion Code
    $('.according-main li span').click(function(e) {
      $('.according-main li span').parent('li').removeClass('acc-current');
      $(this).parent('li').addClass('acc-current');
    });
    function peopleCount() {
      return window.peoplecount;
    }

      $('.next').click(function() {
        console.log(cartOrder);
          current_fs = $(this).parent();
          next_fs = $(this).parent().next();
          if(current_fs.attr('id') == "page-1") {
            if(getTags("#keyword-tags").length == 0){
              swal("Keyword", "Please Enter keywords", "warning");
              return false;
            }else{
              cartOrder.keyword = getTags("#keyword-tags");
            }
            if(getTags("#locations-tags").length == 0){
              swal("Location", "Please Enter Your Locations", "warning");
              return false;
            }else{
              cartOrder.locations = getTags("#locations-tags");
            }
          }
          if(current_fs.attr('id') == "page-2") {
            if(getTags("#language-tags").length == 0){
              swal("Language", "Please Enter language", "warning");
              return false;
            }else{
              cartOrder.keyword = getTags("#language-tags");
            }
            if (getTags('#product-promote-tags').length == 0) {
              swal("Product or Service", "Please Enter product or service do you want to promote", "warning");
              return false;
            }else{
              cartOrder.promoteProduct = getTags('#product-promote-tags');
                  $('#summary-language').text(getTags('#product-promote-tags').toString());
            }
            if (getTags('#business-unique-tags').length != 0) {
              cartOrder.businessUnique = getTags('#business-unique-tags');
            }
            if (getTags('#competitors-tags').length != 0) {
              cartOrder.competitors = getTags('#competitors-tags');
            }
            if (getTags('#extra-details-tags').length != 0) {
              cartOrder.extraDetails = getTags('#extra-details-tags');
            }
          }
          if(current_fs.attr('id') == "page-3") {
            if($('#campaign-run-months').val() == ''){
              swal("campaign months", "Please Enter How many months will this ad campaign run for?", "warning");
                return false;
            }else{
              cartOrder.campaignRunMonths = $('#campaign-run-months').val();
                  $('#summary-months').text($('#campaign-run-months').val());
            }
              if($('#timing-from').val() != '' && $('#timing-to').val() != ''){
                cartOrder.dayRunTime = {'from': $('#timing-from').val(), 'to': $('#timing-to').val()};
                  $('#summary-running-times').text($('#timing-from').val()+' - '+$('#timing-to').val());
              }
              if (getTags('#people-looking-tags').length != 0) {
                cartOrder.campaignShowpeoplelooking = getTags('#people-looking-tags');
              }
          }
          if(current_fs.attr('id') == "page-4") {
            if($("#slider-range").slider("value") == 0){
              swal("budget", "Please Select your budget", "warning");
              return false;
            }else{
              cartOrder.package = {};
              cartOrder.package.price = ($("#slider-range").slider("value")*30);
              $('#summary-budget').text($("#slider-range").slider("value"));
            }

          }
          //activate next step on progressbar using the index of next_fs
          $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

          //show the next fieldset
          next_fs.show();
          //hide the current fieldset with style
          current_fs.animate({
              opacity: 0
          }, {
              step: function(now, mx) {
                  //as the opacity of current_fs reduces to 0 - stored in "now"
                  //1. scale current_fs down to 80%
                  scale = 1 - (1 - now) * 0.2;
                  //2. bring next_fs from the right(50%)
                  left = (now * 50) + "%";
                  //3. increase opacity of next_fs to 1 as it moves in
                  opacity = 1 - now;
                  current_fs.css({
                      'position': 'relative'
                  });
                  next_fs.css({
                      'left': left,
                      'opacity': opacity
                  });
              },
              duration: 800,
              complete: function() {
                  current_fs.hide();
                  animating = false;
              },
              //this comes from the custom easing plugin
              easing: 'easeInOutBack'
          });
      });
      //submit form for add to cart
      $("#addtoCartSEO").click(function() {
        var addToCartButton = $(this);
        swal({
            title: "Are you sure?",
            text: "Please make sure you have filled all details correctly!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              if(!localStorage.getItem("email")){
								localStorage.setItem("email", cartOrder.email);
							}
                $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "POST",
                  url: '{{ url('/addtocart') }}',
                  data: {data: JSON.stringify(cartOrder)},
                  success: function( result ) {
                    var appendData = '<li> <span class="websitepackdata">'+ucwords(result.data.service)+' package </span> <span class="purchase-close" data-orderId="'+result.data.orderId+'" data-cartPackagePrice = "'+result.data.packageprice+'"></span> </li>';
                    appendData += '<li class="Qtypricec"> <strong>Qty: 1</strong> <span class="yourcart-price cart-package-price">R'+result.data.packageprice+'</span> </li>';
                    $('.form-cart').append(appendData);
                    // $('#cart').append(appendData);

                    var cartTotal = 0;
                    var oldCountHeaderCart = parseInt($('#header-cart-count').text());
                    $('.cart-package-price').each(function () {
                      packageprice = $(this).text();
                      cartTotal = cartTotal + parseInt(packageprice.substr(1));
                    });
                    $('.cart-total').text('R'+cartTotal);
                    $('#header-cart-count').text(oldCountHeaderCart+1);
                    // $('#cart-total').text('R'+cartTotal);
                    $('.summary-adit-btn').hide();
 									 addToCartButton.hide();
 									 delete cartOrder;
                    // window.location.reload();
                  }
                });
            }
          });
      });
  });
  //Slider Range
  $(function() {
      $("#slider-range").slider({
          range: "min",
          min: 0,
          max: 73,
          value: 0,
          slide: function(event, ui) {
              $("#amount").val(" ZAR" + ui.value);
          }
      });
      $("#amount").val("ZAR" + $("#slider-range").slider("value"));
  });
  </script>

@endsection
