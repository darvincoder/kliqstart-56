@section('js')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/setup.js"></script>
<script src="js/custom.js"></script>
{{-- https://github.com/aehlke/tag-it --}}
<script type="text/javascript">
  var map;
    function initMap() {
      map = new google.maps.Map(document.getElementById('mapme'), {
        zoom: 2,
        center: new google.maps.LatLng(2.8,-187.3),
        mapTypeId: 'terrain'
      });
      // Create a <script> tag and set the USGS URL as the source.
      var script = document.createElement('script');
      // This example uses a local copy of the GeoJSON stored at
      // http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojsonp
      script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
      document.getElementsByTagName('head')[0].appendChild(script);
    }
    // Loop through the results array and place a marker for each
    // set of coordinates.
    window.eqfeed_callback = function(results) {
      for (var i = 0; i < results.features.length; i++) {
        var coords = results.features[i].geometry.coordinates;
        var latLng = new google.maps.LatLng(coords[1],coords[0]);
        var marker = new google.maps.Marker({
          position: latLng,
          map: map
        });
      }
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('MAP_API_KEY') }}&callback=initMap">
</script>
<script type="text/javascript">
var data = {};
function get_search_volume(data) {
  // data.keyword = ['cheesecake'];
  // data.location = 20008,20009;
  // data.language = 1019,1056;

  $.ajax({
    url: '{{ url('/getsearchvolume') }}',
    type: 'POST',
    dataType: 'json',
    data: {data: data},
    success: function (result) {
      setTimeout(function () {
        $('#audience-count').text(result.count);
      }, 1000);
    }
  });
  // .done(function() {
  //   console.log("success");
  // })
  // .fail(function() {
  //   console.log("error");
  // })
  // .always(function() {
  //   console.log("complete");
  // });

}
  $(document).ready(function(e) {
    $('#keyword').keypress(function(e){
      if(e.which == 13){
        // productsandservices-add
        // keyword-tags
        // console.log($('#keyword').val());
        if($('#keyword').val() == "") {
          return false;
        }
        $('#keyword-tags').append('<span class="tag">'+$('#keyword').val()+'<span class="tag-close" id="keywordremove"></span></span>');
				$('#keyword').val('');
        data.keyword = getTags("#keyword-tags");
        $('#audience-count').html('<img src="images/loader.gif">');
        // console.log(getTags("#keyword-tags"));
        get_search_volume(data);
      }
    });
    // keyword remove
    $(document).on('click', '.tag-close#keywordremove', function () {
    	$(this).parent().remove();
      data.keyword = getTags("#keyword-tags");
      console.log(getTags("#keyword-tags"));
      if(getTags("#keyword-tags").length !== 0){
        get_search_volume(data);
      }else{
        $('#audience-count').text(0);
      }
    });




  // //addition x11 - start
  // var location = document.getElementById('location');
  // var language = document.getElementById('language').value.split(" ");
  // var keyword1 = document.getElementById('keyword1').value.split(" ");
  // var keyword2 = document.getElementById('keyword2').value.split(" ");
  // var keyword3 = document.getElementById('keyword3').value.split(" ");
  // var keyword4 = document.getElementById('keyword4').value.split(" ");
  // var keyword5 = document.getElementById('keyword5').value.split(" ");
  // var keywordArray = keyword1.concat(keyword2, keyword3, keyword4, keyword5);
  // //pull locations from the adwords.json and match it to the map
  // //send ajax to backend with location=String, language=Array|String, keywordArray=Array
  //
  // // update this figure from ajax
  // var figure = document.getElementById('potential');
  // var figure2 = document.getElementById('potential2');
  // var figure3 = document.getElementById('potential3');
  //
  //   //check client input and get the locations
  // var myInput = document.getElementById("targetBusiness");
  // var myInput2 = document.getElementById("targetBusiness2");
  // if (myInput && myInput.value) {
  //   console.log("targetBusiness1");
  // }else{
  //   console.log("targetBusiness2");
  // }
  //addition x11 - end
  $('li').find('.upload-txt-arrow').click(function(e) {
    $('.upload-txt-arrow').removeClass('upload-txt-current')
    $(this).addClass('upload-txt-current')
    $('.content-editer-comment').removeClass('page-editer-current');
    $(this).next('.content-editer-comment').addClass('page-editer-current');
  });
  //Accordion Code
  $('.according-main li span').click(function(e) {
    $('.according-main li span').parent('li').removeClass('acc-current');
    $(this).parent('li').addClass('acc-current');
  });
  $('.next').click(function() {
    current_fs = $(this).parent();
    next_fs = $(this).parent().next();
    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
     step: function(now, mx) {
       //as the opacity of current_fs reduces to 0 - stored in "now"
       //1. scale current_fs down to 80%
       scale = 1 - (1 - now) * 0.2;
       //2. bring next_fs from the right(50%)
       left = (now * 50)+"%";
       //3. increase opacity of next_fs to 1 as it moves in
       opacity = 1 - now;
       current_fs.css({
          'position': 'relative'
        });
       next_fs.css({'left': left, 'opacity': opacity});
     },
     duration: 800,
     complete: function(){
       current_fs.hide();
       animating = false;
     },
     //this comes from the custom easing plugin
     easing: 'easeInOutBack'
    });
  });
});
  //Slider Range
  $(function() {
    $("#slider-range").slider({
      range: true,
      min: 0,
      max: 100,
      values: [0, 72],
      slide: function(event, ui) {
        $("#amount").val(" ZAR" + ui.values[1]);
      }
    });
    $("#amount").val("ZAR" + $("#slider-range").slider("values"));
  });
</script>
@endsection
