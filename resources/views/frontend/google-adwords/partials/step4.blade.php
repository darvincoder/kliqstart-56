<fieldset id="page-4">
            <div class="googleadwords-cont">
              <div class="gl-adwds-cont">
                <div class="gl-adwds-left">
                  <h2>4: Let’s finalise your budget.</h2>
                  <p>Here’s what you can expect. Feel free to play and adjust to suit<span class="question-icon"></span></p>
                  <div class="gl-select-your-budget"> <strong>Select your budget</strong>
                    <p>Set the amount you’d like to spend</p>
                    <p>
                      <input type="text" id="amount" class="amount-range" readonly value="0"> Per day
                      <br> ZAR2,200.00 per month maximum </p>
                    {{--<div id="slider-range"></div>--}}
                    <input type="range" min="0" max="2200" value="1" class="slider" id="slider-range">
                    <div class="how-budget-work">
                      <p><strong>How your budget works</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                        do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea </p>
                    </div>
                  </div>
                </div>
                <div class="gl-adwds-right">
                  <div class="estimated-performance potential-audience">
                    <h5>Keyword CPC</h5>
                    {{-- <h5>Estimated Performance</h5> --}}
                    <div class="estimated-performance-dtl">
                      {{-- <p><strong>coffee</strong> 12343125</p>
                      <div class="estimated-performance-row">
                        <p><span id="views">6591-11005</span>
                          <br> Views per month</p>
                      </div> --}}
                      {{-- <div class="estimated-performance-row">
                        <p>Total Searches per month for above Keyword</p>
                        <p><span></span>
                          <br> Searches per month</p>
                      </div> --}}
                      {{-- <div>
                        <p>This estimate is based on businesses with similar ad settings and budget.</p>
                      </div> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <a href="#" id="next-btn4" class="next nexthelp-btn action-button">Next</a>
            
          </fieldset>