<fieldset id="page-3">
    <div class="googleadwords-cont">
        <div class="gl-adwds-cont">
            <div class="gl-adwds-left">
                <h2>3: What is your ideal campaign setup?</h2>
                <p>Like investing, it’s better to have a long-term view<span class="question-icon"></span></p>
                    <ul class="according-main">
                        <li><span>How many months will this ad campaign run for?<i style="color: red;">*</i> <i class="accor-arrow"></i></span>
                            <input type="number" data-name="campaignRunMonth" id="campaign-run-months" class="form-control">
                            {{--<div class="campaign-run-months-tag-list" id="campaign-run-months-tags" style="margin-top: 15px;"></div>--}}
                        </li>
                        <li><span>Select the best times of the day for it to run<i class="accor-arrow"></i></span>
   
                        <span class="input-group-text" id="inputGroup-sizing-sm" style="display: none;">from
                            <input type="number"  name="campaign-timing-from" id="campaign-timing-from" class="form-control">
                            <span class="input-group-text" id="inputGroup-sizing-sm1">to
                              <input type="number"   id="campaign-timing-to" class="form-control">
                            </span> 
                        </span>

                    <!-- <div class="input-group-prepend">
                              <span class="input-group-text" id="inputGroup-sizing-sm">from</span>
                          </div> -->
                          <!-- <input type="time" data-name="campaignRunMonth" name="timing-from" id="timing-from" class="form-control" style="display: none;"> -->
                          <!-- <span class="input-group-text" id="inputGroup-sizing-sm">to</span> -->
                          <!-- <input type="time" name="timing-to" id="timing-to" class="form-control" style="display: none;"> -->
                        </li>
                        <li><span>Show the ad to people who are looking for <i class="accor-arrow"></i></span>
                            <input type="text" data-name="showTheAd" id="show-the-ad" class="form-control" style="display: none;">
                            <div class="show-the-ad-tag-list" id="show-the-ad-tags" style="margin-top: 15px;"></div>
                        </li>
                    </ul>
            </div>
            <div class="gl-adwds-right">
                <div class="potential-audience">
                    <h5>Potential audience size </h5>
                    <p><span id="audience-count1" class="audience-count">0</span> people per month This is an estimate of how many people search
                          for businesses like yours in your selected locations. Audience size doesn't affect your cost.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <a href="#" id="next-btn3" class="next nexthelp-btn action-button">Next</a>
</fieldset>