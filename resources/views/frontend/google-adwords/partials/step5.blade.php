<fieldset id="page-5">
            <div class="googleadwords-cont">
              <div class="gl-adwds-cont">
                <div class="gl-adwds-left glawd-summary">
                  <h2>5: Google AdWords Summary</h2>
                  <p>Here’s what you can expect. Feel free to play and adjust to suit<span class="question-icon"></span></p>
                  <div class="gl-select-your-budget"> <strong>Budget</strong>
                    <p> <span class="amount-range budget-amount-range">ZAR <div id="summary-budget">72.37</div></span> Per day
                      <br> ZAR2.200.00 per month maximum </p>
                    <div class="lng-drcn-runing">
                      <div class="lng-drcn-item">
                        <p><strong>Language: </strong> <div id="summary-language">English</div></p>
                      </div>
                      <div class="lng-drcn-item">
                        <p><strong>Duration of campaign: </strong> <div id="summary-months">3</div> months</p>
                      </div>
                      <div class="lng-drcn-item">
                        <p><strong>Running times: </strong> <div id="summary-running-times">8:30 - 17:00</div></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="gl-adwds-right">
                  <div class="estimated-performance potential-audience">
                    <h5>Estimated Performance</h5>
                    <div class="estimated-performance-dtl">
                      {{-- <div class="estimated-performance-row">
                        <p><span>6591-11005</span>
                          <br> Views per month</p>
                      </div>
                      <div class="estimated-performance-row">
                        <p><span>224-374</span>
                          <br> Clicks per month</p>
                      </div>
                      <div>
                        <p>This estimate is based on businesses with similar ad settings and budget.</p>
                      </div> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="shopping-check-main"><a id='addtoCartSEO' class="shopping-help-btn nexthelp-btn">Add To Cart</a> <a href="#" class="shopping-help-btn nexthelp-btn">Continue Shopping</a> </div>
          </fieldset>