<fieldset id="page-2">
  <div class="googleadwords-cont">
    <div class="gl-adwds-cont">
      <div class="gl-adwds-left">
        <h2>2: About your business</h2>
        <p>Tell us a little more about your product or service. Don’t be shy!<span class="question-icon"></span></p>
        <ul class="according-main">
          <li><span>What languages do you want to advertise in?<i style="color: red;">*</i> <i class="accor-arrow"></i></span>
            
              <input type="text" data-name="language" id="language" class="form-control">
              <div class="tag-list" style="margin-top:  15px;" id="language-tags"></div>
            
          </li>
          <li><span>What product or service do you want to promote with this ad?<i style="color: red;">*</i>(Enter product or service and press enter key) <i class="accor-arrow"></i></span>
            
              <input type="text" data-name="googleService" id="google-service" class="form-control" style="display:none">
               <div class="google-service-tag-list" style="margin-top:  15px;" id="google-service-tags"></div>
            
          </li>
          <li><span>What makes your business unique? (e.g. 24 hour service, best prices, running a special this month). <i class="accor-arrow"></i></span>
            
              <input type="text" data-name="googleBusiness" name="business-unique" id="business-unique" class="form-control" style="display:none">
              <div class="business-unique-tag-list" style="margin-top:  15px;" id="business-unique-tags"></div>
            
          </li>
        </ul>
      </div>
      <div class="gl-adwds-right">
        <ul class="according-main">
          <li><span>Name three of your main competitors <i class="accor-arrow"></i></span>

              <input type="text" data-name="googleCompetitors" id="google-competitors" class='form-control' style="display: none;">
              <div class="google-competitors-tag-list" style="margin-top:  15px;" id="google-competitors-tags"></div>
            
          </li>
          <li><span>Anything else you’d like to add? <i class="accor-arrow"></i></span>

              <input type="text" data-name="googleExtraDetails" id="google-extra-details" class="form-control" style="display: none;">
              <div class="google-extra-details-tag-list" style="margin-top:  15px;" id="google-extra-details-tags"></div>

          </li>
        </ul>
        <div class="potential-audience">
          <h5>Potential audience size </h5>
          <p><span id="audience-count1" class="audience-count">{{--32,477--}}0</span> people per month This is an estimate of how many people search for
            businesses like yours in your selected locations. Audience size doesn't affect your cost.</p>
        </div>
      </div>
    </div>
  </div>
  <a href="#" id="next-btn2" class="next nexthelp-btn action-button">Next</a>
</fieldset>