<fieldset id="page-1">
            <div class="googleadwords-cont">
              <div class="gl-adwds-cont">
                <div class="gl-adwds-left">
                  <h2 id="a">1: Enter Keywords<i style="color: red;">*</i></h2>(Enter Keywords and press enter key)
                  <input type="text" id="keyword" data-name="keywords" class="form-control" placeholder="Eg. cheesecake, coffee">
                  <div class="tag-list" style="margin-top:  15px;" id="keyword-tags"></div>
                  <h2>Where are your customers?</h2>
                  <p>Think about where your customers are and where your ads should feature.<span class="question-icon"></span></p>
                  <div class="gl-map">
                    {{-- remove this style to the css iframe wrapper / iframe --}} {{-- please note there is usage limit for the map api key
                    https://developers.google.com/maps/documentation/javascript/usage --}}
                    <div style="position: relative;
                    padding-bottom: 56.25%;
                    padding-top: 30px;
                    height: 0;
                    overflow: hidden;">
                      <div id="map_canvas" frameborder="0" style="position: absolute;
                      top: 0;
                      left: 0;
                      width: 100%;
                      height: 100%;"> </div>
                      {{-- <div id="map_canvas"> --}}
                    </div>
                  </div>
                </div>
                <div class="gl-adwds-right">
                  <ul class="radio-group">
                    
                    <li>
                      <div class="adwds-radio">
                        {{-- <input type="radio" id="targetBusiness2" checked name="targetchoose"> --}}
                        <label for="targetBusiness2" style="padding-left: 0px;">Choose location that you would like to focus<i style="color: red;">*</i></label>
                        {{-- <div class="check">
                          <div class="inside"></div>
                        </div> --}}
                        <input type="text" class="form-control" data-name="googleLocation" id="locations">
                        <div id="locations-tags" style="padding-top: 60px" class="locations-tag-list"></div>
                      </div>
                    </li>
                  </ul>
                  <div class="potential-audience">
                    <h5>Potential audience size </h5>
                    <p><span id="audience-count" class="audience-count">{{--<img src="images/loader.gif"> --}}0</span> people per month This is an estimate of how many people search for
                      businesses like yours in your selected locations. Audience size doesn't affect your cost.</p>
                  </div>
                </div>
              </div>
            </div>
            <a href="#" id="next-btn1" class="next nexthelp-btn action-button">Next</a>
          </fieldset>