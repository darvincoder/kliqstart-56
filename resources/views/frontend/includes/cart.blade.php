<div class="header-cart">

	<div class="pack-YourCart">
		<h4>Your Cart</h4>
		<div class="pack-YourCart">
			<ul class="pack-cart ks-top-cart" id="ks-page-cart">
			</ul>
			<div class="clear"></div>
			<ul class="cart-total">
				<li class="carttotal-price">
					<strong>Total</strong>
					<span id="page-yourcart-price" class="total-price">R0</span>
				</li>
				<li class="checkout-item">
					<a href="{{ url('/checkout') }}" class="checkout-btn">Checkout</a>
				</li>
			</ul>

		</div>
		<div class="clear"></div>

	</div>


</div>