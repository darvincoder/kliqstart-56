@extends('layouts.app')

@section('content')
@php
switch ($type) {
    case 'web' :
        $typeName = 'Website';
        break;
    case 'seo' :
        $typeName = 'Seo';
        break;
    case 'facebook' :
        $typeName = 'Facebook';
        break;
    case 'google' :
        $typeName = 'Google Adwords';
        break;

}
@endphp

    <div class="card">
        <div class="card-header">
            <h1> Thanks for adding <span>{{ $typeName }}</span> to your cart</h1>
            <h2>would you like to checkout or add any of our other products</h2>

            <h4>
                @if($type != 'seo')
                    <a href="{{ url('/seo') }}" id="add-to-cart-btn" class="shopping-check-btn wb-checkout-btn">Seo</a>
                @endif
                @if($type != 'web')
                    <a href="{{ url('/purchase-a-website') }}" class="shopping-check-btn c-shoppig-btn">Website</a>
                @endif
                @if($type != 'google')
                    <a href="{{ url('/google-adwords') }}" id="add-to-cart-btn" class="shopping-check-btn wb-checkout-btn">Google Adwords</a>
                @endif
                @if($type != 'facebook')
                <a href="{{ url('/facebook') }}" class="shopping-check-btn c-shoppig-btn">Facebook</a>
                @endif
                <a href="{{ url('/checkout') }}" class="check-out-button-card">checkout</a>
            </h4>

        </div>


    </div>

@endsection
@section('js')
    <script type="text/javascript">
        controller.initSeo();
    </script>
    <script src="js/jquery.easing.min.js"></script>


    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@endsection
<style>


</style>
