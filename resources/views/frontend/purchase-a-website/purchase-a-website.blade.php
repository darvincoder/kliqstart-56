@extends('layouts.app')

@section('css')
	<link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
@endsection
@section('content')
	<script type="text/javascript">
		var selected_vars = [];
	</script>
	<div class="website-bnr-inner">
		<div class="container">
			<div class="bnr-inner-cont" id="inputBox">
				<h2>Purchase a Website</h2>
			</div>
		</div>
		@include('frontend.includes.cart')
	</div>
	<!--Middle Section Start Here-->
	<div class="mid-scn-main">
		<section class="website-setupscn">
			<div class="container">
				<form id="msform" class="website-setupscn">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					<input type="hidden" name="product" value="purchase-a-website">
					<input type="hidden" name="orderid" value="{{ $orderId }}">
					
					@include('frontend.partials.paw_header')
					<div class="website-setup-dtl">
						@include('frontend.partials.package')
						@include('frontend.partials.webinfo')
						@include('frontend.partials.webcontent')
						@include('frontend.partials.webdomain')
						@include('frontend.partials.websummary')
					</div>
				</form>
			</div>
		</section>
	</div>
@endsection

@section('js')
  <script type="text/javascript">
    controller.initWeb();
    $("#header-cart-button").off('click');
  </script>
  <script src="js/jquery.easing.min.js"></script>
  
  
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  
	
@endsection

