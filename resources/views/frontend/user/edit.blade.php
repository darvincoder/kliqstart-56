@extends('layouts.app')

@section('content')
	<div class="mid-scn-main">
		<section class="bnr-acc-setting-scn">
			<div class="container">
				<div class="bnr-acc-setting"><div class="user-pic-setting"><img src="@if(Auth::user()->userDetail()->first()->image)assets/userimages/{{ Auth::user()->userDetail()->first()->image }} @else images/default-avatar.png @endif" alt=""></div> <h3>Account Settings</h3></div>
				</div>
			</section>


			<section class="update-account-scn">
				<div class="container">
					<form method="post" action="{{url('/edit')}}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="update-setting-dtl">
							<div class="fl-left update-pass-prt">
								{{-- <div class="acc-stn-inner">
								<h3>Update your Username <br>or Email</h3>
								<div class="form-group">
								<label class="inputlabel">Enter Description For This Card</label>
								<input type="text" class="form-control" placeholder="Usher Raymond" onfocus="this.placeholder=''" onblur="this.placeholder='Usher Raymond'">
							</div>

							<div class="form-group">
							<label class="inputlabel">Card Number</label>
							<input type="text" class="form-control" placeholder="34545678909876543" onfocus="this.placeholder=''" onblur="this.placeholder='34545678909876543'">
						</div>

					</div> --}}


					<div class="acc-stn-inner stng-password-dtl" style="margin-top: 0;">
						<h3>Update your Password</h3>
						{{-- <form> --}}
						<div class="form-group">
							<label class="inputlabel">Current Password</label>
							<input type="password" class="form-control" placeholder="**************" onfocus="this.placeholder=''" onblur="this.placeholder='**************'" name="current_password">
						</div>

						<div class="form-group">
							<label class="inputlabel">New Password</label>
							<input type="password" class="form-control" placeholder="**************" onfocus="this.placeholder=''" onblur="this.placeholder='**************'" name="password">
						</div>
						<div class="form-group">
							<label class="inputlabel">Re-Type Password</label>
							<input type="password" class="form-control" placeholder="**************" onfocus="this.placeholder=''" onblur="this.placeholder='**************'" name="password_confirmation">
						</div>
						<div class="form-group">
							@include('frontend.includes.errors')
							{{ Session::get('status') }}
						</div>
						{{-- </form> --}}
					</div>
					<div class="acc-stn-inner manage-subscriptions">
						<h3>Manage your subscriptions</h3>
						<div class="custom_checkbox">
							<input type="checkbox" id="keepupdate" name="manageSbtn" @if(Auth::user()->userDetail()->first()->newsletter)checked @endif>
								<label for="keepupdate">Keep me updated about new features and tips</label>
							</div>

						</div>
				</div>
				<div class="fl-right pro-manage-prt">
					<div class="acc-stn-inner">
						<h3>Update Name of <br>Profile Image</h3>
						{{-- <form> --}}
						<div class="form-group">
							<label class="inputlabel">First Name</label>
							<input type="text" class="form-control" placeholder="First Name" onfocus="this.placeholder=''" onblur="this.placeholder='First Name'" value="{{Auth::user()->userDetail()->first()->firstname}}" name="firstname">
						</div>

						<div class="form-group">
							<label class="inputlabel">Last Name</label>
							<input type="text" class="form-control" placeholder="Last Name" onfocus="this.placeholder=''" onblur="this.placeholder='Last Name'" value="{{Auth::user()->userDetail()->first()->lastname}}" name="lastname">
						</div>

						<div class="form-group">
							<label class="inputlabel">Business Name</label>
							<input type="text" class="form-control" placeholder="Business Name" onfocus="this.placeholder=''" onblur="this.placeholder='Business Name'" value="{{ Auth::user()->userDetail()->first()->businessname }}" name="businessname">
						</div>
						{{-- </form> --}}
						<div class="profile-manage-prt">
							<h4>Profile Image</h4>
							<div class="user-pic-setting acc-profile-img"><img id="output" src="@if(Auth::user()->userDetail()->first()->image)assets/userimages/{{ Auth::user()->userDetail()->first()->image }} @else images/default-avatar.png @endif" alt=""></div>

								<div class="update-remove-image">
									<p>For best results, upload a <br>high resolution image</p>
									<div class="update-remove-btn">
										<div class="upload-btn-wrapper">
											<button type="button" class="upld-img-btn">Upload Image</button>
											<input type="file" name="user_image" accept="image/*" onchange="loadFile(event)">
										</div>
										<div class="upload-btn-wrapper">
											<button type="button" class="rmv-img-btn" id="remobeImage">Remove Image</button>
										</div>
									</div>
									<div id="someHiddenDiv" style="" class="alt_msg">Please click on "Save all Change" Button for change image.</div>
								</div>

							</div>


						</div>

						{{-- <div class="acc-stn-inner manage-subscriptions">
							<h3>Manage your subscriptions</h3>
							<div class="custom_checkbox">
								<input type="checkbox" id="keepupdate" name="manageSbtn" @if(Auth::user()->userDetail()->first()->newsletter)checked @endif>
									<label for="keepupdate">Keep me updated about new features and tips</label>
								</div>

							</div> --}}

						</div>
					</div>
					<div class="saveall-change">
						<button type="submit" class="cmn-btn ">Save All Changes</button>
					</div>
				</form>
			</div>
		</section>








	</div>

	<!--Footer Section Start Here-->
	{{-- <footer>
	<div class="ftr-container">
	<div class="ftr-row ftr-company">
	<h3>Company</h3>
	<ul>
	<li><a href="#">FAQâ€™s</a></li>
	<li><a href="#">Terms of Us</a></li>
	<li><a href="#">Privacy Policy</a></li>

</ul>
</div>
<div class="ftr-row ftr-account">
<h3>Account</h3>
<ul>
<li><a href="#">Sign In</a></li>
<li><a href="#">Sign Up</a></li>
<li><a href="#">Reset Password</a></li>

</ul>
</div>
<div class="ftr-row ftr-weoffer">
<h3>What We Offer</h3>
<ul>
<li><a href="facebook.html">Website</a></li>
<li><a href="google-adwords.html">SEO</a></li>
<li><a href="#">PPC</a></li>
<li><a href="facebook.html">Social</a></li>
<li><a href="#">Hosting</a></li>

</ul>
</div>
<div class="ftr-row ftr-quick">
<h3>Quick links</h3>
<ul>
<li><a href="index.html">Home</a></li>
<li><a href="about.html">About US</a></li>
<li><a href="#">How It Works</a></li>
<li><a href="#">Accreditations</a></li>
<li><a href="contact-us.html">Contact Us</a></li>

</ul>
</div>
</div>

<section class="ftr-contact-scn">
<div class="ftr-container">
<div class="ftr-contact-dtl">
<a href="tel:0800 002 3854" class="ftr-phone"><i></i> 0800 002 3854</a>
<a href="mailto:info@kliqstart.co.za" class="ftr-email"><i></i> info@kliqstart.co.za</a>
</div>
</div>
</section>

</footer> --}}

</div>

@endsection
@section('js')
	<script type="text/javascript">
	$(document).ready(function(e) {
		$('#remobeImage').click(function(){
			$.ajax({
				type: "GET",
				url: '/userimageremove/{{ Auth::user()->userDetail()->first()->id }}',
				success: function( result ) {
					// console.log(result);
					window.location.reload();
				}
			});
		});
	});
</script>
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
		 $('#someHiddenDiv').show();
  };
</script>
@endsection
