<fieldset id="fs_summary">
	<div class="summary-setup-main">
		<div class="summary-info-list">
			<div class="summary-row" id="package-summary">
			</div>
			<div class="summary-row">
				<h2><span id="summary-package-title">Package 3</span>
					<a href="#" id="seo-edit-pkg" class="summary-edit-btn"></a>
				</h2>
				<ul id="package-details">

				</ul>

			</div>
			<div class="summary-row">
				<h2>Website Info <a href="#" onClick="changeFs('fs_website')" class="summary-adit-btn"></a></h2>
				<ul>
					<li><span class="s-info-txt">Your Email Address:</span>
						<div class="domain-info-dtl email-wb-info" id="summary-email"></div>
					</li>
					<li><span class="s-info-txt">Copywriting:</span>
						<div class="domain-info-dtl" id="summary-copywriting"></div>
					</li>
					<li><span class="s-info-txt">Template:</span>
						<div class="domain-info-dtl" id="summary-template"></div>
					</li>
					<li><span class="s-info-txt">Colours:</span>
						<div class="domain-info-dtl">
							<div class="domain-color-list"><span class="d-blue-color"></span><span class="d-aquamarine-color"></span><span class="d-yellowlight-color"></span><span class="d-miscellaneous-color"></span></div>
						</div>
					</li>
				</ul>
			</div>
			<div class="summary-row">
				<h2>Content <a href="#" onClick="changeFs('fs_content')" class="summary-adit-btn"></a></h2>
				<ul id="summary-content">
					<li><a href="#" onClick="changeFs('fs_content')">You Uplode Doc file for all Pages</a></li>
				</ul>
			</div>
		</div>
		<div class="summary-info-list summary-domain-price">
			<div class="summary-row">
				<h2>Domain <a href="#" onClick="changeFs('fs_domain')" class="summary-adit-btn"></a></h2>
				<ul id="display-domain-summary">
					<li> www.yourdomain.co.za</li>
					<li>Email Accounts</li>
					
				</ul>
			</div>
			<div class="r-domain-price"><span id="summary-price">R12 000.00</span></div>
		</div>
	</div>
	<div class="shopping-check-main">
		<a href="{{ url('/') }}" class="shopping-check-btn c-shoppig-btn">Continue Shopping</a>
		<a href="#" id="addtoCartWS" class="shopping-check-btn wb-checkout-btn">Add to Cart</a>
		<a href="{{ url('/checkout') }}" class="shopping-check-btn wb-checkout-btn">Check out</a>
	</div>
</fieldset>