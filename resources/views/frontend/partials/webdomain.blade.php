<fieldset id="fs_domain">
	<div class="domain-main">
		<div class="domain-list">
			<div class="domain-list-dtl">
				<h2><span>1:</span> Do you have a Domain?</h2>
				<ul class="radio-group">
					<li>
						<input type="radio" class="have-domain" checked="" name="havedomain" id="domainYes" value="yes">
						<label for="domainYes">Yes</label>
						<div class="check"></div>
					</li>
					<li>
						<input type="radio" class="have-domain" id="domainNo" name="havedomain" value="no">
						<label for="domainNo">No</label>
						<div class="check">
							<div class="inside"></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="step-4-yes">
		<div class="domain-list">
			<div class="domain-list-dtl">
				<h2><span>2:</span> Enter Domain Name</h2>
				<div class="buy-domain-slt">
					<input type="text" class="form-control domain-input" placeholder="Enter Your Domain Name" onfocus="this.placeholder=''" onblur="this.placeholder='Enter Your Domain Name'" id="domainYesName" value=@if(Auth::check()) {{Auth::user()->userDetail()->first()->website_url}}@endif>
				</div>
			</div>
		</div>
		</div>
		<div id="step-4-no" style="display: none">
		<div class="domain-list" id="web-domain-list" >
			<div class="domain-list-dtl">
				<h2><span>2:</span> Buy a Domain for R150pm</h2>
				<div class="buy-domain-slt">
					<input type="text" class="form-control domain-input" placeholder="Search Domain Name" onfocus="this.placeholder=''" onblur="this.placeholder='Search Domain Name'" id="domainNoName" />
					<div class="default-select-box">
						<select id="domain-extenction">
							<option value=".co.za">co.za</option>
							<option value=".co.in">co.in</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="domain-list want-emailaddress">
			<div class="domain-list-dtl">
				<h2><span>3:</span> Want an Email Address? </h2>
				<div id="domain-email-input-list"> <span class="r20peradd-txt">R20 per address</span>
					<div class="d-input-name-list">
						<input type="text" class="form-control domain-input domain-email" placeholder="Name"/>
						<span class="atrate-email">@yourdomain.co.za</span>
					</div>
					<div class="d-input-name-list">
						<input type="text" class="form-control domain-input domain-email" placeholder="Name"/>
						<span class="atrate-email">@yourdomain.co.za </span>
					</div>
				</div>
				<span class="email-add-txt" id="add-email">Add another email address</span>
			</div>
		</div>
		</div>
	</div>
	<input type="button" name="next" class="next next-btn action-button" data-current="submit" id="web-domain-btn" value="Next: Summary" />
</fieldset>