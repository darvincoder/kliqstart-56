<fieldset id="fs_content">
	<div class="purchase-content-main">
		<div class="purchase-website-cont">
			<div class="setup-web-cont-dtl">
				<h2><span> 1:</span> Website Content</h2>
				<ul class="radio-group">
					<li>
						<input type="radio" id="upload-documents" name="websitecontent-radio" checked="" class="websitecontent-radio" value="1-doc-for-allpage">
						<label for="upload-documents">Upload 1 document for all web pages</label>
						<div class="check"></div>
					</li>
					<li>
						<input type="radio" id="upload-create" class="websitecontent-radio" name="websitecontent-radio" value="upload-or-create-content-per-pages">
						<label for="upload-create">Upload or create content per web page</label>
						<div class="check">
							<div class="inside"></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="purchase-website-cont upload-create-cont">
			<div class="setup-web-cont-dtl" id="step-3-content-1">
				<h2><span> 2:</span> Upload your Content</h2>
				<p>Upload one document/brochure for all your web pages</p>
				<input type="file" id="file1" name="singleFileUpload" accept="application/msword">
 
				<div class="upload-btn-wrapper">
					<button class="cont-upload-btn" data-name="file1" id="single-file-upload">Upload</button>
					
				</div>
			<div class="form-group">
                <label class="inputlabel">Additional Comments</label>
                <textarea class="form-control txt-additional-comments " name="fullsitedockcomment" id="fullsitedockcomment"></textarea>
              </div>
			</div>
			<div class="page-conent-update" id="step-3-content-2" style="display: none">
				<ul>
					<li>
						<h4 data-page-name="home" id="page-1">1. 
							<input type="text" id="label-home-page" value="Home Page ">
						</h4>
						
						<div class="upload-file-main">
							<input type="file" id="homefile" accept="application/msword" value="Upload Content Doc">
							<div class="upload-btn-wrapper">
								<button class="cont-upload-btn" data-name="homefile" id="home-upload">Upload</button>
								
							</div>
							<span class="upload-cont-doc">Upload Content Doc</span> </div>
						<div class="upload-txt-arrow">Upload text<span class="cont-upload-arr" id="home-page-upload-text-btn"></span></div>
						<div class="content-editer-comment page-editer-current" id="home-page-content">
							
							<div class="spcltxt-area-main">
								<label>Special Comments or Instructions</label>
								<textarea name="page-1-comment" cols="" rows="" class="spcltxt-area ckeditor" id="home-comment"></textarea>
							</div>
						</div>
					</li>
					<li>
						<h4 data-page-name="services page" id="page-2">2. <input type="text" id="label-services-page" value="Services Page "></h4>
						<div class="upload-file-main">
							<input type="file" id="servicefile" accept="application/msword" value="Upload Content Doc">
							<div class="upload-btn-wrapper">
								<button class="cont-upload-btn" data-name="servicefile" id="service-upload">Upload</button>
								
							</div>
							<span class="upload-cont-doc">Upload Content Doc</span> </div>
						<div class="upload-txt-arrow">Upload text <span class="cont-upload-arr" id="service-page-upload-text-btn"></span></div>
						<div id="service-content" class="content-comment-container" style="display:none">
							<div class="spcltxt-area-main">
								<label>Special Comments or Instructions</label>
								<textarea name="" cols="" rows="" class="spcltxt-area" id="service-comment"></textarea>
							</div>
						</div>
					</li>
					<li>
						<h4 data-page-name="about us page" id="page-3">3. 
							<input type="text" id="label-aboutus-page" value="About Us Page ">
						</h4>
						<div class="upload-file-main">
							<input type="file" id="aboutfile" accept="application/msword" value="Upload Content Doc">
							<div class="upload-btn-wrapper">
								<button class="cont-upload-btn" data-name="aboutfile" id="about-upload">Upload</button>
								
							</div>
							<span class="upload-cont-doc">Upload Content Doc</span> </div>
						<div class="upload-txt-arrow">Upload text <span class="cont-upload-arr" id="about-us-page-upload-text-btn"></span></div>
						<div id="about-us-content" class="content-comment-container" style="display:none">
							<div class="spcltxt-area-main">
								<label>Special Comments or Instructions</label>
								<textarea name="" cols="" rows="" class="spcltxt-area" id="about-comment"></textarea>
							</div>
						</div>
					</li>
					<li>
						<h4 data-page-name="contact us page" id="page-4">4. 
							<input type="text" id="label-contactus-page" value="Contact Us Page ">
						</h4>
						<div class="upload-file-main">
							<input type="file" id="contactfile" accept="application/msword" value="Upload Content Doc">
							<div class="upload-btn-wrapper">
								<button class="cont-upload-btn" data-name="contactfile" id="contact-upload">Upload</button>
								
							</div>
							<span class="upload-cont-doc">Upload Content Doc</span> </div>
						<div class="upload-txt-arrow">Upload text <span class="cont-upload-arr" id="contact-us-page-upload-text-btn"></span></div>
						
						<div id="contact-us-content" class="content-comment-container" style="display:none">
							<div class="spcltxt-area-main">
								<label>Special Comments or Instructions</label>
								<textarea name="" cols="" rows="" class="spcltxt-area" id="contact-comment"></textarea>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<input type="button" name="next" class="next next-btn action-button" id="web-content-btn" value="Next: Domain" />
</fieldset>