<div class="progressbar-main">
    <ul id="progressbar" class="progressbar">
        <li class="active" data-page="fs_packages">
            <h2><a href="#"><span>Select <br>Package</span></a></h2>
        </li>
        <li data-page="fs_website" id="web-nav-website">
            <h2><a href="#"><span> Website <br>Info</span></a></h2>
        </li>
        <li data-page="fs_content" id="web-nav-content">
            <h2><a href="#"><span>Content</span></a></h2>
        </li>
        <li data-page="fs_domain" id="web-nav-domain">
            <h2><a href="#"><span>Domain</span></a></h2>
        </li>
        <li data-page="fs_summary" id="web-nav-summary">
            <h2><a href="#"><span>Summary</span></a></h2>
        </li>
    </ul>

</div>
<div class="back_btn"><a id="previous-btn" data-currentstatus="hi" href="#">Previous</a></div>


