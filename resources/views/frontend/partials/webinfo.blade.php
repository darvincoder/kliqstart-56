<fieldset id="fs_detail">
    <div class="website-info-main">
        <div class="enailAdd-copyright">
            <div class="enailAdd-copy-item">
                <h2><span>1:</span> Enter Your Email Address</h2>
                <div class="website-info-email">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email address" onfocus="this.placeholder=''" onblur="this.placeholder='Email address'"  id="email" value=@if(Auth::check()) {{ Auth::user()->email }} @endif >
                    </div>
                    <a class="i-email-btn" id="email-modal"></a>
                </div>
                <span id="model"></span>
            </div>
<script>

   

</script>


            <div class="enailAdd-copy-item">
                <h2><span>2:</span> Copywriting</h2>
                <ul class="radio-group">
                    <li>
                        <input type="radio" id="copywriting-help" value="yes" name="copywriting">
                        <label for="copywriting-help">I need copywriting help for my 4-5 page</label>
                        <div class="check"></div>
                    </li>
                    <li>
                        <input type="radio" id="provide-my-own" value="no" name="copywriting">
                        <label for="provide-my-own"> I will provide my own copywriting for all 4-5 pages</label>
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="select-one-templete">
            <h2><span>3:</span> Select One of Our Cool Templates</h2>
            <ul class="layout-column">
                <li> <span class="layout-c-title">2 Column</span>
                    <div class="layout-c-frame"><img src="images/2-column-layout.png" alt=""></div>
                    <div id="2-column" class="btn-defualt package-button template-select">Select this one</div> </li>
                <li> <span class="layout-c-title">1/3 Right Column</span>
                    <div class="layout-c-frame"><img src="images/1-3-right-column.png" alt=""></div>
                    <div class="btn-defualt package-button template-select" id="1-3-Right-Column">Select this one</div> </li>
                <li> <span class="layout-c-title">3 Column</span>
                    <div class="layout-c-frame"><img src="images/3-column-layout.png" alt=""></div>
                    <div class="btn-defualt package-button template-select" id="3-column">Select this one</div> </li>
            </ul>
        </div>
        <div class="select-colour-scheme">
            <h2><span>4: </span>Select a Colour Scheme</h2>
            <div class="select-color-option">
                <ul class="radio-group">
                    <li>
                        <input type="radio" id="select-colour"  class="select-colour" checked name="select-colour">
                        <label for="select-colour">Select color option</label>
                        <div class="check"></div>
                    </li>
                    <li>
                        <input type="radio" class="select-colour" id="customize-own-colours" name="select-colour">
                        <label for="customize-own-colours">Customize your own colors</label>
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </li>
                </ul>
            </div>
            <ul class="website-selectcolorlist" id="website-selectcolorlist">
                <li> <span class="select-colour-img"><img src="images/select-colour-scheme-1.png" alt=""></span> 
                    <div  class="btn-defualt select-this-one get-color" data-name="selectedColorSchema" id='color-schema-option-1'>Select this one</div> </li>
                <li> <span class="select-colour-img"><img src="images/select-colour-scheme-2.png" alt=""></span> 
                    <div  class="btn-defualt select-this-one get-color" data-name="selectedColorSchema" id='color-schema-option-2'>Select this one</div> </li>
                <li> <span class="select-colour-img"><img src="images/select-colour-scheme-3.png" alt=""></span> 
                    <div  class="btn-defualt select-this-one get-color" data-name="selectedColorSchema" id='color-schema-option-3'>Select this one</div> </li>
                <li> <span class="select-colour-img"><img src="images/select-colour-scheme-4.png" alt=""></span> 
                    <div  class="btn-defualt select-this-one get-color" data-name="selectedColorSchema" id ='color-schema-option-4'>
                        Select this one
                    </div> 
                </li>
            </ul>
            <div class="customize-own-colours" id="customize-own-colours-section">
                <ul>
                    <li>
                        <h4>Main Colour</h4>
                            <div class="colorpicker-img" id="main-color-picker"><span>Select your colour<br>using this colour<br>picker</span></div>
                            <input type="hidden" id="main-color-hex" value="">
                    </li>
                    <li>
                        <h4>Secondary Colour</h4>
                            <div class="colorpicker-img" id="secondary-color-picker"><span>Select your colour<br>using this colour<br>picker</span></div>
                            <input type="hidden" id="secondary-color-hex">
                    </li>
                    <li>
                        <h4>Call to Action Colour</h4>
                            <div class="colorpicker-img" id="calltoaction-color-picker"><span>Select your colour<br>using this colour<br>picker</span></div>
                            <input type="hidden" id="calltoaction-color-hex">
                    </li>
                    <li>
                        <h4>Miscellaneous Colour</h4>
                            <div class="colorpicker-img" id="miscellaneous-color-picker"><span>Select your colour<br>using this colour<br>picker</span></div>
                            <input type="hidden" id="miscellaneous-color-hex">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <input type="button" id="web-webinfo-btn" name="next" class="next next-btn action-button" data-current="contant" value="Next: Content" />
</fieldset>


