@extends('layouts.app')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
{{-- {{ dd(json_encode(Session::get('cart'))) }} --}}
	  <!--Banner Section Start Here-->
	  <div class="website-bnr-inner">
		<div class="container">
		  <div class="bnr-inner-cont">
			<h2>Payment</h2>
		  </div>
		</div>
	  </div>
	  <!--Middle Section Start Here-->
	  <div class="mid-scn-main">
		<section class="website-setupscn">
		  <div class="container">
			<form id="msform" class="payment-setupscn">
			  <!-- progressbar -->
			  <div class="progressbar-main">
			  	
				<div class="progressbar-main">
				  <ul id="progressbar" class="progressbar">
					<li class="active">
					  <h2><span>Cart</span></h2>
					</li>
					<li>
					  <h2><span>Checkout <br>Details</span></h2>
					</li>
					<li>
					  <h2><span>Billing <br>Details</span></h2>
					</li>
					<li>
					  <h2><span>Payment</span></h2>
					</li>
					<li>
					  <h2><span>Confirmation</span></h2>
					</li>
				  </ul>
				</div>
			  </div>
			  <div class="seo-setup-dtl">
				<fieldset>
				  <input type="button" name="next" class="next next-btn action-button" value="Next: Checkout Details" />
				</fieldset>
				<fieldset id="fs_checkout_detail">
				  <div class="payment-dtl-scn">
					<div class="payment-fix">
					  <h2>Bill to Company (Optional)</h2>
					  <div class="billcompany-flt">
						<div class="biollcompanyform">
						  <form>
							<div class="form-group">
							  <label class="inputlabel">Company Name</label>
							  <input type="text" class="form-control" placeholder="Enter your companies name" onfocus="this.placeholder=''" onblur="this.placeholder='Enter your companies name'" id="company-name" value=@if(Auth::check()) {{Auth::user()->userDetail()->first()->businessname}}@endif>
							</div>
							<div class="form-group">
							  <label class="inputlabel">VAT No.</label>
							  <input type="tel" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="VAT-No">
							</div>
							<div class="form-group">
							  <label class="inputlabel">Contact No.</label>
							  <input type="tel" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="contact-no">
							</div>
						  </form>
						</div>
					  </div>
					  <div class="paymentneed-help">
						<h4>Need Help?</h4>
						<strong>Call 087 362 8000 <br>
				   Email <a href="mailto:help@kliqstart.co.za">help@kliqstart.co.za</a></strong>
						<strong class="safeshopping-txt">Safe shopping at Incubeta</strong>
						<p>Rest assured your transaction is safe</p>
						<span><img src="images/verified-secured.png" alt=""></span>
					  </div>
					  {{-- <div class="panyment-opt-prt">
						<h2>Payment Options</h2>
						<ul class="radio-group payment-optlist">
						  <li>
							<input type="radio" id="creditdebit" name="paymentoption">
							<label for="creditdebit"><em>Credit &amp; Debit Card</em> <span class="paymenticon"><i><img src="images/visa-logo.png" alt=""></i></span></label>
							<div class="check"></div>
						  </li>
						  <li>
							<input type="radio" id="maestrovisa" name="paymentoption">
							<label for="maestrovisa"><em>Maestro &amp; VISA Electron</em> <span class="paymenticon"><i><img src="images/maestro-visa-logo.png" alt=""></i></span></label>
							<div class="check">
							  <div class="inside"></div>
							</div>
						  </li>
						  <li>
							<input type="radio" id="EFT" name="paymentoption">
							<label for="EFT"><em>EFT</em> <span class="paymenticon"><i><img src="images/eft-logo.png" alt=""></i></span></label>
							<div class="check">
							  <div class="inside"></div>
							</div>
						  </li>
						  <li>
							<input type="radio" id="eBucks" name="paymentoption">
							<label for="eBucks"><em>eBucks</em> <span class="paymenticon"><i><img src="images/ebucks-logo.png" alt=""></i></span></label>
							<div class="check">
							  <div class="inside"></div>
							</div>
						  </li>
						  <li>
							<input type="radio" id="DiscoveryMiles" name="paymentoption">
							<label for="DiscoveryMiles"><em>Discovery Miles</em> <span class="paymenticon"><i><img src="images/discovery-miles.png" alt=""></i></span></label>
							<div class="check">
							  <div class="inside"></div>
							</div>
						  </li>
						  <li>
							<input type="radio" id="Mobicred" name="paymentoption">
							<label for="Mobicred"><em>Mobicred</em> <span class="paymenticon"><i><img src="images/mobicred-logo.png" alt=""></i></span></label>
							<div class="check">
							  <div class="inside"></div>
							</div>
						  </li>
						  <li>
							<input type="radio" id="MasterPass" name="paymentoption">
							<label for="MasterPass"><em>MasterPass</em> <span class="paymenticon"><i><img src="images/masterpass-logo.png" alt=""></i></span></label>
							<div class="check">
							  <div class="inside"></div>
							</div>
						  </li>
						</ul>
					  </div> --}}
					  <div class="giftvoucher-coupon">
						<h2>Gift Voucher &amp; Coupon Codes.</h2>
						{{-- <form> --}}
						  <div class="form-group">
							<label class="inputlabel">Enter your gift voucher or coupon code below:</label>
							<input type="text" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="coupon-code">
						  </div>
						{{-- </form> --}}
					  </div>
					</div>
				  </div>
				  <div class="payment-dtl-scn">
					<div class="paymentsummary">
					  <div class="payment-summary-prt">
						<h3>Summary</h3>
						<ul>
						  <li class="itemtitle"><span class="fl-left">Item Description</span> <span class="fl-right">Price</span></li>
						  	@php
						  		$totalPrice = 0;
						  	@endphp
						  	@if (!empty($cart))
							  @foreach ($cart as $item)
							  	@php
							  		$totalPrice += $item->package->price;
							  	@endphp
							  	<li><span class="fl-left">{{ ucfirst($item->service) }} Package {{ $item->package->id }}</span> <i class="pricesmry-close" data-orderId="{{ $item->orderId }}" data-cartPackagePrice="{{ $item->package->price }}"></i><span class="fl-right">{{-- R12 000.00 --}}R{{ $item->package->price }}</span></li>
							  @endforeach
							 @endif
						  {{-- <li><span class="fl-left">Website Package 2</span> <i class="pricesmry-close"></i><span class="fl-right">R12 000.00</span></li>
						  <li><span class="fl-left">Google Package 2</span><i class="pricesmry-close"></i><span class="fl-right">R7 000.00</span></li>
						  <li><span class="fl-left">Facebook Top Up</span><i class="pricesmry-close"></i><span class="fl-right">R12 000.00</span></li>
						  <li><span class="fl-left">Website Package 2</span><i class="pricesmry-close"></i><span class="fl-right">R2 000.00</span></li> --}}
						  <li class="paymentsummarytotal"><span class="fl-left">Total</span><span class="fl-right cart-total" id="cart-total">R{{ $totalPrice }}{{-- 21 000.00 --}}</span></li>
						</ul>
						{{-- <button class="next next-btn action-button">Proceed</button> --}}
						{{-- <input type="button" name="next" class="next next-btn action-button" value="Proceed" /> --}}
						{{-- <button class="next next-btn procees-btn action-button">Proceed</button> --}}
					  </div>
					</div>
				  </div>
				  {{-- <input type="button" name="next" class="next next-btn action-button" value="Next: Billing Details" /> --}}
				  <input type="button" name="next" class="next next-btn action-button" value="Proceed" data-current="CheckoutDetails"/>
				</fieldset>
				<fieldset id="fs_billing_detail">
				  <div class="payment-dtl-scn">
					<div class="payment-fix">
					  <h2>Enter Billing Address</h2>
					  <div class="billcompany-flt">
						<div class="biollcompanyform">
						  <form>
							<div class="form-group">
							  <label class="inputlabel">Country</label>
							  <div class="default-select-box">
								<select id="billing-country">
								  <option>Please select your country</option>
								  <option value="india">India</option>
								  <option value="us">US</option>
								  <option value="usa">USA</option>
								</select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="inputlabel">Address Line 1</label>
							  <input type="email" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="billing-Address-line-1">
							</div>
							<div class="form-group">
							  <label class="inputlabel">Address Line 2</label>
							  <input type="email" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''"  id="billing-Address-line-2">
							</div>
							<div class="form-group">
							  <label class="inputlabel">City</label>
							  <input type="text" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="billing-city">
							</div>
							<div class="form-group">
							  <label class="inputlabel">State / Province / Region</label>
							  <input type="text" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="billing-state">
							</div>
							<div class="form-group">
							  <label class="inputlabel">Zip / Postal Code</label>
							  <input type="text" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" id="billing-zip-code">
							</div>
						  </form>
						</div>
					  </div>
					  <div class="paymentneed-help">
						<h4>Need Help?</h4>
						<strong>Call 087 362 8000 <br>
				   Email <a href="mailto:help@kliqstart.co.za">help@kliqstart.co.za</a></strong>
						<strong class="safeshopping-txt">Safe shopping at Incubeta</strong>
						<p>Rest assured your transaction is safe</p>
						<span><img src="images/verified-secured.png" alt=""></span>
					  </div>
					</div>
				  </div>
				  <input type="button" name="next" class="next next-btn action-button" value="Next: Payment" data-current="BillingDetails"/>
				</fieldset>
				<fieldset id="fs_payment_detail">
				  <div class="payment-dtl-scn">
					<div class="payment-fix">
					  <h2>Choose a Payment Method</h2>
					  <ul class="radio-group payment-optlist">
						<li>
						  <input type="radio" id="creditdebit" name="paymentoption">
						  <label for="creditdebit"><em>Credit &amp; Debit Card</em> <span class="paymenticon"><i><img src="images/visa-logo.png" alt=""></i></span></label>
						  <div class="check"></div>
						</li>
						<li>
						  <input type="radio" id="maestrovisa" name="paymentoption">
						  <label for="maestrovisa"><em>Maestro &amp; VISA Electron</em> <span class="paymenticon"><i><img src="images/maestro-visa-logo.png" alt=""></i></span></label>
						  <div class="check">
							<div class="inside"></div>
						  </div>
						</li>
						<li>
						  <input type="radio" id="EFT" name="paymentoption">
						  <label for="EFT"><em>EFT</em> <span class="paymenticon"><i><img src="images/eft-logo.png" alt=""></i></span></label>
						  <div class="check">
							<div class="inside"></div>
						  </div>
						</li>
						<li>
						  <input type="radio" id="eBucks" name="paymentoption">
						  <label for="eBucks"><em>eBucks</em> <span class="paymenticon"><i><img src="images/ebucks-logo.png" alt=""></i></span></label>
						  <div class="check">
							<div class="inside"></div>
						  </div>
						</li>
						<li>
						  <input type="radio" id="DiscoveryMiles" name="paymentoption">
						  <label for="DiscoveryMiles"><em>Discovery Miles</em> <span class="paymenticon"><i><img src="images/discovery-miles.png" alt=""></i></span></label>
						  <div class="check">
							<div class="inside"></div>
						  </div>
						</li>
						<li>
						  <input type="radio" id="Mobicred" name="paymentoption">
						  <label for="Mobicred"><em>Mobicred</em> <span class="paymenticon"><i><img src="images/mobicred-logo.png" alt=""></i></span></label>
						  <div class="check">
							<div class="inside"></div>
						  </div>
						</li>
						<li>
						  <input type="radio" id="MasterPass" name="paymentoption">
						  <label for="MasterPass"><em>MasterPass</em> <span class="paymenticon"><i><img src="images/masterpass-logo.png" alt=""></i></span></label>
						  <div class="check">
							<div class="inside"></div>
						  </div>
						</li>
					  </ul>
					</div>
				  </div>
				  <div class="payment-dtl-scn paynewcard-dtl-scn">
					<div class="payment-fix">
					  <h2>Pay with a new card</h2>
					  <p>New cards will be saved and therefore ready to be used on future orders</p>
					  <div class="billcompany-flt">
						<div class="biollcompanyform">
						  <form>
							<div class="form-group">
							  <label class="inputlabel">Enter Description For This Card</label>
							  <input type="text" class="form-control" placeholder="Please select your country" onfocus="this.placeholder=''" onblur="this.placeholder='Please select your country'">
							</div>
							<div class="form-group">
							  <label class="inputlabel">Card Number</label>
							  <input type="tel" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''">
							</div>
							<div class="form-group straight-date">
							  <label class="inputlabel">Expiration Date</label>
							  <div class="default-select-box">
								<select>
								  <option>Month</option>
								  <option>Month</option>
								  <option>Month</option>
								  <option>Month</option>
								</select>
							  </div>
							  <div class="default-select-box">
								<select>
								  <option>Year</option>
								  <option>Year</option>
								  <option>Year</option>
								  <option>Year</option>
								</select>
							  </div>
							</div>
							<div class="form-group straight-date">
							  <label class="inputlabel">Straight / Budget</label>
							  <div class="default-select-box">
								<select>
								  <option>Straight</option>
								  <option>Straight</option>
								  <option>Straight</option>
								  <option>Straight</option>
								</select>
							  </div>
							</div>
							<div class="form-group cart-cvv">
							  <label class="inputlabel">CVV</label>
							  <div class="cvv-control">
								<input type="text" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''">
								<span class="whattxt">What is this?</span></div>
							</div>
							<div class="usercard-checkout">
							  <div class="custom_checkbox">
								<input type="checkbox" id="usercard" name="usercardCheckout">
								<label for="usercard">Use as default card on checkout</label>
							  </div>
							</div>
						  </form>
						</div>
					  </div>
					  <div class="paymentneed-help">
						<h4>Need Help?</h4>
						<strong>Call 087 362 8000 <br>
				   Email <a href="mailto:help@kliqstart.co.za">help@kliqstart.co.za</a></strong>
						<strong class="safeshopping-txt">Safe shopping at Incubeta</strong>
						<p>Rest assured your transaction is safe</p>
						<span><img src="images/verified-secured.png" alt=""></span>
					  </div>
					</div>
				  </div>
				  <input type="button" name="next" class="next next-btn action-button" value="Next: Confirmation" />
				</fieldset>
				<fieldset id="fs_summary">
				  <div class="payment-dtl-scn confirmation-form-scn">
					<h2>Bill to Company (Optional)</h2>
					<div class="confirmation-lft">
					  <div class="biollcompanyform">
						<div class="paymentsummary">
						  <ul>
							<li class="itemtitle"><span class="fl-left">Item Description</span> <span class="fl-right">Price</span></li>
							@php
								$totalPrice = 0;
							@endphp
							@if (!empty($cart))
							@foreach ($cart as $item)
								@php
									$totalPrice += $item->package->price;
								@endphp
								<li><span class="fl-left">{{ ucfirst($item->service) }} Package {{ $item->package->id }}</span> <i class="pricesmry-close" data-orderId="{{ $item->orderId }}" data-cartPackagePrice="{{ $item->package->price }}"></i><span class="fl-right">{{-- R12 000.00 --}}R{{ $item->package->price }}</span></li>
							@endforeach
						 @endif
							<li class="paymentsummarytotal"><span class="fl-left">Total</span><span class="fl-right" id="cart-total">R{{ $totalPrice }}{{-- 21 000.00 --}}</span></li>
						  </ul>
						</div>
					  </div>
					</div>
					<div class="paymentneed-help">
					  <h4>Need Help?</h4>
					  <strong>Call 087 362 8000 <br>
				   Email <a href="mailto:help@kliqstart.co.za">help@kliqstart.co.za</a></strong>
					  <strong class="safeshopping-txt">Safe shopping at Incubeta</strong>
					  <p>Rest assured your transaction is safe</p>
					  <span><img src="images/verified-secured.png" alt=""></span>
					</div>
				  </div>
				  <div class="payment-bill-confirmation">
					<div class="summary-info-list payment-bill-list">
					  <div class="summary-row">
						<h2>Checkout details <a href="#" class="summary-adit-btn" onClick="changeFs('fs_checkout_detail')"></a></h2>
						<ul id="checkout-details">
							{{-- <li><strong></strong></li> --}}
						</ul>
						{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.</p> --}}
					  </div>
					  <div class="summary-row">
						<h2>Billing Details <a href="#" class="summary-adit-btn" onClick="changeFs('fs_billing_detail')"></a></h2>
						<ul id="billing-details">
							<li id="billing-details-address-line-1"></li>
							<li id="billing-details-address-line-2"></li>
							<li id="billing-details-city"></li>
							<li id="billing-details-state"></li>
							<li id="billing-details-country"></li>
							<li id="billing-details-zip-code"></li>
						</ul>
						{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.</p> --}}
					  </div>
					  <div class="summary-row">
						<h2>Payment <a href="#" class="summary-adit-btn" onclick="changeFs('fs_payment_detail')"></a></h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.</p>
					  </div>
					</div>
				  </div>
				</fieldset>
			  </div>
			</form>
		  </div>
		</section>
	  </div>
@endsection
@section('js')
<script src="js/jquery.easing.min.js"></script>
<script src="js/setup.js"></script>
<script src="js/custom.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	$(document).ready(function(e) {
	var step = {{ $step-1 }};
	// remove package
	// $(".pricesmry-close").click(function() {
	// 	$.ajax({
	// 		url:
	// 	});
	// });
	// #progressbar
	$('#progressbar li').each(function( index, val) {
		// body...
		if (index > step) {
			return false;
		}
		$(this).addClass('active');
		// console.log($(this));
	});
	$('.seo-setup-dtl fieldset').each(function(index, val) {
		if (index > step) {
			return false;
		}
		if (index < step) {
			$(this).css({'transform': 'scale(0.8)', 'position': 'absolute', 'opacity': '0', 'display': 'none'});
		}
		if (index == step) {
			$(this).css({'display': 'block', 'left': '0%', 'opacity': '1'});
		}
	});
	var order = {};
	order['items'] = {!! json_encode(Session::get('cart')) !!};
	// $('#company-name').focusout(function () {
	// 	order['companyname'] = $(this).val();
	// 	$('#checkout-details').append('<li><strong>Company Name </strong>'+$(this).val()+'</li>');
	// });
	company_name();
			$('#company-name').focusout(company_name);
				function company_name() {
				order['companyname'] = $("#company-name").val();
				$('#checkout-details').text($("#company-name").val());
			};
	$('#VAT-No').focusout(function () {
		order['VATno'] = $(this).val();
		$('#checkout-details').append('<li><strong>VAT No. </strong>'+$(this).val()+'</li>');
	});
	$('#contact-no').focusout(function () {
		order['contactno']  = $(this).val();
		$('#checkout-details').append('<li><strong>Contact No. </strong>'+$(this).val()+'</li>');
	});
	$('#coupon-code').focusout(function () {
		order['couponcode'] = $(this).val();
		$('#checkout-details').append('<li><strong>Gift Voucher Codes </strong>'+$(this).val()+'</li>');
	});
	$('input[name="paymentoption"]').change(function () {
		order['paymentoption'] = $(this).attr('id');
	});
	order['billingAddress']={};
	$('#billing-country').change(function () {
		order.billingAddress['country'] = $(this).val();
		$('#billing-details-country').text($(this).val());
	});
	$('#billing-Address-line-1').focusout(function () {
		order.billingAddress['addressLine1'] = $(this).val();
		$('#billing-details-address-line-1').text($(this).val());
	});
	$('#billing-Address-line-2').focusout(function () {
		order.billingAddress['addressLine2'] = $(this).val();
		$('#billing-details-address-line-2').text($(this).val());
	});
	$('#billing-city').focusout(function () {
		order.billingAddress['city'] = $(this).val();
		$('#billing-details-city').text($(this).val());
	});
	$('#billing-state').focusout(function () {
		order.billingAddress['state'] = $(this).val();
		$('#billing-details-state').text($(this).val());
	});
	$('#billing-zip-code').focusout(function () {
		order.billingAddress['zipcode'] = $(this).val();
		$('#billing-details-zip-code').text($(this).val());
	});
	$('.next').click(function() {
		console.log(order);
		var data_current = $(this).attr('data-current');
		if (data_current == "CheckoutDetails") {
			if (!order.companyname) {
				swal("Package", "Please add company name", "warning");
				return false;
			}
			if (!order.VATno) {
				swal("Package", "Please add VAT No", "warning");
				return false;
			}
			if (!order.contactno) {
				swal("Package", "Please add Contact No", "warning");
				return false;
			}
		}else if (data_current == "BillingDetails") {
			if (!order.billingAddress.country) {
				swal("Package", "Please select Country", "warning");
				return false;
			}
			if (!order.billingAddress.addressLine1) {
				swal("Package", "Please add Address Line1", "warning");
				return false;
			}
			if (!order.billingAddress.city) {
				swal("Package", "Please add city", "warning");
				return false;
			}
			if (!order.billingAddress.state) {
				swal("Package", "Please add State / Province / Region", "warning");
				return false;
			}
			if (!order.billingAddress.zipcode) {
				swal("Package", "Please add Zip / Postal Code", "warning");
				return false;
			}
		}

		current_fs = $(this).parent();
		 next_fs = $(this).parent().next();

		 //activate next step on progressbar using the index of next_fs
		 $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

		 //show the next fieldset
		 next_fs.show();
		 //hide the current fieldset with style
		 current_fs.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50)+"%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
					 'position': 'relative'
				 });
				next_fs.css({'left': left, 'opacity': opacity});
			},
			duration: 800,
			complete: function(){
				current_fs.hide();
				animating = false;
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		 });
	});
});

function changeFs(show_fs_id) {
	var left, opacity, scale;
	current_fs = $("#fs_summary");
	next_fs = $("#"+ show_fs_id);
	console.log(current_fs);
	console.log(next_fs);
	console.log(show_fs_id);

	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

	//show the next fieldset
	next_fs.show();
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
				'position': 'relative'
			});
			next_fs.css({'left': left, 'opacity': opacity, 'transform' : 'none', 'position' : 'relative'});
		},
		duration: 800,
		complete: function(){
			current_fs.hide();
			animating = false;
		},
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
};
	// step
	// .seo-setup-dtl
</script>
@endsection
