@extends('layouts.app')

@section('content')
    <div class="website-bnr-inner">
        <div class="container">
            <div class="bnr-inner-cont">
                <h2>SEO</h2>
            </div>
        </div>


        @include('frontend.includes.cart')


    </div>
    <!--Middle Section Start Here-->
    <div class="mid-scn-main">
        <section class="website-setupscn">
            <div class="container">
                <form id="msform">

                    <!-- progressbar -->
                    <div class="progressbar-main">
                        <ul id="progressbar" class="progressbar">
                            <li class="active" id="seo-nav-select-pkg">
                                <h2><a href="#"><span>Select<br>Package</span></a></h2>
                            </li>
                            <li data-page="fs_detail" id="seo-nav-details">
                                <h2><a href="#"><span>Details</span></a></h2>
                            </li>
                            <li data-page="fs_recurring" id="seo-nav-recurring">
                                <h2><a href="#"><span>Recurring</span></a></h2>
                            </li>
                            <li data-page="fs_summary" id="seo-nav-summary">
                                <h2><a href="#"><span>Summary</span></a></h2>
                            </li>
                        </ul>
                    </div>
                    <div class="back_btn"><a id="previous-btn" data-currentstatus="hi" href="#">Previous</a></div>
                    <div class="seo-setup-dtl">
                        @include('frontend.seo.partials.packages')
                        <fieldset id="fs_detail" style="display: none">
                            <div class="advt-pdt-service-main">
                                <div class="advt-pdt-service-dtl">
                                    <h2>We Need Your Details</h2>
                                    <div class="website-info-email">
                                        <div class="form-group">
                                            <label class="inputlabel">Enter Your Email Address</label>
                                            <input type="text" class="form-control" placeholder="Email address" onfocus="this.placeholder=''" onblur="this.placeholder='Email address'" name="email" id="email" value=@if(Auth::check()) {{ Auth::user()->email }} @endif >
                                            <i class="i-email-btn fl-right" id="email-modal"></i>
                                            <span id="email-example"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="inputlabel">Enter Your Website URL</label>
                                        <input type="text" class="form-control" placeholder="http://www.demo.com" onfocus="this.placeholder=''" onblur="this.placeholder='http://www.demo.com'" name="url" id="url" value=@if(Auth::check()) {{Auth::user()->userDetail()->first()->website_url}}@endif>
                                    </div>
                                    <div class="form-group">
                                        <label class="inputlabel">Select a Gender</label>
                                        <div class="default-select-box">
                                            <select name="gender" id="gender">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="website-info-email">
                                        <div class="form-group mb-1">
                                            <label class="inputlabel">What are Your Products and/or Services</label>
                                            <input type="text" class="form-control" name="productsandservices" id="productsandservices">
                                            <span class="i-add-btn" id="productsandservices-add" data-name="products"></span>
                                        </div>
                                        <div class="tag-list" style="margin-top:  15px;" id="productsandservices-tags"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="inputlabel">ISP</label>
                                        <ul class="radio-group isp-radio-group">
                                            <li>
                                                <input type="radio" id="WithUs" checked="" name="isp" value="withus">
                                                <label for="WithUs">With Us</label>
                                                <div class="check"></div>
                                            </li>
                                            <li>
                                                <input type="radio" id="Other" name="isp" value="other">
                                                <label for="Other">Other</label>
                                                <div class="check">
                                                    <div class="inside"></div>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="form-group" id="isp-other" style="display: none;">
                                            <input type="text" class="form-control" placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''" name="" id="isp-detail">
                                        </div>
                                    </div>
                                    <div class="form-group comment-area">
                                        <label class="inputlabel">Additional Comments</label>
                                        <textarea class="form-control txt-additional-comments" name="comments" id="comments"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="button" id="seo-detail-btn" name="next" class="next-btn action-button" data-current="details" value="Next: Recurring" />
                        </fieldset>
                        <fieldset id="fs_recurring" style="display: none">
                            <div class="recurring-seo-fieldset" >
                                <h2>Recurring</h2>
                                <p>For how many months will this campaign be running?</p>
                                <div class="recurring-month-list">
                                    <div class="rng-month-item">
                                        <div class="btn-defualt run-month-btn" id="seo-campaign-period--6">6 Months</div>
                                    </div>
                                    <div class="rng-month-item">
                                        <div class="btn-defualt run-month-btn" id="seo-campaign-period--12">12 Months</div> </div>
                                    <div class="rng-month-item">
                                        <div href="" class="btn-defualt run-month-btn" id="seo-campaign-period--18">18 Months</div>
                                    </div>
                                    <div class="rng-month-item">
                                        <div href="" class="btn-defualt run-month-btn" id="seo-campaign-period--24">24 Months</div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next next-btn action-button" value="Next: Summary" id="seo-recurring-btn" />
                        </fieldset>
                        <fieldset id="fs_summary" style="display: none">
                            <div class="summary-setup-main summary-Seosetup">
                                <div class="summary-info-list">
                                    <div class="summary-row">
                                        <h2><span id="summary-package-title">Package 3</span>
                                            <a href="#" id="seo-edit-pkg" class="summary-edit-btn"></a>
                                        </h2>
                                        <ul id="package-details">

                                        </ul>

                                    </div>
                                    <div class="summary-row">
                                        <h2>Details <a href="#" id="seo-edit-details" class="summary-edit-btn"></a></h2>
                                        <ul>
                                            <li><strong>Email </strong><div id="summary-email"></div></li>
                                            <li><strong>Website</strong><div id="summary-website"></div></li>
                                            <li><strong>Gender</strong><div id="summary-gender"></div></li>
                                            <li><strong>Services</strong><div id="summary-products"></div></li>
                                        </ul>
                                    </div>
                                    <div class="summary-row">
                                        <h2>Reccuring <a href="#" id="seo-edit-recurring" class="summary-edit-btn"></a></h2>
                                        <ul>
                                            <li><span id="summary-reccuring">2</span> Months</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="shopping-check-main">
                                <a href="{{ url('/') }}" class="shopping-check-btn c-shoppig-btn">Continue Shopping</a>
                                <a href="#" id="add-to-cart-btn" class="shopping-check-btn wb-checkout-btn">Add to Cart</a>
                                <a href="{{ url('/checkout') }}" class="shopping-check-btn wb-checkout-btn">Check out</a>
                            </div>
                        </fieldset>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        controller.initSeo();
        $("#header-cart-button").off('click');
    </script>
    <script src="js/jquery.easing.min.js"></script>


    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@endsection
