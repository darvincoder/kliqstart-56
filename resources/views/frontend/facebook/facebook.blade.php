@extends('layouts.app')

@section('content')
	<div class="website-bnr-inner">
		<div class="container">
			<div class="bnr-inner-cont">
				<h2>Facebook</h2>
			</div>
		</div>
		@include('frontend.includes.cart')
	</div>
	<div class="mid-scn-main">
		<section class="website-setupscn">
			<div class="container">
				<form id="msform" class="facebook-setupscn">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
					<meta name="csrf-token" content="{{ csrf_token() }}">
					<input type="hidden" name="product" value="facebook">
					<input type="hidden" name="package" id="package">
					<input type="hidden" name="orderid" value="{{ $orderId }}">
					<!-- progressbar -->
					@include('frontend.facebook.partials.progressbar')
					<div class="website-setup-dtl">

						@include('frontend.facebook.partials.packages')
						@include('frontend.facebook.partials.details')
						@include('frontend.facebook.partials.domain')
						@include('frontend.facebook.partials.summary')
					</div>
				</form>
			</div>
		</section>
	</div>
	<script type="text/javascript">
        controller.initFacebook();
        $("#header-cart-button").off('click');
	</script>

@endsection
