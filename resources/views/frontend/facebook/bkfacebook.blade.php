@extends('layouts.app')

@section('content')
<div class="website-bnr-inner">
		<div class="container">
			<div class="bnr-inner-cont">
				<h2>Facebook</h2>
			</div>
		</div>
		@include('frontend.includes.cart')
	</div>
<div class="mid-scn-main">
	<section class="website-setupscn">
		<div class="container">
			<form id="msform" class="facebook-setupscn">
				<meta name="csrf-token" content="{{ csrf_token() }}">
				<input type="hidden" name="product" value="facebook">
			  <input type="hidden" name="package" id="package">
			  <input type="hidden" name="orderid" value="{{ $orderId }}">
				<!-- progressbar -->
				<div class="progressbar-main">
					<ul id="progressbar" class="progressbar">
						<li class="active">
							<h2><span id="progressbar-step-1">Select <br>
						Package</span></h2>
						</li>
						<li>
							<h2><span id="progressbar-step-2">Campaign <br>
						Details</span></h2>
						</li>
						<li>
							<h2><span id="progressbar-step-3">Product/ <br>
						Service</span></h2>
						</li>
						<li>
							<h2><span id="progressbar-step-4">Summary</span></h2>
						</li>
					</ul>
				</div>
				<div class="website-setup-dtl">
					<fieldset>
						<div class="fb-selectpackage-main">
							<div class="fb-selectpackage">
								<div class="fb-selectpackage-list">
									<h2>Select Package</h2>
									<span class="fb-businesspackage-sub">Do you have a Facebook Business Page?</span>
									<ul class="radio-group">
										<li>
											<input type="radio" id="fbpackageYes" checked="" name="fb-business-page" value="yes">
											<label for="fbpackageYes">Yes</label>
											<div class="check"></div>
										</li>
										<li>
											<input type="radio" id="fbpackageNo" name="fb-business-page" value="no">
											<label for="fbpackageNo">No</label>
											<div class="check">
												<div class="inside"></div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="package-list fb-package-list" id="fb-package-list-step-1">
								@foreach ($facebookPackage as $package)
								<div class="package-item">
									<h3>Facebook Package {{ $package->id }}</h3>
									<ul>
										@foreach ($package->detail as $detail)
											<li>{{ $detail }}</li>
										@endforeach
										{{-- <li>Setup</li>
										<li>Support</li>
										<li>Budjet R1 000.00</li> --}}
									</ul>
									<div class="package-price">R{{ number_format($package->price) }}</div>
									<div href="#" class=" btn-defualt facebook-package fb-slt-package package-button" data-package="{{ json_encode((array) $package) }}">Select Package</div>
								</div>
								@endforeach
								{{-- <div class="package-item">
									<h3>Facebook Package 2</h3>
									<ul>
										<li>Admin Fee Incl.</li>
										<li>Setup</li>
										<li>Support</li>
										<li>Budjet R1 000.00</li>
									</ul>
									<div class="package-price">R1, 500</div>
									<a href="#" class=" btn-defualt fb-slt-package">Select Package</a> </div>
								<div class="package-item">
									<h3>Facebook Package 3</h3>
									<ul>
										<li>Admin Fee Incl.</li>
										<li>Setup</li>
										<li>Support</li>
										<li>Budjet R1 000.00</li>
									</ul>
									<div class="package-price">R1, 500</div>
									<a href="#" class=" btn-defualt fb-slt-package">Select Package</a> </div> --}}
							</div>
						</div>
						<input type="button" name="next" class="next next-btn action-button"  value="Next: Campaign Details" data-current="campaign-details" />
					</fieldset>
					<fieldset>
						<div class="advt-pdt-service-main" id="facebook-step-2-no">
							<h2>{{-- <span>4: </span> --}}Select Package</h2>
									<div class="select-color-option">
										<ul class="radio-group">
											<li>
												<input type="radio" value="fb-step-2-website-detail-button" id="fb-step-2-website-detail-button" checked name="select-details">
												<label for="fb-step-2-website-detail-button">Use details from website</label>
												<div class="check"></div>
											</li>
											<li>
												<input type="radio" value="fb-step-2-custom-detail-button" id="fb-step-2-custom-detail-button" name="select-details">
												<label for="fb-step-2-custom-detail-button">Enter custom detail</label>
												<div class="check">
													<div class="inside"></div>
												</div>
											</li>
										</ul>
									</div>
								<div class="advt-pdt-service-dtl" id="fb-step-2-website-detail">
								<div class="form-group">
									<label class="inputlabel">Enter Your Email Address</label>
									<input type="text" class="form-control" placeholder="Your Email Address" onfocus="this.placeholder=''" onblur="this.placeholder='Your Email Address'" id="website-detail-email"> <button class="i-email-btn"></button>
								</div>
								<div class="form-group">
									<label class="inputlabel">Enter Your Website URL</label>
									<input type="text" class="form-control" placeholder="www...." onfocus="this.placeholder=''" onblur="this.placeholder='www....'" id="Website-URL">
								</div>
								</div>
								<div class="advt-pdt-service-dtl" id="fb-step-2-custom-detail">
								{{-- <h2>second phase</h2> --}}
								<div class="form-group">
									<label class="inputlabel">Enter Your Email Address</label>
									<input type="text" class="form-control" placeholder="Your Email Address" onfocus="this.placeholder=''" onblur="this.placeholder='Your Email Address'" id="custom-detail-email">
								</div>
								<div class="form-group">
									<label class="inputlabel">Company Name</label>
									<input type="text" class="form-control" placeholder="Company Name" onfocus="this.placeholder=''" onblur="this.placeholder='Company Name'" id="custom-company-name">
								</div>
								<div class="form-group">
                      				<label class="inputlabel">Description</label>
                        			<textarea class="form-control txt-additional-comments" name="comments" id="custom-detail-comments"  placeholder="100 Character" onfocus="this.placeholder=''" onblur="this.placeholder='100 Character'"></textarea>
                      			</div>
                      			<div class="form-group">
									<label class="inputlabel">Upload a Logo</label>
										<div class="upload-btn-wrapper">
											<button class="cont-upload-btn" id="custom-detail-logo">Upload</button>
											<input name="custom-detail-logo" type="file">
										</div>(200 x 200px - Min 150Mb)
                      			</div>
                      				<div class="form-group">
                      			    	<label class="inputlabel">Opening Hours</label>
                      			    	<textarea class="form-control txt-additional-comments" name="comments" id="opening-hours"  placeholder="100 Character" onfocus="this.placeholder=''" onblur="this.placeholder='100 Character	'"></textarea>
                      			    </div>
							</div>
						</div>
						<div class="fb-campaign-details-main" id="facebook-step-2-yes">
				<div class="campaign-details-fix">
						<h2><span>1:</span> Campaign Details</h2>
							<div class="cmp-dtl-checkbox">
								<ul>
										<li>
												<div class="custom_checkbox">
														<input type="checkbox" id="FacebookTraffic" name="FacebookTraffic" value="facebook">
														<label for="FacebookTraffic">Facebook Traffic</label>
												</div>
										</li>
										<li>
												<div class="custom_checkbox">
														<input type="checkbox" id="WebsiteTraffic" name="WebsiteTraffic" value="website">
														<label for="WebsiteTraffic">Website Traffic</label>
												</div>
										</li>
										<li>
												<div class="custom_checkbox">
														<input type="checkbox" id="Both" name="Both" value="['facebook','website']">
														<label for="Both">Both</label>
												</div>
										</li>


								</ul>
							</div>

							<div class="email-url-prt">

									<div class="form-group">
									<label class="inputlabel">Enter Your Email Address</label>
											<input type="text" class="form-control" placeholder="Your email address" onfocus="this.placeholder=''" onblur="this.placeholder='Your email address'" id="campaign-email">
									</div>
									<button class="i-email-btn"></button>

									<div class="form-group">
									<label class="inputlabel">Enter Your Website URL</label>
											<input type="text" class="form-control" placeholder="www...." onfocus="this.placeholder=''" onblur="this.placeholder='www....'" id="campaign-website-URL">
									</div>

							</div>

								<div class="setupcampaign-prt">
									<h2><span>2:</span> Setup your campaign and who to target</h2>

					 				<div class="form-group">
									<label  class="inputlabel">Enter Province, City, Town, or Suburb</label>
											<input type="text" class="form-control" placeholder="Enter country" onfocus="this.placeholder=''" onblur="this.placeholder='Enter country'" id="target-city">
									</div>
									<button class="i-email-btn"></button>

									<div class="form-group">
									<label class="inputlabel">Enter an Age </label>
											<input type="text" class="form-control min-max-control" placeholder="Min age" onfocus="this.placeholder=''" onblur="this.placeholder='Max age'">
											<input type="text" class="form-control min-max-control" placeholder="Max age" onfocus="this.placeholder=''" onblur="this.placeholder='Max age'">
									</div>
									<button class="i-email-btn"></button>
								</div>


							</div>
							</div>
							<input type="button" name="next" class="next next-btn action-button" value="Next: Product/Service" data-current="product-service" />
					</fieldset>
					<fieldset>
						<div id="facebook-step-3-yes">
						<div class="domain-main">
							<div class="domain-list">
								<div class="domain-list-dtl">
									<h2>Advertised Product / Service</h2>
									<div class="form-group">
                      				<label class="inputlabel">Enter the Product and Services you wish to market </label>
                        			<textarea class="form-control txt-additional-comments" name="comments" id="product-and-services-detail"  placeholder="" onfocus="this.placeholder=''" onblur="this.placeholder=''"></textarea>
                      			</div>
								<div class="form-group">
									<label class="inputlabel">Supply your URL</label>
									<input type="text" class="form-control" placeholder="Enter URL" onfocus="this.placeholder=''" onblur="this.placeholder='Enter URL'" id="product-and-services-url">
								</div>
								</div>
							</div>
						</div>
						</div>
						<input type="button" name="next" class="next next-btn action-button" value="Next: Summary" / id="next-summary-button">
						<div id="facebook-step-3-no">
								<div class="fb-slt-summary-main">
								<div class="setuppackage-summary-user">
									<h2>Facebook Setup Package <span class="summary-adit-btn"></span></h2>
									<div class="ctp-dtlform-link">Capture details from <a href="www.yourdomain.co.za" target="_black" id="summary-website-url">www.yourdomain.co.za</a></div>
									<span class="CaptureOr-text">Or</span>
									<div class="summary-agency-user-dtl">
										<div class="summary-agency-pic"><img src="images/summary-agency-user.jpg" alt=""></div>
										<div class="summary-a-user-cont">
											<div class="s-companyname">Company Name: <strong id="summary-company-name"> Your Agency Name</strong></div>
											<p id="summary-detail">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
												<br> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
											<div class="summary-ort-hours">Operating Hours: <strong id="summary-operating-hours">Mon - Fri 8:30 - 17:00</strong></div>
										</div>
									</div>
								</div>
								<div class="package-list fb-package-list">
								@foreach ($facebookPackage as $package)
								<div class="package-item">
									<h3>Facebook Package {{ $package->id }}</h3>
									<ul>
										@foreach ($package->detail as $detail)
											<li>{{ $detail }}</li>
										@endforeach
										{{-- <li>Setup</li>
										<li>Support</li>
										<li>Budjet R1 000.00</li> --}}
									</ul>
									<div class="package-price">R{{ number_format($package->price) }}</div>
									<div href="#" class=" btn-defualt facebook-package fb-slt-package package-button" data-package="{{ json_encode((array) $package) }}">Add to cart</div>
								</div>
								@endforeach
							</div>
								{{-- <div class="package-list fb-package-list">
									<div class="package-item">
										<h3>Facebook Package 1</h3>
										<ul>
											<li>Admin Fee Incl.</li>
											<li>Setup</li>
											<li>Support</li>
											<li>Budjet R1 000.00</li>
										</ul>
										<div class="package-price">R1, 500</div>
										<a href="#" class=" btn-defualt fb-slt-package">Add to cart</a> </div>
									<div class="package-item">
										<h3>Facebook Package 2</h3>
										<ul>
											<li>Admin Fee Incl.</li>
											<li>Setup</li>
											<li>Support</li>
											<li>Budjet R1 000.00</li>
										</ul>
										<div class="package-price">R1, 500</div>
										<a href="#" class=" btn-defualt fb-slt-package">Add to cart</a> </div>
									<div class="package-item">
										<h3>Facebook Package 3</h3>
										<ul>
											<li>Admin Fee Incl.</li>
											<li>Setup</li>
											<li>Support</li>
											<li>Budjet R1 000.00</li>
										</ul>
										<div class="package-price">R1, 500</div>
										<a href="#" class=" btn-defualt fb-slt-package">Add to cart</a> </div>
								</div> --}}
							</div>
							<div class="shopping-check-main" id="continue-shoppig-check-out-button"> <a href="#" class="shopping-check-btn c-shoppig-btn">Continue Shoppig</a> <a href="#" class="shopping-check-btn wb-checkout-btn">Check out</a> </div>
						</div>
					</fieldset>
					<fieldset>
						<div id="facebook-step-4-yes">
							<div class="summary-setup-main summary-Seosetup">
		                    <div class="summary-info-list">
		                      <div class="summary-row" id="package-summary">
		                        {{-- <h2>Package 3<a href="#" class="summary-adit-btn"></a></h2>
		                        <ul>
		                          <li>Admin Fee Incl.</li>
		                          <li>Setup</li>
		                          <li>Support</li>
		                          <li>Budget R5 000.00</li>
		                          <li class="summary-r7">R7 500.00</li>
		                        </ul> --}}
		                      </div>
		                      <div class="summary-row">
		                        <h2>Campain Details <a href="#" class="summary-adit-btn"></a></h2>
		                        <ul>
		                          <li><strong id="summary-traffic">Facebook Traffic</strong></li>
		                          <li><strong>Email </strong><div id="summary-email">kitty@domain.co.za</div></li>
		                          <li><strong>Website URL</strong><div id="summary-website">www.domain.co.za</div></li>
		                          <li><strong>Geotarget</strong>Western Cape, Cape Town
		                            <br> Somerset West</li>
		                          <li><strong>Age</strong>20-25, 30-40</li>
		                          <li><strong>Gender</strong><div id="summary-gender">Both</div></li>
		                        </ul>
		                      </div>
		                      <div class="summary-row">
		                        <h2>Product / Service Details <a href="#" class="summary-adit-btn"></a></h2>
		                        <ul>
		                          <li><span id="summary-reccuring"></span> Get Details From</li>
		                          <li class="summary-r7">www.</li>
		                        </ul>
		                      </div>
		                    </div>
		                  </div>
		                  <div class="seo-summary-addmore">
		                    <div class="fb-selectpackage">
		                      <div class="fb-selectpackage-list">
		                        <h2>Add More</h2>
		                      </div>
		                    </div>
		                    <div class="package-list fb-package-list">
		                      <div class="package-item">
		                        <h3>Facebook Package 1</h3>
		                        <ul>
		                          <li>Admin Fee Incl.</li>
		                          <li>Setup</li>
		                          <li>Support</li>
		                          <li>Budjet R1 000.00</li>
		                        </ul>
		                        <div class="package-price">R1, 500</div>
		                        <a href="#" class=" btn-defualt fb-slt-package seo-package">Select Package</a> </div>
		                      <div class="package-item">
		                        <h3>Facebook Package 2</h3>
		                        <ul>
		                          <li>Admin Fee Incl.</li>
		                          <li>Setup</li>
		                          <li>Support</li>
		                          <li>Budjet R1 000.00</li>
		                        </ul>
		                        <div class="package-price">R1, 500</div>
		                        <a href="#" class=" btn-defualt fb-slt-package seo-package">Select Package</a> </div>
		                      <div class="package-item">
		                        <h3>Facebook Package 3</h3>
		                        <ul>
		                          <li>Admin Fee Incl.</li>
		                          <li>Setup</li>
		                          <li>Support</li>
		                          <li>Budjet R1 000.00</li>
		                        </ul>
		                        <div class="package-price">R1, 500</div>
		                        <a href="#" class=" btn-defualt fb-slt-package seo-package">Select Package</a> </div>
		                    </div>
		                  </div>
		                  <div class="shopping-check-main"> <a href="#" class="shopping-check-btn c-shoppig-btn">Continue Shoppig</a> <a href="#" class="shopping-check-btn wb-checkout-btn">Check out</a> </div>
						</div>
						<div id="facebook-step-4-no">

						</div>

					</fieldset>
{{-- 					<fieldset>
						<div class="summary-setup-main">
							<div class="summary-info-list">
								<div class="summary-row">
									<h2>Package 2 <a href="#" class="summary-adit-btn"></a></h2>
									<ul>
										<li><a href="">4-5 Pages</a></li>
										<li><a href="">Domain</a></li>
										<li><a href="">Hosting</a></li>
										<li><a href="">Selected Own Template</a></li>
										<li><a href="">Responsive</a></li>
									</ul>
								</div>
								<div class="summary-row">
									<h2>Website Info <a href="#" class="summary-adit-btn"></a></h2>
									<ul>
										<li><span class="s-info-txt">Your Email Address:</span>
											<div class="domain-info-dtl email-wb-info">John@domain.co.za</div>
										</li>
										<li><span class="s-info-txt">Copywriting:</span>
											<div class="domain-info-dtl">Yes</div>
										</li>
										<li><span class="s-info-txt">Template:</span>
											<div class="domain-info-dtl">3 Columns</div>
										</li>
										<li><span class="s-info-txt">Colours:</span>
											<div class="domain-info-dtl">
												<div class="domain-color-list"><span class="d-blue-color"></span><span class="d-aquamarine-color"></span><span class="d-yellowlight-color"></span></div>
											</div>
										</li>
									</ul>
								</div>
								<div class="summary-row">
									<h2>Content <a href="#" class="summary-adit-btn"></a></h2>
									<ul>
										<li><a href="">Home Page</a></li>
										<li><a href="">About Us</a></li>
										<li><a href="">Services Page</a></li>
										<li><a href="">Contact Us</a></li>
										<li><a href="">Blog</a></li>
									</ul>
								</div>
							</div>
							<div class="summary-info-list summary-domain-price">
								<div class="summary-row">
									<h2>Domain <a href="#" class="summary-adit-btn"></a></h2>
									<ul>
										<li><a href=""><strong> www.yourdomain.co.za</strong> <br>
									<br>
									</a></li>
										<li>Email Accounts</li>
										<li><a href="">Flopsy@yourdomain.co.za</a></li>
										<li><a href="">Sherry@yourdomain.co.za</a></li>
										<li><a href="">Kitty@yourdomain.co.za</a></li>
										<li><a href="">Crystal@yourdomain.co.za</a></li>
									</ul>
								</div>
								<div class="r-domain-price"><span>R12 000.00</span></div>
							</div>
						</div>
						<div class="shopping-check-main"> <a href="#" class="shopping-check-btn c-shoppig-btn">Continue Shoppig</a> <a href="#" class="shopping-check-btn wb-checkout-btn">Check out</a> </div>
					</fieldset> --}}
				</div>
			</form>
		</div>
	</section>
</div>
@endsection
@section('js')
		<script src="js/jquery.easing.min.js"></script>
		<script src="js/setup.js"></script>
		<script src="js/custom.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function(e) {
			$('li').find('.upload-txt-arrow').click(function(e) {
				$('.upload-txt-arrow').removeClass('upload-txt-current')
				$(this).addClass('upload-txt-current')
				$('.content-editer-comment').removeClass('page-editer-current');
				$(this).next('.content-editer-comment').addClass('page-editer-current');
			});
			//cart process
			var cartOrder = {};
			cartOrder['service'] = 'facebook';
			cartOrder['orderId'] = $("input[name='orderid']").val();
			cartOrder['details'] = {};
			//step 1 selection
			// cartOrder["fb-business-page"] = $("input[name='fb-business-page']:checked").val();
			function hasBusinessPage() {
				var fbbusinesspage = $("input[name='fb-business-page']:checked").val();
				cartOrder["fbBusinessPage"] = fbbusinesspage;
				if(fbbusinesspage == 'yes') {
					$('#fb-package-list-step-1').show();
					$('#progressbar-step-2').html('Campaign <br> Details');
					$('#progressbar-step-3').html('Product/ <br>Service');
					$('#progressbar-step-4').html('Summary');
					$('#facebook-step-2-yes').show();
					$('#facebook-step-2-no').hide();
					$('#facebook-step-3-yes').show();
					$('#facebook-step-3-no').hide();
					$('#continue-shoppig-check-out-button').hide();
					$('#next-summary-button').show();
					$('#facebook-step-4-yes').show();
					$('#facebook-step-4-no').hide();

				}else{
					$('#fb-package-list-step-1, #facebook-step-2-yes').hide();
					$('#progressbar-step-2').html('Details');
					$('#progressbar-step-3').html('Summary');
					$('#progressbar-step-4').html('Select Marketing<br>Package');

					$('#facebook-step-2-no').show();
					$('#facebook-step-3-no').show();
					$('#continue-shoppig-check-out-button').show();
					$('#next-summary-button').hide();
					$('#facebook-step-3-yes').hide();
					$('#facebook-step-4-no').show();
					$('#facebook-step-4-yes').hide();

					// $("input[name='select-details']").click(fbStep2detail);
					// $('#facebook-step-2-yes').hide();
					// $$('#progressbar-step-1').html('Select <br> Package');
				}
			}
			hasBusinessPage();
			$("input[type='radio']").click(hasBusinessPage);
			fbStep2detail();
			$("input[name='select-details']").click(fbStep2detail);
			function fbStep2detail() {
				var fbStep2Detail = $("input[name='select-details']:checked").val();
				if (fbStep2Detail == "fb-step-2-website-detail-button") {
					$('#fb-step-2-website-detail').show();
					$('#fb-step-2-custom-detail').hide();
					cartOrder.details['type'] = 'website details';
					// console.log(cartOrder);
				}
				if (fbStep2Detail == "fb-step-2-custom-detail-button") {
					$('#fb-step-2-custom-detail').show();
					$('#fb-step-2-website-detail').hide();
					cartOrder.details['type'] = 'custom details';
					// console.log(cartOrder);
				}
			}
			// details
			$('#website-detail-email').focusout(function() {
				cartOrder.details['email'] = $(this).val();
			});
			$('#Website-URL').focusout(function() {
				cartOrder.details['websiteurl'] = $(this).val();
				$('#summary-website-url').text($(this).val());
				$('#summary-website-url').attr('href', $(this).val());
			});
			$('#custom-detail-email').focusout(function() {
				cartOrder.details['email'] = $(this).val();
			});
			$('#custom-company-name').focusout(function() {
				cartOrder.details['companyname'] = $(this).val();
				$('#summary-company-name').text($(this).val());
			});
			$('#custom-detail-comments').focusout(function() {
				cartOrder.details['comments'] = $(this).val();
				$('#summary-detail').html($(this).val());
			});
			$('input[name="custom-detail-logo"]').change(function(event) {
				cartOrder.details['logo'] = event.target.files;
				if ($('#custom-detail-logo').text() == 'Upload'){
			 		$('#custom-detail-logo').text('Remove');
	 			}
	 			else{
	 				$('#custom-detail-logo').text('Uplode');
	 			}
			});
			$('#opening-hours').focusout(function() {
				cartOrder.details['openinghours'] = $(this).val();
				$('#summary-operating-hours').html($(this).val());
			});
			// yes detail
			$('#FacebookTraffic').change(function() {
				cartOrder.details['traffic'] = $(this).val();
			});
			$('#WebsiteTraffic').change(function() {
				cartOrder.details['traffic'] = $(this).val();
				$('summary-traffic').html($(this).val()+" Traffic");
			});
			$('#Both').change(function() {
				cartOrder.details['traffic'] = $(this).val();
				var traffic = $.parseJSON($(this).val());
				console.log(traffic);
				$('summary-traffic').html($(this).val()+" Traffic");
			});
			$('#campaign-email').focusout(function() {
				cartOrder.details['email'] = $(this).val();
			});
			$('#campaign-website-URL').focusout(function() {
				cartOrder.details['websiteurl'] = $(this).val();
			});
			// Setup your campaign and who to target PANDING
			//
			// Setup your campaign and who to target PANDING

			$('#product-and-services-detail').focusout(function() {
				cartOrder.details['productAndServicesDetail'] = $(this).val();
			});
			$('#product-and-services-url').focusout(function() {
				cartOrder.details['productandservicesurl'] = $(this).val();
			});
			//package select
			$('.facebook-package').click(function(){
				$('.facebook-package').removeClass('package-button-click');
				$(this).addClass('package-button-click');
				$('#package').val($(this).attr('data-package'));
				var package = $.parseJSON($(this).attr('data-package'));
				cartOrder["package"] = package;
				// package-summary
				var packagesummary = '<h2>Package '+package.id+'<a href="#" class="summary-adit-btn"></a></h2>';
				packagesummary += '<ul>';
				$.each(package.detail, function(i, val){
				  packagesummary += '<li>'+val+'</li>';
				});
				packagesummary += '<li class="summary-r7">R'+package.price.toFixed(2)+'</li>';
				packagesummary += '</ul>';
				$('#package-summary').html(packagesummary);
			});
			// console.log(cartOrder);
			//
			// summary data

			$('.next').click(function() {
				console.log(cartOrder);

				var data_current = $(this).attr('data-current');
				if (data_current == 'campaign-details') {
					if(cartOrder.fbBusinessPage == 'yes'){
						if(!cartOrder.package){
				 			swal("Package", "Please Select Website Package!", "warning");
				 			return false;
				 		}
					}
				}
				if (data_current == 'product-service') {
					if(cartOrder.fbBusinessPage == 'yes'){
						if (!cartOrder.details.traffic) {
							swal("Package", "Please select campaign traffic", "warning");
							return false;
						}else if (!cartOrder.details.email) {
							swal("Package", "Please add your email address!", "warning");
							return false;
						}else if (!cartOrder.details.websiteurl) {
							swal("Package", "Please Add your website.", "warning");
							return false;
						}
						// panding
						// Enter Province, City, Town, or Suburb
						// Enter an Age
						//
					}
					if(cartOrder.fbBusinessPage == 'no'){
						if(cartOrder.details.type == 'website details'){
							// console.log('da');
							if (!cartOrder.details.email) {
								swal("Package", "Please add your email address!", "warning");
	              return false;
							}else if(!cartOrder.details.websiteurl) {
								swal("Package", "Please Add your website.", "warning");
								return false;
							}
						}
						if (cartOrder.details.type == 'custom details') {
							if(!cartOrder.details.email){
								swal("Package", "Please add your email address!", "warning");
	              return false;
							}else if (!cartOrder.details.companyname) {
								swal("Package", "Please add your coupany name.", "warning");
								return false;
							}else if (!cartOrder.details.comments) {
								swal("Package", "Please add description.", "warning");
								return false;
							}else if (!cartOrder.details.openinghours) {
								swal("Package", "Please add your Opening Hours.", "warning");
								return false;
							}
						}
					}
				}


				current_fs = $(this).parent();
			 next_fs = $(this).parent().next();

			 //activate next step on progressbar using the index of next_fs
			 $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

			 //show the next fieldset
			 next_fs.show();
			 //hide the current fieldset with style
			 current_fs.animate({opacity: 0}, {
			 	step: function(now, mx) {
			 		//as the opacity of current_fs reduces to 0 - stored in "now"
			 		//1. scale current_fs down to 80%
			 		scale = 1 - (1 - now) * 0.2;
			 		//2. bring next_fs from the right(50%)
			 		left = (now * 50)+"%";
			 		//3. increase opacity of next_fs to 1 as it moves in
			 		opacity = 1 - now;
			 		current_fs.css({
			       'transform': 'scale('+scale+')',
			       'position': 'absolute'
			     });
			 		next_fs.css({'left': left, 'opacity': opacity});
			 	},
			 	duration: 800,
			 	complete: function(){
			 		current_fs.hide();
			 		animating = false;
			 	},
			 	//this comes from the custom easing plugin
			 	easing: 'easeInOutBack'
			 });
			});

		});

		function changeFs(current_fs_id,show_fs_id) {
				var left, opacity, scale;
				current_fs = $("#"+current_fs_id);
				next_fs = $("#"+ show_fs_id);
				console.log(current_fs);
				console.log(next_fs);
				console.log(show_fs_id);

				//activate next step on progressbar using the index of next_fs
				$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

				//show the next fieldset
				next_fs.show();
				//hide the current fieldset with style
				current_fs.animate({opacity: 0}, {
					step: function(now, mx) {
						//as the opacity of current_fs reduces to 0 - stored in "now"
						//1. scale current_fs down to 80%
						scale = 1 - (1 - now) * 0.2;
						//2. bring next_fs from the right(50%)
						left = (now * 50)+"%";
						//3. increase opacity of next_fs to 1 as it moves in
						opacity = 1 - now;
						current_fs.css({
			        'position': 'relative'
			      });
						next_fs.css({'left': left, 'opacity': opacity, 'transform' : 'none', 'position' : 'relative'});
					},
					duration: 800,
					complete: function(){
						current_fs.hide();
						animating = false;
					},
					//this comes from the custom easing plugin
					easing: 'easeInOutBack'
				});
		};


		</script>
@endsection
