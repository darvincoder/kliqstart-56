<fieldset id="fs_summary">
    <div id="facebook-step-4-yes">
        <div class="summary-setup-main summary-Seosetup">
            <div class="summary-info-list">
                <div class="summary-row" id="package-summary">
                    <h2><span id="summary-package-title">Package 3</span><a href="#" class="summary-adit-btn"></a></h2>
                    <ul id="package-details">
                      <li>Admin Fee Incl.</li>
                      <li>Setup</li>
                      <li>Support</li>
                      <li>Budget R5 000.00</li>
                      <li class="summary-r7">R7 500.00</li>
                    </ul>
                </div>
                <div class="summary-row">
                    <h2>Campain Details <a href="#" class="summary-adit-btn" onClick="changeFs('fs_details')"></a></h2>
                    <ul>
                        <li><strong id="summary-traffic"></strong></li>
                        <li><strong>Email </strong><div id="campain-summary-email">kitty@domain.co.za</div></li>
                        <li><strong>Website URL</strong>
                            <div id="campain-summary-website">www.domain.co.za</div>
                        </li>
                        <li><strong>Geotarget</strong>
                            <div id="campain-summary-target-city">Western Cape, Cape Town Somerset West</div>
                        </li>
                        <li><strong>Age</strong>
                            <span id="campain-summary-target-age-from">20-25</span>
                        </li>
                        <li><strong>Gender</strong><div id="campain-summary-gender">Both</div></li>
                    </ul>
                </div>
                <div class="summary-row">
                    <h2>Product / Service Details <a href="#" onClick="changeFs('fs_domain')" class="summary-adit-btn"></a></h2>
                    <ul>
                        <li id="products-and-services-summary"></li>
                        <li id="products-and-services-url-summary"></li>
                    </ul>
                </div>
            </div>
        </div>
        {{-- <div class="seo-summary-addmore">
          <div class="fb-selectpackage">
            <div class="fb-selectpackage-list">
              <h2>Add More</h2>
            </div>
          </div>
          <div class="package-list fb-package-list">
            <div class="package-item">
              <h3>Facebook Package 1</h3>
              <ul>
                <li>Admin Fee Incl.</li>
                <li>Setup</li>
                <li>Support</li>
                <li>Budjet R1 000.00</li>
              </ul>
              <div class="package-price">R1, 500</div>
              <a href="#" class=" btn-defualt fb-slt-package seo-package">Select Package</a> </div>
            <div class="package-item">
              <h3>Facebook Package 2</h3>
              <ul>
                <li>Admin Fee Incl.</li>
                <li>Setup</li>
                <li>Support</li>
                <li>Budjet R1 000.00</li>
              </ul>
              <div class="package-price">R1, 500</div>
              <a href="#" class=" btn-defualt fb-slt-package seo-package">Select Package</a> </div>
            <div class="package-item">
              <h3>Facebook Package 3</h3>
              <ul>
                <li>Admin Fee Incl.</li>
                <li>Setup</li>
                <li>Support</li>
                <li>Budjet R1 000.00</li>
              </ul>
              <div class="package-price">R1, 500</div>
              <a href="#" class=" btn-defualt fb-slt-package seo-package">Select Package</a> </div>
          </div>
        </div> --}}
        <div class="shopping-check-main">
            <a href="{{ url('/') }}" class="shopping-check-btn c-shoppig-btn">Continue Shopping</a>
            <a id="addtoCartWSstepYes" class="shopping-check-btn wb-checkout-btn">Add to Cart</a>
            <a href="{{ url('/checkout') }}" class="shopping-check-btn wb-checkout-btn">Check out</a>
        </div>
    </div>
    <div id="facebook-step-4-no">

    </div>

</fieldset>