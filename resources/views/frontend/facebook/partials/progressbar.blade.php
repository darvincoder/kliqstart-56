<div class="progressbar-main">
    <ul id="progressbar" class="progressbar">
        <li class="active" id="fb-nav-packages">
            <a href="#"><h2><span id="progressbar-step-1">Select <br>
						Package</span></h2></a>
        </li>
        <li id="nav-details">
            <h2><span id="progressbar-step-2">Campaign <br>
						Details</span></h2>
        </li>
        <li id="nav-domain">
            <h2><span id="progressbar-step-3">Product/ <br>
						Service</span></h2>
        </li>
        <li id="nav-summary">
            <h2><span id="progressbar-step-4">Summary</span></h2>
        </li>
    </ul>

</div>
<div class="back_btn"><a id="previous-btn" data-currentstatus="hi" href="#">Previous</a></div>