<fieldset id="fs_domain">
    <div id="facebook-step-3-yes">
        <div class="domain-main">
            <div class="domain-list">
                <div class="domain-list-dtl">
                    <h2>Advertised Product / Service</h2>
                    <div class="form-group">
                        <label class="inputlabel">Enter the Product and Services you wish to market </label>
                        <textarea class="form-control txt-additional-comments" id="products-and-services"
                        placeholder='Shoes, Cellphones, Website Design, Lawn Mowing Service, Super Hero Outfitting'></textarea>
                    </div>
                    <div class="form-group">
                        <label class="inputlabel">Supply your URL</label>
                        <input type="text" class="form-control" placeholder="http://test.com" id="products-and-services-url">
                    </div>
                </div>
            </div>
        </div>
        <input type="button" name="next" class="next next-btn action-button" value="Next: Summary" id="fb-summary-btn">
    </div>
    <div id="facebook-step-3-no">
        <div class="fb-slt-summary-main">
            <div class="setuppackage-summary-user">
                <h2>Facebook Setup Package <span class="summary-adit-btn"></span></h2>
                <div class="ctp-dtlform-link" id="website-detail-summary">Capture details from <a href="www.yourdomain.co.za" target="_black" id="summary-website-url">www.yourdomain.co.za</a></div>
                <div class="summary-agency-user-dtl" id="custom-detail-summary">
                    <div class="summary-agency-pic"><img src="images/summary-agency-user.jpg" alt="" id="custom-detail-summary-image"></div>
                    <div class="summary-a-user-cont">
                        <div class="s-companyname">Company Name: <strong id="summary-company-name"> Your Agency Name</strong></div>
                        <p id="summary-detail">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            <br> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                        <div class="summary-ort-hours">Operating Hours: <strong id="summary-operating-hours">Mon - Fri 8:30 - 17:00</strong></div>
                    </div>
                </div>
            </div>
            <div id="package-list1" class="package-list fb-package-list">
                @foreach ($facebookPackage as $package)
                    <div class="package-item">
                        <h3>Facebook Package {{ $package->id }}</h3>
                        <ul>
                            @foreach ($package->detail as $detail)
                                <li>{{ $detail }}</li>
                            @endforeach
                            {{-- <li>Setup</li>
                            <li>Support</li>
                            <li>Budjet R1 000.00</li> --}}
                        </ul>
                        <div class="package-price">R{{ number_format($package->price) }}</div>
                        <div href="#" class=" btn-defualt facebook-package fb-slt-package package-button" data-package="{{ json_encode((array) $package) }}">Add to cart</div>
                    </div>
                @endforeach
            </div>
            
        </div>
        <div class="shopping-check-main" id="continue-shoppig-check-out-button">
            <a href="{{ url('/') }}" class="shopping-check-btn c-shoppig-btn">Continue Shopping</a>
            <a id="addtoCartWSstepNo" class="shopping-check-btn wb-checkout-btn">Add to Cart</a>
            <a href="{{ url('/checkout') }}" class="shopping-check-btn wb-checkout-btn">Check out</a>
        </div>
    </div>
</fieldset>