<fieldset id="fs_packages">
	
    <div class="fb-selectpackage-main">
        <div class="fb-selectpackage">
            <div class="fb-selectpackage-list">
                
                <span class="fb-businesspackage-sub">Do you have a Facebook Business Page?</span>
                <ul class="radio-group">
                    <li>
                        <input type="radio" class="have-fb-page" id="fbpackageYes" checked="" name="fb-business-page" value="yes">
                        <label for="fbpackageYes">Yes</label>
                        <div class="check"></div>
                    </li>
                    <li>
                        <input type="radio" class="have-fb-page" id="fbpackageNo" name="fb-business-page" value="no">
                        <label for="fbpackageNo">No</label>
                        <div class="check">
                            <div class="inside"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="fb-package">
            <h2>Select Package</h2>
            <div class="package-list fb-package-list" id="package-list"></div>
            
        </div>
        
    </div>
    <input type="button" name="next" id="fb-pkg-btn" class="next next-btn action-button" value="Next: Campaign Details" data-current="campaign-details" />
</fieldset>