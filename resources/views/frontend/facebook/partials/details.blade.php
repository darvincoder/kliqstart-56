<fieldset id="fs_detail">
    <div class="advt-pdt-service-main" id="facebook-step-2-no">
        <h2>Campaign Details</h2>
        <div class="select-color-option">
            <ul class="radio-group">
                <li>
                    <input type="radio" value="fb-step-2-website-detail-button" id="fb-step-2-website-detail-button" checked class="select-fb-campaign-details" name="select-details">
                    <label for="fb-step-2-website-detail-button">Use details from website</label>
                    <div class="check"></div>
                </li>
                <li>
                    <input type="radio" class="select-fb-campaign-details" value="fb-step-2-custom-detail-button" id="fb-step-2-custom-detail-button" name="select-details">
                    <label for="fb-step-2-custom-detail-button">Enter custom detail</label>
                    <div class="check">
                        <div class="inside"></div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="advt-pdt-service-dtl" id="fb-step-2-website-detail">
            <div class="website-info-email">
                <div class="form-group">
                    <label class="inputlabel">Enter Your Email Address</label>
                    <input type="text" class="form-control" placeholder="Your Email Address" id="use-detail-from-website-email"> <i class="i-email-btn" id="email-modal"></i>
                </div>
            </div>
            <div class="form-group">
                <label class="inputlabel">Enter Your Website URL</label>
                <input type="text" class="form-control" placeholder="http://test.com" id="use-detail-from-website-url">
            </div>
        </div>
        <div class="advt-pdt-service-dtl" id="fb-step-2-custom-detail">
            <div class="form-group">
                <label class="inputlabel">Enter Your Email Address</label>
                <input type="text" class="form-control" placeholder="Your Email Address" id="email1">
            </div>
            <div class="form-group">
                <label class="inputlabel">Company Name</label>
                <input type="text" class="form-control" placeholder="Company Name" id="company-name">
            </div>
            <div class="form-group">
                <label class="inputlabel">Description</label>
                <textarea class="form-control txt-additional-comments" name="description" id="description"  placeholder="100 Character"></textarea>
            </div>
            <div class="form-group">
                <label class="inputlabel">Upload a Logo</label>
                <input type="file" id="file1" name="singleFileUpload" accept="image/*">
 
				<div class="upload-btn-wrapper">
					<span class="cont-upload-btn" data-name="file1" id="single-file-upload">Upload</span>
				</div>(200 x 200px - Min 150Mb)
            </div>
            <div class="form-group">
                <label class="inputlabel">Opening Hours</label>
                <textarea class="form-control txt-additional-comments" id="opening-hours"  placeholder="100 Character"></textarea>
            </div>
        </div>
    </div>
    <div class="fb-campaign-details-main" id="facebook-step-2-yes">
        <div class="campaign-details-fix">
            <h2><span>1:</span> Campaign Details</h2>
            <div class="cmp-dtl-checkbox">
                <ul>
                    <li>
                        <div class="custom_checkbox">
                            <input type="checkbox" class="traffic-from" id="FacebookTraffic" name="FacebookTraffic" value="facebook">
                            <label for="FacebookTraffic">Facebook Traffic</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom_checkbox">
                            <input type="checkbox" class="traffic-from" id="WebsiteTraffic" name="WebsiteTraffic" value="website">
                            <label for="WebsiteTraffic">Website Traffic</label>
                        </div>
                    </li>
                    <li>
                        <div class="custom_checkbox">
                            <input type="checkbox" class="traffic-from" id="Both" name="Both" value='both'>
                            <label for="Both">Both</label>
                        </div>
                    </li>


                </ul>
            </div>

            
            <div class="website-info-email">
                <div class="form-group">
                    <label class="inputlabel">Enter Your Email Address</label>
                    <input type="text" class="form-control" placeholder="Your email address" id="campaign-detail-email" value=@if(Auth::check()) {{ Auth::user()->email }} @endif>
                    <a class="i-email-btn" id="campaign-email-modal"></a>
                </div>
            </div>

            <div class="form-group">
                <label class="inputlabel">Enter Your Website URL</label>
                <input type="text" class="form-control" placeholder="http://test.com" id="campaign-detail-url" value=@if(Auth::check()) {{Auth::user()->userDetail()->first()->website_url}} @endif >
            </div>

            

            <div class="setupcampaign-prt">
                <h2><span>2:</span> Setup your campaign and who to target</h2>

                <div class="website-info-email">
                    <div class="form-group mb-1">
                        <label  class="inputlabel">Enter Province, City, Town, Suburb or Country</label>
                        <input type="text" class="form-control" placeholder="Enter Province, City, Town, Suburb or Country" id="target-city" value="">
                        
                        
                        <span data-name="countries" class="i-add-btn" id="country-add"></span>
                    </div>
                    <div class="tag-list" style="margin-top:  15px;" id="country-tags">
                        
                    </div>
                </div>



                <div class="website-info-email">
                    <div class="form-group">
                        <label class="inputlabel">Enter an Age </label>
                        <input type="number" id="target-age-from" class="form-control min-max-control"  placeholder="Min age">
                        <input type="number" id="target-age-to" class="form-control min-max-control" placeholder="Max age">
                        <span data-name="targetAge" class="i-add-btn" id="age-add"></span>
                    </div>
                    <div class="tag-list age-tag-list" style="margin-top:  15px;" id="age-tags">
                    </div>
                </div>
                <div class="form-group">
                    <label class="inputlabel">Select a Gender</label>
                    <div class="cmp-dtl-checkbox">
                        <ul>
                            <li>
                                <div class="custom_checkbox">
                                    <input type="checkbox" class="genderCheckbox" id="male" name="male" value="male">
                                    <label for="male">Male</label>
                                </div>
                            </li>
                            <li>
                                <div class="custom_checkbox">
                                    <input type="checkbox" class="genderCheckbox" id="female" name="female" value="female">
                                    <label for="female">Female</label>
                                </div>
                            </li>
                            <li>
                                <div class="custom_checkbox">
                                    <input type="checkbox" class="genderCheckbox" id="other" name="other" value="other">
                                    <label for="other">Other</label>
                                </div>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <input type="button" name="next" class="next next-btn action-button" id="fb-detail-btn" value="Next: Product/Service" data-current="product-service" />
</fieldset>