<!DOCTYPE html>
<html>
<head>
    <title>Kliqstart</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="images/favicon.png" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/colorpicker.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/media.css') }}">
    <link rel="stylesheet" media="screen" type="text/css" href="css/colorpicker.css" />
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="js/colorpicker.js"></script>

    <script type="text/javascript" src="{{ asset('js/packagedata.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script type='text/javascript'>
      (function (d, t) {
        var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
        bh.type = 'text/javascript';
        bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=pki80yedwnxoocgjsgxvuq';
        s.parentNode.insertBefore(bh, s);
      })(document, 'script');
    </script>
    @yield('css')
</head>
<body>

  {{-- @if (Session::has('cart') && Request::path() != 'seo' && Request::path() != 'purchase-a-website' && Request::path() != 'google-adwords' && Request::path() != 'facebook')

    {{ dd(Request::path()) }}
  @else
    {{ dd('123') }}
  @endif --}}
    <div class="wrapper">
        <!---Header Section Start Here-->
        <header class="menu">
            <h1 class="logo fl-left"><a href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}" alt=""></a></h1>
            <div class="fl-left menu-main">
                <nav>
                    <ul>
                        <li class="home-icon">
                            <a href="{{ url('/') }}"></a>
                        </li>
                        <li><a href="{{ url('/we-what-offer') }}">What we offer</a></li>
                        <li><a href="{{ url('/who-uses-kliqstart') }}">Who uses kliqstart?</a></li>
                        <li><a href="{{ url('/why-us') }}">Why Us?</a></li>
                        <li><a href="{{ url('/about-us') }}">More about us</a></li>
                        <li><a href="{{ url('/contact-us') }}">Contact US</a></li>

                    </ul>
                </nav>
            </div>

            <div class="cart-search-sign fl-right">
              <div class="cart-list">

                <a href="#" id="header-cart-button">
                  <i class="cart-ion"></i> <span id="header-cart-count"></span>
                </a>

                  @if(!Request::is('purchase-a-website') || !Request::is('google-adwords') || !Request::is('seo') || !Request::is('facebook'))
                    <div class="header-cart headerCartToggal" style="display: none;">

                    <div class="pack-YourCart">
                    	<h4>Your Cart</h4>
                      <div class="pack-YourCart">
                        <ul class="pack-cart ks-top-cart" id="ks-top-cart">
                						{{--<li>--}}
                              {{--<span>Website package 2</span>--}}
                              {{--<span class="purchase-close"></span>--}}

                              {{--<span class="Qtypricec">--}}
                                {{--<strong>Qty: 1</strong>--}}
                              {{--<span class="yourcart-price">R12, 000</span>--}}
                            {{--</li>--}}
                				</ul>
                        <div class="clear"></div>
                          <ul class="cart-total">
                            <li class="carttotal-price">
                              <strong>Total</strong>
                              <span id="yourcart-price" class="total-price">R0</span>
                            </li>
                						<li class="checkout-item">
                              <a href="{{ url('/checkout') }}" class="checkout-btn">Checkout</a>
                            </li>
                        </ul>

                      </div>
                      <div class="clear"></div>

                    </div>


                  </div>
                  @endif
              </div>


<!--                 <button class="hdr-search"></button>
 -->                @if (Auth::guest())
                <a href="{{ url('/login') }}" class="signin-btn"><i></i> Sign In</a>
                @else
                <a href="{{-- url('/myaccount') --}}" class="my-account-btn"><i></i> My Account</a>
                @endif

            </div>
            <span class="menu-toggle"><i></i><i></i><i></i></span>
            <div class="looking-search-popup">
                <div class="looking-search-dtl">
                    <h3>Looking for Something?</h3>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control search-button" placeholder="Search" onfocus="this.placeholder=''" onblur="this.placeholder='Search'" />
                        </div>
                        <button class="btn-common seach-btn">Search</button>
                        <a class="btn-common searchClose-btn">Close</a>
                    </form>
                </div>
            </div>
            @if(Auth::user())
            <div class="payment-login-form" style="display: none">
              <span class="payment-popup-close"></span>
              <div class="payment-user-pic"><img src="@if(Auth::user()->userDetail()->first()->image)assets/userimages/{{ Auth::user()->userDetail()->first()->image }} @else images/default-avatar.png @endif" alt=""></div>
                <div class="payment-login-logout">
                  <div class="payment-username">{{Auth::user()->userDetail()->first()->firstname}} {{Auth::user()->userDetail()->first()->lastname}}</div>
                    <a href="{{ url('/edit') }}" class="loginlink">Edit</a>
                    <a href="{{ url('/logout') }}" class="loginlink">Logout</a>
                </div>
                <div class="payment-list-main">
                <ul class="payment-list">
                  <li><a href="#">Domains</a></li>
                  <li><a href="#">Mail boxes</a></li>
                  <li><a href="#">Billing Payments</a></li>
                  <li><a href="#">Vouchers</a> </li>
                </ul>
                <ul class="payment-list">
                  <li><a href="#">Help Center</a></li>
                  <li><a href="#">Updates & Releases</a></li>
                </ul>
                </div>
            </div>
          @endif
        </header>
        <!--Middle Section Start Here-->
        @yield('content')
        <!--Footer Section Start Here-->
        <footer>
            <div class="ftr-container">
                <div class="ftr-row ftr-company">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="{{ url('/faq') }}">FAQ’s</a></li>
                        <li><a href="{{ url('/terms-of-us') }}">Terms of Use</a></li>
                        <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="ftr-row ftr-account">
                    <h3>Account</h3>
                    <ul>
                        <li><a href="{{ url('/login') }}">Sign In</a></li>
                        <li><a href="{{ url('/register') }}">Sign Up</a></li>
                        <li><a href="{{ url('/password/reset') }}">Reset Password</a></li>
                    </ul>
                </div>
                <div class="ftr-row ftr-weoffer">
                    <h3>What We Offer</h3>
                    <ul>
                        <li><a href="{{ url('/purchase-a-website') }}">Website</a></li>
                        <li><a href="{{ url('/seo') }}">SEO</a></li>
                        <li><a href="{{ url('/google-adwords') }}">PPC</a></li>
                        <li><a href="{{ url('/facebook') }}">Social</a></li>
                        <li><a href="{{ url('/we-offer-hosting') }}">Hosting</a></li>
                    </ul>
                </div>
                <div class="ftr-row ftr-quick">
                    <h3>Quick links</h3>
                    <ul>
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/about-us') }}">About US</a></li>
                        <li><a href="{{ url('/who-uses-kliqstart') }}">How It Works</a></li>
                        <li><a href="{{ url('/accreditations') }}">Accreditations</a></li>
                        <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <section class="ftr-contact-scn">
                <div class="ftr-container">
                    <div class="ftr-contact-dtl">
                        <a href="tel:0800 002 3854" class="ftr-phone"><i></i> 0800 002 3854</a>
                        <a href="mailto:info@kliqstart.co.za" class="ftr-email"><i></i> info@kliqstart.co.za</a>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <script src="{{ asset('js/animation.js') }}"></script>
    <script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/ckeditor.js') }}"></script>
<script src="js/custom.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        $('.ClientsSay').slick({
        infinite: true,
        dots: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }, ]
    });
    </script>
    @yield('js')
</body>

</html>
