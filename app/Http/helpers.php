<?php
/**
 * remoive Keys from array we provided
 * @param  [array] $keys  [list of key we want to remove]
 * @param  [array] $array [Array where from we need to remove]
 * @return [type]        [array]
 */
function removeArrayKeys($keys, $array)
{
	foreach($keys as $key) {
	   unset($array[$key]);
	}

	return $array;
}

/**
 * [changeCheckboxValue 
 * 	Change checkbox value to true or false ]
 * @param [array] $fields          [array of checkbox fields]
 * @param [array] $data            [All data array]
 * @param [Boolean] $useOldValue [set true if you want to use same value of checkbox you give]
 */
function changeCheckboxValue($fields, $data, $useOldValue=false)
{
	foreach ($fields as $field) {
		if(array_key_exists($field, $data)) {
			if ($useOldValue == false) {
				$data[$field] = true;
			}
		} else {
			$data[$field] = false;
		}
	}
	return $data;
}


// for remamber
// Carbon::createFromTimeStamp($a)->toDateTimeString()