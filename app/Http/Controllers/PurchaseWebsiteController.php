<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Model\Package;
use App\Helpers\HelpFunctions;

class PurchaseWebsiteController extends Controller
{
    public function create()
    {
    	$package = new Package;
    	$packages = $package->getPurchaseAWebsite();
    	$orderId = str_random(10);
    	return view('frontend.purchase-a-website.purchase-a-website', compact('packages', 'orderId'));
    }

    public function fileUpload(Request $request)
    {

      if(!empty($request['upload_file'])) {
          $data = HelpFunctions::upload('image', $request['upload_file'], 'uploaded_docs', false);
          if(!$data['success']){
            return response()->json(['status'=>'error','message'=>"Please select file to upload."]);
          }
          $base = "http://localhost:8000";
          $url = $base.'/assets/uploaded_docs/'.$data['url'];
          return response()->json(['status'=>'success','message'=>"", 'url' => $url]);
      } else {
        return response()->json(['status'=>'error','message'=>"Please select file to upload."]);
      }

      // return $request->all();
    }
}
