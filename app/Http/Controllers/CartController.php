<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {

    	$order = json_decode($request->data);
    	if(Session::has('cart')) {
	    	Session::push('cart', $order);
    	}else{
    		Session::put('cart', [$order]);
    	}
      if(!isset($order->package->id)){
        $order->package->id = '';
      }
    	return response()->json(['message'=>'Package add to cart', 'status' => 200, 'data'=>['service'=>$order->service,'packageid'=>$order->package->id,'packageprice'=>$order->package->price,'orderId' => $order->orderId]]);
    }

    public function removeToCart(Request $request)
    {
        $cart = Session::get('cart');
        foreach($cart as $key=>$item){
            if($item->orderId == $request->orderid) {
                $removeKey = $key;
            }
        }
        unset($cart[$removeKey]);
        Session::put('cart', $cart);
        return $request->orderid;
    }
}
