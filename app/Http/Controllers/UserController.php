<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Session;
// use App\Model\Package;
use Auth;
use App\Http\Requests;
use App\Model\User;
use App\Model\UserDetail;
use Hash;
use File;

class UserController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function edit()
    {
    	return view('frontend.user.edit' );
    }

    public function update(Request $request)
    {
      // dd($request->user_image);
      $this->validate($request, [
        'firstname' => 'required',
        'lastname' => 'required',
      ]);
      if($request->get('current_password') != "") {
        $this->validate($request, [
          'password' => 'min:6|confirmed|required'
        ]);
        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
          return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        $request->session()->flash('status', 'Your password is reset.');
      }
      $userDetail = UserDetail::where('user_id', Auth::user()->id)->first();
      $userDetail->firstname = $request->get('firstname');
      $userDetail->lastname = $request->get('lastname');
      $userDetail->businessname = $request->get('businessname');
      // dd($userDetail->image);
      if($request->file('user_image') != null){
        if(!File::isDirectory('assets/userimages/')){
          File::makeDirectory('assets/userimages/',0777,true,true);
        }
        $image = time().$request->user_image->getClientOriginalName();
        // $image = time().$request->user_image->getClientOriginalName().'.'.$request->user_image->getClientOriginalExtension();
        $request->user_image->move(public_path('assets/userimages'), $image);
        if($userDetail->image != null){
          if(File::exists(public_path('assets/userimages').'/'.$userDetail->image)){
            unlink(public_path('assets/userimages').'/'.$userDetail->image);
          }
        }
        $userDetail->image = $image;
      }
      $userDetail->save();
      return redirect()->back();
      // return redirect()->back()->with("success","Password changed successfully !");
      // return $request->all();
    }

    public function removeImage($id)
    {
      $userDetail = UserDetail::where('id', $id)->first();
      unlink(public_path('assets/userimages').'/'.$userDetail->image);
      $userDetail->image = null;
      $userDetail->save();
      return response()->json(['message'=>'image removed', 'status' => 200]);;
    }
}
