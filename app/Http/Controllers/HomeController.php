<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

// use App\Http\Requests;

class HomeController extends Controller
{
    public function wantTo(Request $request)
    {
      $this->validate($request, [
        'want_to' => 'required|not_in:0',
      ]);
      return Redirect::to($request->want_to);
    }
}
