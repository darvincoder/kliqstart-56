<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;

class SubmitController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->all();
        $name = time() . '_' . $request->file->getClientOriginalName();
        $input['file'] = time().'.'.$request->file->getClientOriginalExtension();

        if($request->file->move(('assets/files'), $name)) { 
            return $name;
        }

        return false;
    }

}
