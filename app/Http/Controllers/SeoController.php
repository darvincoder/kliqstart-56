<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Model\Package;

class SeoController extends Controller
{
    public function create()
    {
    	$package = new Package;
    	$packages = $package->getSeo();
        $orderId = str_random(10);
    	// $cartPackage = Session::get('cart');
    	return view('frontend.seo.seo', compact('packages','orderId'));
    	// return view('frontend.seo.seo', compact('packages','cartPackage'));
    }  
}
