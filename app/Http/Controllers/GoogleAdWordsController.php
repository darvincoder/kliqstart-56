<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Adwordlocation;
use App\Model\Adwordlanguage;

use App\Http\Requests;
use AdWordsFacade;
use Response;
// use SchulzeFelix\AdWords\AdWords;
// https://developers.google.com/adwords/api/docs/ratesheet for google rates

class GoogleAdWordsController extends Controller
{
    public function getForm()
    {
    	$orderId = str_random(10);
    	return view('frontend.google-adwords.google-adwords',compact('orderId'));
    }
    /*
    * Please make sure that the storage folder has been linked if not;
    *  type in the console "php artisan storage:link".
    *
    *
    *
    */
    // public function starter(Request $request){
    //     $location = $request->input('modifiedLocation');
    //     $language = $request->input('language');
    //     // this array includes all the keywords from the front end.
    //     $keywordsSelected = explode(",", $request->input('keywordArray'));
    //
    //     $search = \SchulzeFelix\AdWords\AdWordsFacade::location($location)->language(1001)->keywordIdeas(['a']);
    //     $customerSearch = \SchulzeFelix\AdWords\AdWordsFacade::location($location)->language($language)->keywordIdeas(['a']);
    //     $searchVolumes = \SchulzeFelix\AdWords\AdWordsFacade::searchVolumes($keywordArray);
    //
    //
    //     return response()->json([
    //             'PrimarySearch'=> $search,
    //             'modifiedSearch' => $customerSearch,
    //             'keywordsSelected' => $keywordsSelected,
    //             ]);
    //     }
    public function getSearchVolume(Request $request)
    {
      // dd($request->data);
      $data = $request->data;
      $totalCount = 0;
      // dd($data['keyword']);
      $search_volumes = AdWordsFacade::withTargetedMonthlySearches();
      // $data['location'] string eg Parent ID,Parent ID
      if (isset($data['language'])) {
        $search_volumes->language($data['language']);
      }
      if(isset($data['location'])) {
        $search_volumes->location($data['location']);
      }
      if(isset($data['keyword'])) {
        $cpc = [];
        $search_volumes = $search_volumes->searchVolumes($data['keyword'])->toArray();
        foreach ($search_volumes as $search_volume) {
          array_push($cpc, ['keyword'=>$search_volume['keyword'],'cpc'=>$search_volume['cpc']]);
          $totalCount += $search_volume['targeted_monthly_searches'][0]['count'];
        }
        return Response::json(['status'=>true, 'count'=> $totalCount, 'cpc'=>$cpc]);
      }else{
        return Response::json(['status'=>false, 'error'=> 'Please add keywords']);
      }
    }

    public function getLocations(Request $request)
    {
      // dd($request->all());
      // echo "darvin";
      $locations = Adwordlocation::where('canonicalName', 'like', '%'.$request->term.'%')->get();
      return Response::json($locations);
    }

    public function getLanguage(Request $request)
    {
      $language = Adwordlanguage::where('LanguageName', 'like', '%'.$request->term.'%')->get();
      return Response::json($language);
    }
}
