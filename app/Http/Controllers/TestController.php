<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Package;
use App\Model\Adwordlocation;
use AdWordsFacade;
use Response;

class TestController extends Controller
{
    // public function __construct()
    // {
    // 	$this->middleware('auth');
    // }

    // public function myAccount()
    // {
    // 	return view('frontend.myaccount');
    // }
    public function index()
    {
      // location(2276)->language(1001)->
      // return Adwordlocation::all();
      return AdWordsFacade::withTargetedMonthlySearches()->location(1000281,1000322)->searchVolumes(['website development in india']);
      return AdWordsFacade::withTargetedMonthlySearches()->searchVolumes(['cheesecake', 'coffee']);
    	// $package = new Package;
    // return $package->getFacebookPackage();
    	# code...
    }

    public function getSearchVolume(Request $request)
    {
      // dd($request->data);
      $data = $request->data;
      $totalCount = 0;
      // dd($data['keyword']);
      $search_volumes = AdWordsFacade::withTargetedMonthlySearches();
      // $data['location'] string eg Parent ID,Parent ID
      if (isset($data['language'])) {
        dd('dada');
        $search_volumes->language($data['language']);
      }
      if(isset($data['location'])) {
        dd('dsds');
        $search_volumes->location($data['location']);
      }
      if(isset($data['keyword'])) {
        $search_volumes = $search_volumes->searchVolumes($data['keyword'])->toArray();
        foreach ($search_volumes as $search_volume) {
          $totalCount += $search_volume['targeted_monthly_searches'][0]['count'];
        }
        return Response::json(['status'=>true, 'count'=> $totalCount]);
      }else{
        return Response::json(['status'=>false, 'error'=> 'Please add keywords']);
      }
    }
}
