<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Session;
use App\Model\User;
use App\Model\UserDetail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Carbon\Carbon;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';
    protected $loginView = 'frontend.auth.login';
    protected $registerView = 'frontend.auth.register';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'mobilenumber' => 'size:10',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        //$dateofbirth = Carbon::createFromFormat('Y/m/d', $data['year']."/".$data['month']."/".$data['day']);
        // $dateofbirth = Carbon::createFromFormat('Y/m/d', $data['year']."/".$data['month']."/".$data['day'])->timestamp;
        //dd($data);

        if ( !$data['year'] || !$data['month'] || !$data['month']) {
            //die('no date selected');
            $data['dateofbirth'] = null;
        } else {
           $data['dateofbirth'] = Carbon::createFromFormat('Y/m/d', $data['year']."/".$data['month']."/".$data['day']);
        }

        $data = removeArrayKeys(['email','password','password_confirmation', 'year', 'month', 'day'],$data);
        $data['user_id'] = $user->id;

        $data = changeCheckboxValue(['newsletter','sms'],$data);

        UserDetail::create($data);

        return $user;
    }

    /**
     * Overrides method in class 'AuthenticatesUsers'
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        $view = property_exists($this, 'loginView')
            ? $this->loginView : 'auth.authenticate';
        if (view()->exists($view)) {
            return view($view);
        }
        /**
         * seve the previous page in the session
         */
        $previous_url = Session::get('_previous.url');
        $ref = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        $ref = rtrim($ref, '/');
        if ($previous_url != url('login')) {
            Session::put('referrer', $ref);
            if ($previous_url == $ref) {
                Session::put('url.intended', $ref);
            }
        }
        /**
         * seve the previous page in the session
         * end
         */
        return view('auth.login');
    }
    /**
     * Overrides method in class 'AuthenticatesUsers'
     *
     * @param Request $request
     * @param $throttles
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }
        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::guard($this->getGuard())->user());
        }
        /*return to the previous page*/
        return redirect()->intended(Session::pull('referrer'));
//        return redirect()->intended($this->redirectPath()); /*Larevel default*/
    }
}
