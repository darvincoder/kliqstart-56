<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Model\Package;

use App\Http\Requests;

class FacebookController extends Controller
{
    public function create()
    {
    	$package = new Package;
    	$facebookBusinessPageSetupPackage = $package->getFacebookBusinessPageSetupPackage();
    	$facebookPackage = $package->getFacebookPackage();
    	$orderId = str_random(10);
    	return view('frontend.facebook.facebook', compact('facebookBusinessPageSetupPackage', 'facebookPackage', 'orderId'));
    }
}
