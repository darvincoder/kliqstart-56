<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $guarded = [];

    /**
     * get user email
     */
    public function user()
    {
    	return $this->belongsTo('App\Model\User');
    }
}
