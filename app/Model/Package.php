<?php

namespace App\Model;

/**
* manage all packages for purchases
*/
class Package
{
	
	protected $path;
	protected $packages;
	protected $error;

	public function __construct()
	{
		$this->path = storage_path('package/package.json');
		$this->packages = json_decode(file_get_contents($this->path));
		// $this->error = json_last_error();
	}

	public function index()
	{
		return $this->path;
	}

	public function getJsonFile()
	{
		return $this->packages;
	}

	public function getSeo()
	{
		return $this->packages->seo;
	}

	public function getFacebook()
	{
		$package = $this->packages->facebook;
		return $package;

	}

	public function getFacebookBusinessPageSetupPackage()
	{
		foreach ($this->getFacebook() as $package) {
			if ($package->id == 'setup') {
				return $package;
			}
		}
	}

	public function getFacebookPackage()
	{
		foreach ($this->getFacebook() as $key => $package) {
			if($package->id == 'setup') {
				$allFbPackage = $this->getFacebook();
				unset($allFbPackage[$key]);
				return $allFbPackage;
			}
		}
		return $package;
	}

	public function getPurchaseAWebsite()
	{
		return $this->packages->purchasewebsite;
	}
}