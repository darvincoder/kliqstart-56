<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Adwordlanguage extends Model
{
    protected $table = 'adword_languages';
    public $timestamps = false;
}
