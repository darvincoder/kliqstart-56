<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Adwordlocation extends Model
{
    protected $table = 'adword_locations';
    public $timestamps = false;
}
