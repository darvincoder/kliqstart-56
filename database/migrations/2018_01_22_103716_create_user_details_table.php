<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('image')->nullable();
            $table->text('firstname')-> nullable();
            $table->text('lastname')-> nullable();
            $table->string('businessname')-> nullable();
            $table->string('website_url')-> nullable();
            $table->bigInteger('mobilenumber')-> nullable();
            $table->date('dateofbirth')-> nullable();
            $table->boolean('newsletter')->default(false);
            $table->boolean('sms')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }
}
