var packageData ={
	
		"seo" : [
		{
			"id": 1,
			"name": "Seo Package 1",
			"detail": [
					"Admin Fee Incl.",
					"Setup",
					"Support",
					"Budget R 1000.00"
				],
			"price": 1000

		},
		{
			"id": 2,
			"name": "Seo Package 2",
			"detail": [
					"Admin Fee Incl.",
					"Setup",
					"Support",
					"Budget R 1500.00"
				],
			"price": 1500

		},
		{
			"id": 3,
			"name": "Seo Package 3",
			"detail": [
					"Admin Fee Incl.",
					"Setup",
					"Support",
					"Budget R 2000.00"
				],
			"price": 2000

		},
	],



	"web" : [
		{
			"id": 1,
			"name": "Website Package 1",
			"detail": [
					"Admin Fee Incl.",
					"Setup",
					"Support",
					"Budget R 1000.00"
				],
			"price": 1000

		},
		{
			"id": 2,
			"name": "Website Package 2",
			"detail": [
					"Admin Fee Incl.",
					"Setup",
					"Support",
					"Budget R 1500.00"
				],
			"price": 1500

		},
		{
			"id": 3,
			"name": "Website Package 3",
			"detail": [
					"Admin Fee Incl.",
					"Setup",
					"Support",
					"Budget R 2000.00"
				],
			"price": 2000

		},
	],

    "facebook" : [
        {
            "id": 1,
            "name": "Facebook Package 1",
            "detail": [
                "FB pageAdmin Fee Incl.",
                "Setup",
                "Support",
                "Budget R 1000.00"
            ],
            "price": 13000

        },
        {
            "id": 2,
            "name": "Facebook Package 2",
            "detail": [
                "Admin Fee Incl.",
                "Setup",
                "Support",
                "Budget R 1500.00"
            ],
            "price": 1500

        },
        {
            "id": 3,
            "name": "Facebook Package 3",
            "detail": [
                "Admin Fee Incl.",
                "Setup",
                "Support",
                "Budget R 2000.00"
            ],
            "price": 2000

        },
    ]
}
