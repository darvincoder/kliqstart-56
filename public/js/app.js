// localStorage.removeItem('orders');

var errors = {
    email: function(param) {
        return "Please enter valid email id";
    },
    require: function(param) {
        return param + " is required";
    },
    blank: function(param) {
        return param + " cannot be blank";
    },
    campaignPeriod: function() {
        return "Please select campaign period";
    }
};

var KsController = (function() {

    var PackageObj = {
        comment: {
            home: "",
            services: "",
            about: "",
            contact: ""
        },
        domain: "www.coderadobe.com",
        domainEmails: ["test", "test"],
        label: {
            home: "Home Page ",
            services: "Services Page ",
            about: "About Us Page ",
            contact: "Contact Us Page "
        },
        type: 'google',
        email: 'brijeshmkt@gmail.com',
        packageId: null,
        products: [],
        countries: [],
        period: 0,
        packagePrice: 0,
        price: 0,
        url: 'thisIsblank.com',
        gender: 'male',
        isp: '',
        ispOther: '',
        copywriting: 'yes',
        webtemplate: null,
        colourScheme: 'color-schema-option-2',
        selectedColorSchema: '',
        customSchema: '',
        haveDomain: 'yes',
        haveFbpage: 'yes',
        trafficFrom: [],
        targetAge: [],
        targetGender: [],
        fbUseDetailFromWebsite: 'yes',
        fbEnterCustomDetail: '',
        email1 : '',
        companyName : 'Coder Adobe',
        description : '100 Character',
        openingHours : '10 am to 12 pm',
        productsAndServices: 'Shoes, CellPhones, website Designs',
        productsAndServicesUrl: 'http://test.com',
        previous: {"adwords": ['step1', 'step2', 'step3', 'step4', 'step5', ]},
        file1: '1527740107_ihg1.png',
        keywords: [],
        language: [],
        googleService: [],
        googleBusiness: [],
        googleCompetitors: [],
        googleExtraDetails: [],
        campaignRunMonth: 0,
        campaignRunTime: '',
        showTheAd: [],
        budget: 0,
        googleLocation: []
    };


    var calculatePrice = function() {
        let db = KsController.readPkg();
        db.price = db.period * db.packagePrice;
        KsController.savePkg(db);
    };

    var addToCart = function(dbObject) {
        console.log("cart aDD");
        let orders = localStorage.getItem('orders');

        if (orders === null) {
            orders = [];
        } else {
            orders = JSON.parse(orders);
        }
        orders.push(dbObject);
        orders = JSON.stringify(orders);
        localStorage.setItem('orders', orders);
        window.location.href = "/thank-you/"+KsController.readPkg().type;
    };

    var savePkg = function(pkg) {
        var pkg = JSON.stringify(pkg);
        localStorage.setItem('pkg', pkg);
    };


    return {
        data: PackageObj,
        update: function(param, value) {
            PackageObj[param] = value;
        },
        savePkg: function(pkg) {
            savePkg(pkg);
        },
        readPkg: function() {
            mypkg = localStorage.getItem('pkg');
            currentPkg = JSON.parse(mypkg);
            return currentPkg;
        },
        setType: function(param) {
            PackageObj.type = param;
        },
        addTag: function(value, objectField) {
            let db = this.readPkg();
            db[objectField].push(value);
            this.savePkg(db);
        },
        deleteTag: function(tag, fieldName) {
            let db = this.readPkg();
            var tagArr = db[fieldName];
            tagArr.splice(tagArr.indexOf(tag), 1);
            this.savePkg(db);
        },
        addToCart: function(dbObject) {
            addToCart(dbObject);
        },
        calculatePrice: function() {
            calculatePrice();
        },

        initSaveCart: function() {
            saveCart();
        },

        test: function() {
            return PackageObj;
        },
        cart: function() {
            return cart;
        }
    }

})();

var KsUIController;
KsUIController = (function () {

    var type = '';

    var DOMstrings = {
        seoProductTags: 'productsandservices-tags',
        seoEmail: 'email',
        seoUrl: 'url',
        seoGender: 'gender',
        seoProductTag: 'productsandservices',
        seoComment: 'comments',
        seoTagInput: 'productsandservices',
        seoEditPkg: 'seo-edit-pkg',
        seoEditDetails: 'seo-edit-details',
        seoEditRecurring: 'seo-edit-recurring',
        headerCartCount: 'header-cart-count',
    };

    var seoSection = {
        packages: 'fs_packages',
        details: 'fs_detail',
        recurring: 'fs_recurring',
        summary: 'fs_summary',
    }

    var loadWebSummary = {
        packages: 'fs_packages',
        details: 'fs_detail',
        recurring: 'fs_recurring',
        summary: 'fs_summary',
    }

    var addActiveClass = function (eleId) {
        (document.getElementById(eleId)) ? document.getElementById(eleId).className += ' active' : '';
    };

    var selectTemplateColor = function () {
        $('#main-color-picker').ColorPicker({
            flat: false,
            onChange: function (hsb, hex, rgb) {
                $('.colorpicker-img#main-color-picker').css('background-color', '#' + hex);
                $('#main-color-hex').val(hex);
                $('.colorpicker-img#main-color-picker').children().remove();
            }
        });
        $('#secondary-color-picker').ColorPicker({
            flat: false,
            onChange: function (hsb, hex, rgb) {
                $('.colorpicker-img#secondary-color-picker').css('background-color', '#' + hex);
                $('#secondary-color-hex').val(hex);
                $('.colorpicker-img#secondary-color-picker').children().remove();
            }
        });
        $('#calltoaction-color-picker').ColorPicker({
            flat: false,
            onChange: function (hsb, hex, rgb) {
                $('.colorpicker-img#calltoaction-color-picker').css('background-color', '#' + hex);
                $('#calltoaction-color-hex').val(hex);
                $('.colorpicker-img#calltoaction-color-picker').children().remove();
            }
        });
        $('#miscellaneous-color-picker').ColorPicker({
            flat: false,
            onChange: function (hsb, hex, rgb) {
                $('.colorpicker-img#miscellaneous-color-picker').css('background-color', '#' + hex);
                $('#miscellaneous-color-hex').val(hex);
                $('.colorpicker-img#miscellaneous-color-picker').children().remove();
            }
        });
    };

    var displayPackages = function (type) {

        let packages = packageData[type];

        let html = '';

        for (var i = 0; i <= packages.length - 1; i++) {
            html += '<div class="package-item"><h3>' + packages[i].name + '</h3><ul>';
            for (var j = 0; j <= packages[i].detail.length - 1; j++) {
                html += '<li>' + packages[i].detail[j] + '</li>';
            }
            html += '</ul>';
            html += '<div class="package-price">R ' + packages[i].price + '</div>';
            html += '<div id="' + packages[i].id + '" class="btn-defualt fb-slt-package select-this-one select-package get-package" data-name="packageId">Select Package</div>';
            html += '</div>';
        }
        ;
        return html;
    };

    var hideOnLoad = function () {
        checkDom(document.getElementById('step-3-content-1')) ? document.getElementById('step-3-content-1').style.display = 'none' : '';
        checkDom(document.getElementById('step-3-content-2')) ? document.getElementById('step-3-content-2').style.display = 'none' : '';
    }

    var addClickEventToRemoveCartItem = function () {
        var cartItems = document.getElementsByClassName("purchase-close");

        for (var i = 0; i <= cartItems.length - 1; i++) {
            cartItems[i].addEventListener('click', function (event) {
                ids = event.target.id.split('--');
                document.getElementById(event.target.id).parentNode.style.display = 'none';
                removeItemFromCart(ids[1]);
                displayTotalPrice();
            });
        }
    };

    var displayTotalPrice = function () {
        if (document.getElementById('yourcart-price')) {
            document.getElementById('yourcart-price').innerText = 'R ' + calculateTotalPrice();
        }
        if (document.getElementById('page-yourcart-price')) {
            document.getElementById('page-yourcart-price').innerText = 'R ' + calculateTotalPrice();
        }
    };

    var removeItemFromCart = function (id) {
        let orders = JSON.parse(localStorage.getItem('orders'));
        orders.splice(id, 1);
        orders = JSON.stringify(orders);
        localStorage.setItem('orders', orders);
    };

    var getOrders = function () {
        if (localStorage.getItem('orders') === null) {
            orders = [];
        } else {
            orders = JSON.parse(localStorage.getItem('orders'));
        }

        return orders;
    }

    var calculateTotalPrice = function () {
        let orders = getOrders();
        let total = 0;

        for (var i = 0; i <= orders.length - 1; i++) {
            total = parseInt(orders[i].price) + total;
        }
        return total;
    }

    var tagsToHTML = function (tags, fieldName) {
        var i, html = '',
            tags;

        //console.log(tags);
        if (tags.length) {
            for (i = 0; i <= tags.length - 1; i++) {
                html += '<span id="tag-' + [i] + '" class="tag">' + tags[i] + '<span data-name="' + fieldName + '" class="tag-close"></span></span>'
            }
        }
        return html;
    }

    var cartCount = function () {
        if (localStorage.getItem('orders') === null) {
            console.log('local storage is missing');
            orders = [];
        } else {
            orders = JSON.parse(localStorage.getItem('orders'));

        }

        document.getElementById('header-cart-count').innerHTML = orders.length;
    };

    var displayCartProducts = function () {
        var cartHtml = '';
        let orders = getOrders();
        let html = '';

        if (orders.length > 0) {
            for (var i = 0; i < orders.length; i++) {
                html += '<li>';
                html += '<span class="websitepackdata">' + orders[i].type + ' package ' + orders[i].packageId + '</span> <span class="purchase-close purchase-close--' + [i] + '" id="purchase-close--' + [i] + '"></span><br><strong>Qty: 1</strong> <span class="yourcart-price">R' + orders[i].price + '</span>';
                html += '</li>';
            }
        }
        if (document.getElementById('ks-top-cart') !== null) {
            document.getElementById('ks-top-cart').innerHTML = html;
        }
        if (document.getElementById('ks-page-cart') !== null) {
            document.getElementById('ks-page-cart').innerHTML = html;
        }
    };

    var checkIfDomIsLoaded = function (param) {
        let dom = document.getElementById(param);

        if (dom === null) {
            return null;
        } else {
            return dom.value;
        }


    };

    var checkDom = function (param) {
        let dom = document.getElementById(param);

        if (dom === null) {
            return null;
        } else {
            return dom;
        }
    };

    // Get element By Id
    var getEle = function (ele) {
        if (document.getElementById(ele)) {
            return document.getElementById(ele);
        }
    };

    var displayPackage = function (dbObject) {
        let packageId, type, package;
        var  details, summaryPackage;

        packageId = dbObject.packageId;
        type = dbObject.type;
        package = packageData[type];
        if (!packageId) {
            return false;
        }

        package.forEach(function (current) {
            // console.log(packageId);
            if (current.id === parseInt(packageId)) {
                summaryPackage = current;
            }
        });
        // console.log(package);
        details = summaryPackage.detail;
        var str = '';
        for (var i = 0; i < details.length; i++) {
            str += '<li>' + details[i] + '</li>';
        }
        ;
        str += '<li class="summary-r7">R ' + summaryPackage.price + '</li>';

        document.getElementById('package-details').innerHTML = str;
        document.getElementById('summary-package-title').innerHTML = "Package " + packageId;
    };
    var packagereadPkg = function () {
        return packageData;
    };

    return {
        clearInputEleId: function (eleId) {
            document.getElementById(eleId).value = '';
        },
        getEle: function (ele) {
            return getEle(ele);
        },
        displayPackages: function (packageType) {
            return displayPackages(packageType);
        },
        addActiveClass: function (eleId) {
            addActiveClass(eleId);
        },
        displayCart: function () {
            displayCartProducts();
            cartCount();
            addClickEventToRemoveCartItem();
            displayTotalPrice();

        },
        displayCartProducts: function () {
            console.log("display cart");
            displayCartProducts();
            displayTotalPrice();
        },
        getSeoFormDetails: function () {
            return {
                email: (checkIfDomIsLoaded(DOMstrings.seoEmail)),
                url: (checkIfDomIsLoaded(DOMstrings.seoUrl)),
                gender: (checkIfDomIsLoaded(DOMstrings.seoGender)),
                tag: (checkIfDomIsLoaded(DOMstrings.seoProductTag)),
                comment: (checkIfDomIsLoaded(DOMstrings.seoComment)),
                tagInput: (checkIfDomIsLoaded(DOMstrings.seoComment)),
            }
        },
        getWebForm: function () {
            return {
                email: document.getElementById('email').value,
            }
        },
        getInputValue: function (param) {
            if (document.getElementById(param) !== null) {
                return document.getElementById(param).value;
            }
        },
        getDomEle: function () {
            return {
                seoEditPkg: document.getElementById(DOMstrings.seoEditPkg),
                seoEditDetails: document.getElementById(DOMstrings.seoEditDetails),
                seoEditRecurring: document.getElementById(DOMstrings.seoEditRecurring),
                displayCartCount: document.getElementById(DOMstrings.headerCartCount),

            }
        },
        commonDomEle: function (type) {
            loadCommonDomElement(type);
        },
        webDomEle: function () {
            selectTemplateColor();
            hideOnLoad();
        },
        displayTags: function (tags, eleId, fieldName) {
            var html = tagsToHTML(tags, fieldName);
            if (tags.length > 0) {
            document.getElementById(eleId).innerHTML = html;
            }
        },

        checkIfDomIsLoaded: function (param) {
            return checkIfDomIsLoaded(param);
        },
        checkDom: function (param) {
            return checkDom(param);
        },

        displayPackage: function (db) {
            displayPackage(db);
        },

        loadWebSummary: function () {

            console.log('web summery page');
            let db = KsController.readPkg();

            // var summaryPackage;
            displayPackage(db);
            console.log(db.price);
            document.getElementById('summary-price').innerHTML = 'R' + KsController.readPkg().price;

            document.getElementById('summary-email').innerHTML = db.email;
            document.getElementById('summary-copywriting').innerHTML = db.copywriting;
            document.getElementById('summary-template').innerHTML = db.webtemplate;
            var summeryDomain = '<li>' + db.domain + '</li>';
            if(db.domainEmails.length !== 0) {
                db.domainEmails.forEach(function (email) {
                    summeryDomain += '<li>'+ email + '</li>';
                });
            }
            document.getElementById('display-domain-summary').innerHTML = summeryDomain;
        },


        displaySeoSummery: function (dataObj) {

            if (dataObj) {
                packageId = dataObj.packageId;
                displayPackage(dataObj);
                document.getElementById('summary-email').innerHTML = dataObj.email;
                document.getElementById('summary-website').innerHTML = dataObj.url;
                document.getElementById('summary-gender').innerHTML = dataObj.gender;
                document.getElementById('summary-products').innerHTML = dataObj.products;
                document.getElementById('summary-reccuring').innerHTML = dataObj.period;

            }
        },
        hide: function (eleId) {
            if (document.getElementById(eleId)) {
                document.getElementById(eleId).style.display = 'none';
            }
        },
        show: function (eleId) {
            if (document.getElementById(eleId)) {
                document.getElementById(eleId).style.display = 'block';
            }
        },
        seosection: function () {
            return seoSection;
        },
        WebSummary: function () {
            return loadWebSummary;
        },
        addClick: function (eleId, callbackFunction) {
            if (document.getElementById(eleId) !== null) {
                document.getElementById(eleId).addEventListener('click', callbackFunction);
            }
        },
        DOMstrings: DOMstrings
    }
})();

var validation = (function() {
    return {
        email: function(email) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == false) {
                return false;
            }
            return true;
        },
        require: function(stringValue) {
            return stringValue.length > 0 ? true : false;
        },
        test: function() {
            return "Hello Me";
        }
    }
})();

var controller = (function(validation, ks, UI, $) {


    errorsArr = [];



    var setupEventListner = function() {
        ks.setType("seo");
        // $('#seo-pkg-btn').on('click', sectionPkg); // Step 1
        // $('.select-package').on('click', selectPkg);

        // Step 2 Details
        // $('input[name="isp"]').change(function () {
        //     alert("isp");
        //     let isp = selectedISP(isps);
        // });
        $('#seo-detail-btn').on('click', function() {
            let email,url,gender,comment;
            clearToGo = true;
            let db = ks.readPkg();
            email = UI.getInputValue('email');
            (validation.email(email)) ? db.email = email : customAlert("email");

            url = UI.getInputValue('url');
            (validation.require(url)) ? db.url = url : customAlert("require","url");

            gender = UI.getInputValue('gender');
            db.gender = gender;

            var isps = document.getElementsByName('isp');
            var isp = selectedISP(isps);
            db.isp = isp;

            comment = UI.getInputValue('comments');
            db.comment = comment;

            if (clearToGo) {
                console.log();
                ks.savePkg(db);
                let goToSection = 'recurring';
                console.log('btn clicked');
                displaySection(goToSection);
                UI.getEle('previous-btn').dataset.currentstatus = goToSection;
                // $('#fs_detail').toggle("slow");
                // $('#fs_recurring').toggle("slow");
                document.getElementById('seo-nav-recurring').className += ' active';
            }
        });


        $('#productsandservices-add').on('click', function (event) {
            addTag(event);
            seoProductTags();
        });
        seoProductTags();
        if (document.querySelector('.tag-list') !== null) {
            document.querySelector('.tag-list').addEventListener('click', deleteTag);
        };

        if (document.querySelector('#seo-recurring-btn') !== null) {
            document.querySelector('#seo-recurring-btn').addEventListener('click', campaignPeriod);
        };

        $('.run-month-btn').on('click', selectCampaignPeriod);
    };

    var seoSectionLoadOnInit = function () {
        console.log('loading Seo');
        (UI.getEle('package-list')) ? UI.getEle('package-list').innerHTML = UI.displayPackages("seo") : '';
        if (UI.checkDom('seo-pkg-btn') !== null) {
            UI.checkDom('seo-pkg-btn').addEventListener('click', sectionPkg);

        }
        if (document.getElementsByClassName("select-package") !== null) {
            setEventListenerOnClass("select-package", selectPkg);
        }
        // UI.addClick('previous-btn', previous);
    }

    var webSectionLoadOnInit = function() {
        console.log('loading web');
        (UI.getEle('package-list')) ? UI.getEle('package-list').innerHTML = UI.displayPackages("web") : '';
        if (UI.checkDom('web-pkg-btn') !== null) {
            UI.checkDom('web-pkg-btn').addEventListener('click', sectionPkg);
        }
        if (document.getElementsByClassName("select-package") !== null) {
            setEventListenerOnClass("select-package", selectPkg);
        };
        // Website Info section
        if (UI.checkDom('web-webinfo-btn') !== null) {
            UI.checkDom('web-webinfo-btn').addEventListener('click', webInfoSection);
        }
        if (document.getElementsByClassName("template-select") !== null) {
            setEventListenerOnClass("template-select", selectTemplate);
        };
        if (document.getElementsByClassName("select-colour") !== null) {
            setEventListenerOnClass("select-colour", colorOption);
            if (UI.checkDom('customize-own-colours-section') !== null) {
                document.getElementById('customize-own-colours-section').style.display = 'none';
            }
        };
        if (document.getElementsByClassName("get-color") !== null) {
            setEventListenerOnClass("get-color", selectThis);
        };

        // Content section
        if (document.getElementsByClassName("websitecontent-radio") !== null) {
            setEventListenerOnClass("websitecontent-radio", uploadContentOption);
        };
        if (document.getElementsByClassName("cont-upload-arr") !== null) {
            setEventListenerOnClass("cont-upload-arr", toggleThis);
        };

        document.getElementById('single-file-upload').addEventListener('click', singleFileUpload);
        document.getElementById('home-upload').addEventListener('click', singleFileUpload);
        document.getElementById('service-upload').addEventListener('click', singleFileUpload);
        document.getElementById('about-upload').addEventListener('click', singleFileUpload);
        document.getElementById('contact-upload').addEventListener('click', singleFileUpload);

        if (UI.checkDom('web-content-btn') !== null) {
            UI.checkDom('web-content-btn').addEventListener('click', webContentSection);
        }
        // Domain Section
        if (document.getElementsByClassName("have-domain") !== null) {
            setEventListenerOnClass("have-domain", haveDomain);
        };

        if (UI.checkDom('web-domain-btn') !== null) {
            UI.checkDom('web-domain-btn').addEventListener('click', webDomainSection);
        }

        if (UI.checkDom('add-email') !== null) {
            UI.checkDom('add-email').addEventListener('click', addEmail);
        }


        // Add to cart pending
        if (UI.checkDom('addtoCartWS')) {
            UI.checkDom('addtoCartWS').addEventListener('click',function () {
                ks.addToCart(ks.readPkg());
                UI.displayCart();
            });

        }

    };

    var addEmail = function() {
        console.log('add');
        html = '<div class="d-input-name-list"><input type="text" class="form-control domain-input domain-email" placeholder="Name"/><span class="atrate-email">@yourdomain.co.za</span></div>';

        let domainEmailSection = document.querySelector('#domain-email-input-list');
        domainEmailSection.insertAdjacentHTML('beforeend', html);
    }

    var webDomainSection = function() {
        clearToGo = true;
        console.log('clicked');

        let db = ks.readPkg();
        // Get name of existing domain name

        // Get value of existing domain name
        domainYesName = document.getElementById('domainYesName').value;
        domainNoName = document.getElementById('domainNoName').value;
        domainExtension = document.getElementById('domain-extenction').value;

        if (ks.readPkg().haveDomain === 'yes') {
            (validation.require(domainYesName)) ? db.domain = domainYesName : customAlert("require", "Domain ");
        } else if (ks.readPkg().haveDomain === 'no') {
            (validation.require(domainNoName)) ? db.domain = domainNoName + domainExtension : customAlert("require", "Domain ");
        }

        // Domain Email section;
        var domainEmails = [];

        // Loops thru all elements of domain email and add to the array
        domainEmail = document.querySelectorAll('.domain-email');

        domainEmail.forEach(function(current) {
            if (current.value.length > 3) {
                domainEmails.push(current.value);
            }
        });

        // Save email to db
        db.domainEmails = domainEmails;
        ks.savePkg(db);


        if (clearToGo) {
            console.log('going to show summary now');
            // calculate email price and price
            let emailPrice = (domainEmails.length) * 20;
            ks.update('emailPrice', emailPrice);
            console.log(emailPrice);
            totalPrice = ks.readPkg().packagePrice + emailPrice;
            db.price = totalPrice;

            displaySection('summary');
            UI.getEle('previous-btn').dataset.currentstatus = 'summary';

            (document.getElementById('seo-nav-details')) ? document.getElementById('seo-nav-details').className += ' active': '';
            (document.getElementById('web-nav-website')) ? document.getElementById('web-nav-website').className += ' active': '';
            (document.getElementById('web-nav-domain')) ? document.getElementById('web-nav-domain').className += ' active': '';
            (document.getElementById('web-nav-summary')) ? document.getElementById('web-nav-summary').className += ' active': '';

            UI.loadWebSummary();
        };
        ks.savePkg(db);

    }


    var displayDomainSummary = function() {
        let html = '';
        html += '<li>' + ks.readPkg().domain + '</li>';
        ks.readPkg().domainEmails.forEach(function(current) {
            html += '<li>' + current + '</li>';
        });

        document.getElementById('display-domain-summary').innerHTML = html;
    }


    // var displayPackageSummary = function(packageDetails) {
    //     let html = '';

    //     packageDetails.forEach(function(current) {
    //         html += '<li>' + current + '</li>';
    //     });

    //     document.getElementById('display-package-summary').innerHTML = html;
    // }



    var haveDomain = function(event) {
        let db = ks.readPkg();
        if (event.target.value === 'yes') {
            db.haveDomain = 'yes';
            document.getElementById('step-4-yes').style.display = 'block';
            document.getElementById('web-domain-list').style.display = 'none';
            document.getElementById('step-4-no').style.display = 'none';
        } else if (event.target.value === 'no') {
            db.haveDomain = 'no';
            document.getElementById('step-4-yes').style.display = 'none';
            document.getElementById('web-domain-list').style.display = 'block';
            document.getElementById('step-4-no').style.display = 'block';
        }
        ks.savePkg(db);
    }

    var toggleThis = function(event) {
        if (event.target.id === 'home-page-upload-text-btn') {
            toggle('home-page-content');
        }
        if (event.target.id === 'service-page-upload-text-btn') {
            toggle('service-content');
        }
        if (event.target.id === 'about-us-page-upload-text-btn') {
            toggle('about-us-content');
        }
        if (event.target.id === 'contact-us-page-upload-text-btn') {
            toggle('contact-us-content');
        }
    }

    var toggle = function myFunction(myDiv) {
        var x = document.getElementById(myDiv);
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    var webContentSection = function() {
        let errorsArr = [];
        let db = ks.readPkg();

        console.log('hello content btn');
        var file1Content = document.getElementById('fullsitedockcomment').value;
        db.file1Content = file1Content;

        var labelHome = document.getElementById('label-home-page').value;
        var labelServices = document.getElementById('label-services-page').value;
        var labelAbout = document.getElementById('label-aboutus-page').value;
        var labelContact = document.getElementById('label-contactus-page').value;

        // Comments

        var commentHome = document.getElementById('home-comment').value;
        var commentServices = document.getElementById('service-comment').value;
        var commentAbout = document.getElementById('about-comment').value;
        var commentContact = document.getElementById('contact-comment').value;


        let label = {
            "home": labelHome,
            "services": labelServices,
            "about": labelAbout,
            "contact": labelContact
        };

        let comment = {
            "home": commentHome,
            "services": commentServices,
            "about": commentAbout,
            "contact": commentContact
        };


        allPageContent = {
            "label": label,
            "comment": comment
        };

        db.content = allPageContent;

        if (errorsArr.length === 0) {
            console.log('going to show content now');

            displaySection('domain');
            UI.getEle('previous-btn').dataset.currentstatus = 'domain';

            (document.getElementById('seo-nav-details')) ? document.getElementById('seo-nav-details').className += ' active': '';
            (document.getElementById('web-nav-website')) ? document.getElementById('web-nav-website').className += ' active': '';
            (document.getElementById('web-nav-content')) ? document.getElementById('web-nav-content').className += ' active': '';

        }

        ks.savePkg(db);
    };

    var singleFileUpload = function(event) {

        console.log('photo section caal');
        var inputTypeName = '#' + event.target.dataset.name; //file2
        let file_data = $(inputTypeName).prop('files')[0];

        // console.log(file_data);
        // return false;
        let db = ks.readPkg();
        if (file_data) {
            var _token = document.getElementById('csrf-token').value;
            var form_data = new FormData();
            form_data.append('_token', _token);
            form_data.append('file', file_data);

            $.ajax({
                url: '/submit',
                type: 'post',
                data: form_data,
                processData: false,
                contentType: false,
                success: function(response) {
                    db[event.target.dataset.name] = response;
                    ks.savePkg(db);
                }
            });
            event.preventDefault();
        } else {
            alert('Please select file to upload');
        }
    };

    var uploadContentOption = function(event) {
        //console.log(event.target);
        if (event.target.id === 'upload-documents') {
            document.getElementById('step-3-content-1').style.display = 'block';
            document.getElementById('step-3-content-2').style.display = 'none';

        } else if (event.target.id === 'upload-create') {
            document.getElementById('step-3-content-1').style.display = 'none';
            document.getElementById('step-3-content-2').style.display = 'block';

        }
    }

    var selectThis = function(event) {
        let id, objectName;

        let data = ks.readPkg();
        id = event.target.id;
        objectName = event.target.dataset.name;
        data[objectName] = id;

        removeCurrentActiveClass('.select-this-one');
        $(this).addClass('package-button-click');

        ks.savePkg(data);
    };

    var selectedCheckBox = function(event) {
        let value = event.target.value;
        let db = ks.readPkg();
        if (event.target.className === 'genderCheckbox') {

            arrayField = db.targetGender;
        } else {
            arrayField = db.trafficFrom;
        }

        var checkIndex = arrayField.indexOf(value);

        if (checkIndex === -1) {
            arrayField.push(event.target.value);
        } else {
            arrayField.splice(checkIndex, 1);
        }
        ks.savePkg(db);
    };

    var facebookSectionLoadOnInit = function() {

        (UI.getEle('package-list')) ? UI.getEle('package-list').innerHTML = UI.displayPackages("facebook") : '';
        (UI.getEle('package-list1')) ? UI.getEle('package-list1').innerHTML = UI.displayPackages("facebook") : '';

        //UI.getEle('package-list1').innerHTML = "Brijesh";

        if (document.getElementsByClassName("have-fb-page") !== null) {
            setEventListenerOnClass("have-fb-page", displayPackageOption);
        };
        if (document.getElementsByClassName("get-package") !== null) {
            setEventListenerOnClass("get-package", selectThis);
        };
        UI.addClick('fb-pkg-btn', fbPkgSection);

        if (ks.readPkg().haveFbpage === 'yes') {
            UI.hide('facebook-step-2-no');
        } else {
            UI.hide('facebook-step-2-yes');

        }

        // Details

        //class="traffic-from"
        if (document.getElementsByClassName("traffic-from") !== null) {
            setEventListenerOnClass("traffic-from", selectedCheckBox);
        }

        UI.addClick('fb-detail-btn', fbDetailSection);
        UI.addClick('country-add', addTag);
        UI.addClick('age-add', addTagAge);
        UI.addClick('ks-back-btn', ksBack);
        UI.addClick('single-file-upload', singleFileUpload);
        fbCountryTags();
        fbAgeTags();
        if (document.querySelector('.tag-list') !== null) {
            document.querySelector('.tag-list').addEventListener('click', deleteTag);
        };

        if (document.querySelector('.age-tag-list') !== null) {
            document.querySelector('.age-tag-list').addEventListener('click', deleteTag);
        };


        if (document.getElementsByClassName("genderCheckbox") !== null) {
            setEventListenerOnClass("genderCheckbox", selectedCheckBox);
        };

        if (document.getElementsByClassName("select-fb-campaign-details") !== null) {
            setEventListenerOnClass("select-fb-campaign-details", selectFbCampaignDetails);
        }

        UI.show('fb-step-2-website-detail');
        UI.hide('fb-step-2-custom-detail');

        // if (ks.readPkg().haveFbpage == 'yes') {
        //     UI.show('facebook-step-3-yes');
        //     UI.hide('facebook-step-3-no');
        // } else if (ks.readPkg().haveFbpage == 'no') {
        //     UI.hide('facebook-step-3-yes');
        //     UI.show('facebook-step-3-no');
        // }

        UI.addClick('fb-summary-btn', fbSummarySection);
        //fbDisplaySummary();
        let db = ks.readPkg();
        UI.getEle('custom-detail-summary-image').src = "assets/files/" + db.file1 ;
        UI.getEle('summary-company-name').innerHTML = db.companyName;
        UI.getEle('summary-detail').innerHTML =  db.description;
        UI.getEle('summary-operating-hours').innerHTML = db.openingHours;
        UI.getEle('package-list1').innerHTML = UI.displayPackage(db);
        // if(UI.checkDom('addtoCartWSstepNo')){
        //     UI.checkDom('addtoCartWSstepNo').addEventListener("click", function () {
        //         console.log('cart add no');
        //         ks.addToCart(ks.readPkg());
        //         UI.displayCart();
        //     });
        // }
        // if(UI.checkDom('addtoCartWSstepYes')){
        //     UI.checkDom('addtoCartWSstepYes').addEventListener("click", function () {
        //         console.log('cart add YES');
        //         ks.addToCart(ks.readPkg());
        //         UI.displayCart();
        //     });
        // }
    };

    var ksBack = function() {
        displaySection(ks.readPkg().previous);
    };

    var fbPkgSection = function() {
        if (ks.readPkg().haveFbpage === 'yes') {
            sectionPkg();
            UI.show('facebook-step-2-yes');
            UI.hide('facebook-step-2-no');

        } else if (ks.readPkg().haveFbpage === 'no') {
            displaySection('details');
            UI.getEle('previous-btn').dataset.currentstatus = 'details';
            UI.addActiveClass('nav-details');

            UI.hide('facebook-step-2-yes');
            UI.show('facebook-step-2-no');

        }

        let data = ks.readPkg();
        data.previous = "packages";
        ks.savePkg(data);

    };
    var fbSummarySection = function() {
        clearToGo = true;
        let productsAndServices, productsAndServicesUrl;
        let db = ks.readPkg();
        console.log('summary clicked');

        if(ks.readPkg().haveFbpage == 'yes') {
            productsAndServices = UI.getInputValue('products-and-services');
            productsAndServicesUrl = UI.getInputValue('products-and-services-url');

            (validation.require(productsAndServices)) ? db.productsAndServices = productsAndServices : customAlert("require", "Product ");
            (validation.require(productsAndServicesUrl)) ? db.productsAndServicesUrl = productsAndServicesUrl : customAlert("require", "Url ");

        }

        //console.log(clearToGo);
        if (clearToGo) {
            console.log('going to next section after summary');
            db.previous = 'domain';
            displaySection('summary');
            UI.getEle('previous-btn').dataset.currentstatus = 'summary';
            if(UI.checkDom('addtoCartWSstepYes')){
                UI.checkDom('addtoCartWSstepYes').addEventListener("click", function () {
                    console.log('cart add YES');
                    ks.addToCart(ks.readPkg());
                    UI.displayCart();
                });
            }
            UI.addActiveClass('nav-summary');
            ks.savePkg(db);
            fbDisplaySummary();
        };
    }

    var fbDisplaySummary = function() {
        console.log("Hello Brijesh");
        let db = ks.readPkg();
        // console.log(db);
        UI.displayPackage(db);

        //Campaing Details
        UI.getEle('campain-summary-email').innerHTML = db.email;
        UI.getEle('campain-summary-website').innerHTML = db.url;
        UI.getEle('campain-summary-target-city').innerHTML = db.countries;
        UI.getEle('campain-summary-target-age-from').innerHTML = db.targetAge;
        UI.getEle('campain-summary-gender').innerHTML = db.targetGender;
        // Product and Services details
        UI.getEle('products-and-services-summary').innerHTML = db.productsAndServices;
        UI.getEle('products-and-services-url-summary').innerHTML = db.productsAndServicesUrl;
    }

    var selectFbCampaignDetails = function(event) {
        let db = ks.readPkg();
        if (event.target.value === 'fb-step-2-website-detail-button') {
            UI.show('fb-step-2-website-detail');
            UI.hide('fb-step-2-custom-detail');
            db.fbUseDetailFromWebsite = 'yes';
            db.fbEnterCustomDetail = 'no';
        }else {
            UI.hide('fb-step-2-website-detail');
            UI.show('fb-step-2-custom-detail');
            db.fbUseDetailFromWebsite = 'no';
            db.fbEnterCustomDetail = 'yes';
        }
        ks.savePkg(db);
    };

    var fbDetailSection = function() {
        // Brijesh
        clearToGo = true;
        let email = UI.getInputValue('email');
        let email1 = UI.getInputValue('email1');
        let url = UI.getInputValue('url');
        let url1 = UI.getInputValue('url1');
        let campaignDetailUrl   = UI.getInputValue('campaign-detail-url');
        let campaignDetailEmail = UI.getInputValue('campaign-detail-email');
        let websiteEmail = UI.getInputValue('use-detail-from-website-email');
        let websiteUrl = UI.getInputValue('use-detail-from-website-url');

        let companyName = UI.getInputValue('company-name');
        let description = UI.getInputValue('description');
        let openingHours = UI.getInputValue('opening-hours');
        let db = ks.readPkg();

        if (ks.readPkg().haveFbpage === 'yes') {
            console.log('landing here');
            if (ks.readPkg().trafficFrom.length === 0) { customAlert("require", "Traffic From") };
            (validation.email(campaignDetailEmail)) ? db.email = campaignDetailEmail : customAlert("email");
            (validation.require(campaignDetailUrl)) ? db.url = campaignDetailUrl : customAlert("require", "URL ");
            ks.savePkg(db);
        } else {
            console.log(db.fbUseDetailFromWebsite);

            if ( db.fbUseDetailFromWebsite === 'no' ) {
                console.log('sssss');
                (validation.email(email1)) ? db.email = email1 : customAlert("email");
                (validation.require(companyName)) ? db.companyName = companyName : customAlert("require", "company Name");
                (validation.require(description)) ? db.description = description : customAlert("require", "description ");
                (validation.require(openingHours)) ? db.openingHours = openingHours : customAlert("require", "Opening Hours ");
                (validation.require(db.file1)) ? console.log('file exist') : customAlert("require", "Logo Image ");
            }

            if ( db.fbUseDetailFromWebsite === 'yes' ) {
                (validation.email(websiteEmail)) ? db.email = email : customAlert("email");
                (validation.require(websiteUrl)) ? db.url = url : customAlert("require", "URL ");
                console.log('jjjj');

            }
        }





        if (clearToGo) {
            console.log('going to next section');
            displaySection('domain');
            UI.getEle('previous-btn').dataset.currentstatus = 'domain';

            if (ks.readPkg().haveFbpage == 'yes') {
                UI.show('facebook-step-3-yes');
                UI.hide('facebook-step-3-no');
            } else if (ks.readPkg().haveFbpage == 'no') {
                UI.hide('facebook-step-3-yes');
                UI.show('facebook-step-3-no');
                if(UI.checkDom('addtoCartWSstepNo')){
                    UI.checkDom('addtoCartWSstepNo').addEventListener("click", function () {
                        console.log('cart add no');
                        ks.addToCart(ks.readPkg());
                        UI.displayCart();
                    });
                }
                if (db.fbUseDetailFromWebsite === 'yes') {
                    UI.show('website-detail-summary');
                    UI.hide('custom-detail-summary');
                } else {
                    UI.show('custom-detail-summary');
                    UI.hide('website-detail-summary');
                }
            }
            UI.addActiveClass('nav-domain');


            db.previous = 'details';

            //ks.savePkg(data);
            ks.savePkg(db);
        }


    }

    var customAlert = function(type1,  param="") {
        alert(errors[type1](param));
        clearToGo = false;
    }

    var displayPackageOption = function(event) {
        console.log(event.target.value);
        let db = ks.readPkg();
        if (event.target.value === 'yes') {
            db.haveFbpage = 'yes';

            document.getElementById('fb-package').style.display = 'block';
            document.getElementById('progressbar-step-2').innerHTML = 'Campaign <br> Details';
            document.getElementById('progressbar-step-3').innerHTML = 'Product/ <br>Service';
            document.getElementById('progressbar-step-4').innerHTML = 'Summary';
        } else if (event.target.value === 'no') {
            db.haveFbpage = 'no';
            document.getElementById('fb-package').style.display = 'none';
            document.getElementById('progressbar-step-2').innerHTML = 'Details';
            document.getElementById('progressbar-step-3').innerHTML = 'Summary';
            document.getElementById('progressbar-step-4').innerHTML = 'Select Marketing<br>Package';
        }

        ks.savePkg(db);
    };

    var colorOption = function(event) {

        if (event.target.id === 'customize-own-colours') {
            document.getElementById('website-selectcolorlist').style.display = 'none';
            document.getElementById('customize-own-colours-section').style.display = 'block';
        } else if (event.target.id === 'select-colour') {
            console.log('select color'); //customize-own-colours-section
            document.getElementById('website-selectcolorlist').style.display = 'block';
            document.getElementById('customize-own-colours-section').style.display = 'none';

        }
    }



    var setEventListenerOnClass = function(classname, methodname) {
        var selectTemplates = document.getElementsByClassName(classname);
        for (var i = 0; i <= selectTemplates.length - 1; i++) {
            selectTemplates[i].addEventListener('click', methodname);
        }
    }

    var webInfoSection = function() {
        console.log('clicked');
        var errorsArr = [];
        let db = ks.readPkg();
        //validate value and add to object
        let email = UI.getWebForm().email;
        if (validation.email(email)) {
            db.email = email;
        } else {
            errorsArr.push("Email");
            alert(errors.email());
        };

        var copywritings = document.getElementsByName('copywriting');
        var copywriting = selectedISP(copywritings);
        console.log(copywriting);
        if (copywriting === undefined) {
            errorsArr.push("copywriting");
            swal("Please select an option for copywriting");

        }
        db.copywriting = copywriting;

        // Select Template
        let customSchema = {
            "main": document.getElementById('main-color-hex').value,
            "secondary": document.getElementById('secondary-color-hex').value,
            "calltoaction": document.getElementById('calltoaction-color-hex').value,
            "miscellaneous": document.getElementById('miscellaneous-color-hex').value,
        };
        db.customSchema = customSchema;
        ks.savePkg(db);

        if (errorsArr.length === 0) {
            console.log('going to show content now');

            displaySection('content');
            UI.getEle('previous-btn').dataset.currentstatus = 'content';
            (document.getElementById('seo-nav-details')) ? document.getElementById('seo-nav-details').className += ' active': '';
            (document.getElementById('web-nav-website')) ? document.getElementById('web-nav-website').className += ' active': '';
            (document.getElementById('web-nav-content')) ? document.getElementById('web-nav-content').className += ' active': '';

        }

    }


    var editSeoSummary = function() {

        UI.getDomEle().seoEditPkg.addEventListener('click', editPkg);
        UI.getDomEle().seoEditDetails.addEventListener('click', editPkg);
        UI.getDomEle().seoEditRecurring.addEventListener('click', editPkg);

    }

    var subNav = function() {
        console.log('Sub Nav');
        if(ks.readPkg().type === 'seo') {
            document.getElementById('seo-nav-select-pkg').addEventListener('click', navigation);
            document.getElementById('seo-nav-details').addEventListener('click', navigation);
            document.getElementById('seo-nav-recurring').addEventListener('click', navigation);
            document.getElementById('seo-nav-summary').addEventListener('click', navigation);
            // document.getElementById('add-to-cart-btn').addEventListener('click', addToCart);
        }

        if(ks.readPkg().type === 'web'){
            document.getElementById('web-nav-select-pkg').addEventListener('click', navigation);
            document.getElementById('web-nav-website').addEventListener('click', navigation);
            document.getElementById('web-nav-content').addEventListener('click', navigation);
            document.getElementById('web-nav-domain').addEventListener('click', navigation);
            document.getElementById('web-nav-summary').addEventListener('click', navigation);
        }

        if(ks.readPkg().type === 'facebook'){
            document.getElementById('fb-nav-packages').addEventListener('click', navigation);
            document.getElementById('nav-details').addEventListener('click', navigation);
            document.getElementById('nav-domain').addEventListener('click', navigation);
            document.getElementById('nav-summary').addEventListener('click', navigation);
        }

    }

    var addToCart = function() {
        console.log('add to cart clicked');

        ks.addToCart();

        UI.displayCartProducts();
        //UI.displayTotalPrice();
    }

    var navigation = function(event) {
        console.log('call navigation part');
        var navText = event.target.innerText;

        if (navText.indexOf("Package") !== -1) {
            displaySection('packages');
        }

        if (navText === "Details" && ks.readPkg().type === 'seo') {
            seoNavDetails = document.getElementById('seo-nav-details');
            if (hasClass(seoNavDetails, 'active')) {
                displaySection('details');
            }
        }

        if (navText === "Recurring") {
            seoNavRecurring = document.getElementById('seo-nav-recurring');
            if (hasClass(seoNavRecurring, 'active')) {
                displaySection('recurring');
            }

        }

        if (navText === "Summary" && ks.readPkg().type === 'seo') {
            seoNavSummery = document.getElementById('seo-nav-summary');
            if (hasClass(seoNavRecurring, 'active')) {
                displaySection('summary');
            }
        }

        if (navText.indexOf("Website") !== -1) {
            displaySection('details');
        }

        if (navText === "Content") {
            webNavContent = document.getElementById('web-nav-content');
            if (hasClass(webNavContent, 'active')) {
                displaySection('content');
            }
        }

        if (navText === "Domain") {
            webNavDomain = document.getElementById('web-nav-domain');
            if (hasClass(webNavDomain, 'active')) {
                displaySection('domain');
            }
        }

        if (navText === "Summary" && ks.readPkg().type === 'web') {
            webNavSummary = document.getElementById('web-nav-summary');
            if (hasClass(webNavSummary, 'active')) {
                displaySection('summary');
            }
        }

        if (navText.indexOf("Campaign") !== -1 || navText.indexOf("Details") !== -1 ) {
            displaySection('details');
        }

        if (navText.indexOf("Product") !== -1) {
                displaySection('domain');
        }

        if (navText === "Summary" && ks.readPkg().type === 'facebook') {
            fbNavSummary = document.getElementById('nav-summary');
            if (hasClass(fbNavSummary, 'active')) {
                displaySection('summary');
            }
        }

    };

    var displaySection = function(section) {

        if (document.getElementById('page-1')) {
            document.getElementById('page-1').style.display = (section === 'page-1') ? "block" : "none";
        };
        if (document.getElementById('page-2')) {
            document.getElementById('page-2').style.display = (section === 'page-2') ? "block" : "none";
        };
        if (document.getElementById('page-3')) {
            document.getElementById('page-3').style.display = (section === 'page-3') ? "block" : "none";
        };
        if (document.getElementById('page-4')) {
            document.getElementById('page-4').style.display = (section === 'page-4') ? "block" : "none";
        };
        if (document.getElementById('page-5')) {
            document.getElementById('page-5').style.display = (section === 'page-5') ? "block" : "none";
        };
        if (document.getElementById('fs_packages')) {
            document.getElementById('fs_packages').style.display = (section === 'packages') ? "block" : "none";
        }
        if (document.getElementById('fs_detail')) {
            document.getElementById('fs_detail').style.display = (section === 'details') ? "block" : "none";
        }
        if (document.getElementById('fs_content')) {
            document.getElementById('fs_content').style.display = (section === 'content') ? "block" : "none";
        }
        if (document.getElementById('fs_domain')) {
            document.getElementById('fs_domain').style.display = (section === 'domain') ? "block" : "none";
        }
        if (document.getElementById('fs_recurring')) {
            document.getElementById('fs_recurring').style.display = (section === 'recurring') ? "block" : "none";
        }
        if (document.getElementById('fs_summary')) {
            document.getElementById('fs_summary').style.display = (section === 'summary') ? "block" : "none";
        }

    }

    var hasClass = function hasClass(element, className) {
        return element.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(element.className);
    };

    var editPkg = function(event) {
        let id = event.target.id;

        if (id === 'seo-edit-pkg') {
            displaySection('packages');
        } else if (id === 'seo-edit-details') {
            displaySection('details');
        } else if (id === 'seo-edit-recurring') {
            displaySection('recurring');
        }
    };

    var campaignPeriod = function() {


        if (ks.readPkg().period === 0) {
            alert('Please select Package')
        } else {

            console.log('Go to next section');
            $('#fs_recurring').toggle("slow");
            $('#fs_summary').toggle("slow");
            let goToSection = 'summary';
            document.getElementById('seo-nav-summary').className += ' active';
            // Add Active call to Recurring Nav
            UI.displaySeoSummery(ks.readPkg());
            if(UI.checkDom('add-to-cart-btn')){
                // console.log(ks.readPkg());
                UI.checkDom('add-to-cart-btn').addEventListener("click", function () {
                    console.log("ct");
                    ks.addToCart(ks.readPkg());
                    UI.displayCart();
                });
                // ks.addToCart(ks.readPkg());
            }
            // displaySection(goToSection);
            UI.getEle('previous-btn').dataset.currentstatus = goToSection;
        }


    };

    var selectTemplate = function() {
        console.log('clicked select package');
        let id = $(this).attr('id');
        console.log( 'selected template : ' + id);
        let db = ks.readPkg();
        db.webtemplate = id;
        removeCurrentActiveClass('.template-select');
        $(this).addClass('package-button-click');

        ks.savePkg(db);
    };

    var selectCampaignPeriod = function() {
        id = $(this).attr('id').split("--");
        let db = ks.readPkg();
        db.period = parseInt(id[1]);
        ks.savePkg(db);
        ks.calculatePrice();

        removeCurrentActiveClass('.run-month-btn');
        $(this).addClass('package-button-click');
    }

    // Provide classname, class name is used to find all elements
    var removeCurrentActiveClass = function(className) {
        let list = document.querySelectorAll(className);
        list.forEach(function(current) {
            current.classList.remove('package-button-click');
        });
    }

    var selectPkg = function() {
        var id;
        let db = ks.readPkg();
        id = $(this).attr('id');
        db.packageId = parseInt(id);

        packageData[ks.readPkg().type].forEach(function(current) {
            if (current.id == id) {
                db.packagePrice = current.price;
                db.price = current.price;
            }
        });
        ks.savePkg(db);
        removeCurrentActiveClass('.select-package');
        $(this).addClass('package-button-click');

    }


    var sectionPkg = function() {
        if (ks.readPkg().packageId) {
            let goToSection = 'details';
            console.log('btn clicked');
            displaySection(goToSection);
            UI.getEle('previous-btn').dataset.currentstatus = goToSection;

            // displaySection('details');
            UI.addActiveClass('nav-details');
            (document.getElementById('seo-nav-details')) ? document.getElementById('seo-nav-details').className += ' active': '';
            (document.getElementById('web-nav-website')) ? document.getElementById('web-nav-website').className += ' active': '';
        } else {
            alert(errors.require("Packed "));
        }

    };

    var haveFbpage = function() {
        // BM
        (ks.readPkg().haveFbpage === 'yes') ? sectionPkg(): displaySection('details');



    }

    var selectedISP = function(isps) {
        for (var i = 0, length = isps.length; i < length; i++) {
            if (isps[i].checked) {
                // do whatever you want with the checked radio
                return isps[i].value;

                // only one radio can be logically checked, don't check the rest
                break;
            }
        }
    };

    var selectedRadio = function(elements) {
        for (var i = 0, length = elements.length; i < length; i++) {
            if (elements[i].checked) {
                // do whatever you want with the checked radio
                return elements[i].value;

                // only one radio can be logically checked, don't check the rest
                break;
            }
        }
    };

    var deleteTag = function(event) {
        console.log('delete tag call');
        let fieldName = event.target.dataset.name;
        tag = event.target.parentNode.textContent;
        ks.deleteTag(tag, fieldName);
        if (fieldName === 'googleLocation') {
            locationTags();
        }else if (fieldName === 'countries') {
            fbCountryTags();
        } else if (fieldName === 'targetAge') {
            fbAgeTags();
        } else if (fieldName === 'keywords') {
            keywordTags();
        } else if (fieldName === 'language') {
            languageTags();
        } else if (fieldName === 'googleService') {
            googleServiceTags();
        } else if (fieldName === 'products') {
            seoProductTags();
        } else if (fieldName === 'googleBusiness') {
            GoogleBusinessTags();
        } else if (fieldName === 'googleCompetitors') {
            GoogleCompetitorsTags();
        } else if (fieldName === 'googleExtraDetails') {
            GoogleExtraDetailsTags();
        } else
        //     if (fieldName === 'campaignRunMonth') {
        //     CampaignRunMonthTags();
        // } else
            if (fieldName === 'showTheAd') {
            ShowTheAdTags();
        }

    };

    var addTagAge = function(event) {

        let targetInput, objectField, reloadTag;

        let ageFrom = document.getElementById('target-age-from').value;
        let ageTo = document.getElementById('target-age-to').value;

        targetInput = ageFrom + '-' + ageTo;
        objectField = event.target.dataset.name;
        var tag = targetInput;

        ks.addTag(tag, objectField);
        fbAgeTags();

        document.getElementById('target-age-from').value = '';
        document.getElementById('target-age-to').value = '';
    };

    var addTag = function(event) {
        let targetInput, objectField, reloadTag;

        if (event.target.id === 'country-add') {
            targetInput = 'target-city';
        }else if (event.target.id === 'productsandservices-add') {
            targetInput = 'productsandservices';
        } else {
            targetInput = event.target.id;
        }
        objectField = event.target.dataset.name;
        var tag = UI.getInputValue(targetInput);
        // Validate and Add tag to Array
        (validation.require(tag)) ? ks.addTag(tag, objectField) : alert(errors.blank("Tag")) ;
        UI.clearInputEleId(targetInput);
        fbCountryTags();
    }

    var seoProductTags = function() {
        // 1. Get data from KS controller
        let tags = ks.readPkg().products;
        let fieldName = 'products';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'productsandservices-tags', fieldName);
    }

    var fbAgeTags = function() {
        // 1. Get data from KS controller
        let tags = ks.readPkg().targetAge;
        let fieldName = 'targetAge';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'age-tags', fieldName);
    }

    var fbCountryTags = function() {
        // 1. Get data from KS controller
        let tags = ks.readPkg().countries;
        let fieldName = 'countries';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'country-tags', fieldName);
    };

    var keywordTags = function() {
        // 1. Get data from KS controller
        let tags = ks.readPkg().keywords;
        let fieldName = 'keywords';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'keyword-tags', fieldName);
    };

    var languageTags = function() {
        // 1. Get data from KS controller
        let tags = ks.readPkg().language;
        let fieldName = 'language';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'language-tags', fieldName);
    };

    var googleServiceTags = function() {
        // 1. Get data from KS controller
        let tags = ks.readPkg().googleService;
        let fieldName = 'googleService';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'google-service-tags', fieldName);
    };

    var GoogleBusinessTags = function() {
        let tags = ks.readPkg().googleBusiness;
        let fieldName = 'googleBusiness';
        UI.displayTags(tags, 'business-unique-tags', fieldName);
    };

    var GoogleCompetitorsTags = function() {
        let tags = ks.readPkg().googleCompetitors;
        let fieldName = 'googleCompetitors';
        UI.displayTags(tags, 'google-competitors-tags', fieldName);
    };

    var GoogleExtraDetailsTags = function() {
        let tags = ks.readPkg().googleExtraDetails;
        let fieldName = 'googleExtraDetails';
        UI.displayTags(tags, 'google-extra-details-tags', fieldName);
    };

    // var CampaignRunMonthTags = function() {
    //     let tags = ks.readPkg().campaignRunMonth;
    //     let fieldName = 'campaignRunMonth';
    //     UI.displayTags(tags, 'campaign-run-months-tags', fieldName);
    // };

    var ShowTheAdTags = function() {
        let tags = ks.readPkg().showTheAd;
        let fieldName = 'showTheAd';
        UI.displayTags(tags, 'show-the-ad-tags', fieldName);
    };

    var locationTags = function () {
        let tags = ks.readPkg().googleLocation;
        let fieldName = 'googleLocation';
        // 2. Display data using UI Controller
        UI.displayTags(tags, 'locations-tags', fieldName);
    }

    var getLocationList = function () {
        $('#locations').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/getlocations",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function(data) {
                        var tempArr = data.map((item) => {
                            return {
                                label: item.canonicalName,
                                value: item.canonicalName,
                                id: item.criteriaID
                            }
                        });
                        response(tempArr);
                    }
                });
            },
            select: function(event, ui) {

                //get selected country lat long
                $.ajax({
                    url: "https://maps.googleapis.com/maps/api/geocode/json?address="+ ui.item.value +"&key=AIzaSyBSTBZbCaCE1trqgmiUPCKdovtuPVtVdO4",
                    dataType: "json",
                    success: function(data) {
                        addTag(event);
                        locationTags();
                        // console.log(event.target.id);
                        // $('#locations-tags').append('<span class="tag" data-latlng='+JSON.stringify(data.results[0].geometry.location)+' data-criteriaID='+ui.item.id+' data-canonicalname="'+ui.item.value+'" >'+ui.item.value+'<span class="tag-close" id="location-tagremove"></span></span>');
                        addNewCountryMap(data.results[0].geometry.location);
                        window.data.location = ks.readPkg().googleLocation;
                        get_search_volume(window.data);
                        // $('#locations').val('');
                        // window.data.location = getCriteriaidOfTags("#locations-tags");
                        // get_search_volume(window.data);
                    }
                });
            },
            minLength: 4,
        });
    };

    var addNewCountryMap = function(countryLatLong){
        window.selectedCountries.push(countryLatLong);
        loadGoogleMaps();
    };

    var initialize = function() {
        var uluru = window.selectedCountries;
        var uluru1 = {
            lat: -25.363,
            lng: 131.044
        };
        var mapOptions = {
            zoom: 2,
            panControl: true,
            // center: new google.maps.LatLng(47.3239, 5.0428),
            center: uluru1,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        var marker = {};
        var bounds = {};
        var loc = {};
        for (var i = 0; i < window.selectedCountries.length; i++) {
            // data[i]
            bounds = new google.maps.LatLngBounds();
            loc = new google.maps.LatLng(window.selectedCountries[i].lat, window.selectedCountries[i].lng);
            bounds.extend(loc);
            marker = new google.maps.Marker({
                position: loc,
                // position: uluru,
                map: map
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(window.selectedCountries[i]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
        // map.fitBounds(bounds);
        // map.panToBounds(bounds);
    };

    var loadGoogleMaps = function() {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src", "https://maps.googleapis.com/maps/api/js?key=AIzaSyBSTBZbCaCE1trqgmiUPCKdovtuPVtVdO4&callback=gMapsCallback");
        (document.getElementsByTagName("body")[0] || document.documentElement).appendChild(script_tag);
    };

    var initGoogleMap = function () {
        // window.selectedCountries = [];
        window.gMapsCallback = function() {
            $(window).trigger('gMapsLoaded');
        };
        $(window).bind('gMapsLoaded', initialize);
        loadGoogleMaps();
    };

    var addNewCountryMap = function(countryLatLong){
        window.selectedCountries.push(countryLatLong);
        loadGoogleMaps();
    }

    var get_search_volume = function(data) {
        // data.keyword = ['cheesecake'];
        // data.location = 20008,20009;
        // data.language = 1019,1056;

        $.ajax({
            url: '/getsearchvolume',
            type: 'POST',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                data: data
            },
            success: function(result) {
                // setTimeout(function() {
                $('.audience-count').text(result.count);
                var cpc = '';
                $(result.cpc).each(function(index, el) {
                    cpc += '<p><strong>'+el.keyword+'</strong> '+el.cpc+'(cpc)</p>';
                });
                cpc += '<div class="estimated-performance-row"><p>Total Searches per month for above Keywords</p><p style="margin-top:  12px;"><span>'+result.count+'</span></p></div>';
                // console.log(cpc);
                $('.estimated-performance-dtl').html(cpc);
                window.peoplecount = result.count;
                // }, 1000);
            }
        });

    }

    var googleAdwordsLoadOnInit = function() {
        clearToGo = true;
        window.data = {};
        window.selectedCountries = [];
        UI.addClick('next-btn1', stepSection1);
        if (UI.checkDom('next-btn1')) {
            keywordTags();
            locationTags();
            loadGoogleMaps();
        }
        UI.addClick('next-btn2', stepSection2);
        UI.addClick('next-btn3', stepSection3);
        UI.addClick('next-btn4', stepSection4);
        UI.addClick('previous-btn', previous);

        initGoogleMap();

        document.getElementById('keyword').addEventListener('keypress', function(event) {
            addKeywords(event);
            window.data.keyword = ks.readPkg().keywords;
            $('#audience-count').html('<img src="images/loader.gif">');
            get_search_volume(data);
        });
        if (document.querySelector('.tag-list') !== null) {
            document.querySelector('.tag-list').addEventListener('click', deleteTag);
        };

        if (document.querySelector('#locations') !== null) {
            getLocationList();
        }
        if (document.querySelector('.locations-tag-list') !== null) {
            document.querySelector('.locations-tag-list').addEventListener('click',function(event){
                let db = ks.readPkg().googleLocation;
                let index = db.indexOf(event.target.parentNode.textContent);
                let latLong = window.selectedCountries;
                latLong.splice(latLong.indexOf(index), 1);
                window.selectedCountries = latLong;
                loadGoogleMaps();
                deleteTag(event);
            });
        }

        if (document.getElementsByClassName("accor-arrow") !== null) {
            setEventListenerOnClass("accor-arrow", accorArrow);
        }

        document.getElementById('language').addEventListener('keypress', addLanguage);
        if (document.querySelector('.tag-list') !== null) {
            document.querySelector('.tag-list').addEventListener('click', deleteTag);
        };

        document.getElementById('google-service').addEventListener('keypress', addGoogleService);
        if (document.querySelector('.google-service-tag-list') !== null) {
            document.querySelector('.google-service-tag-list').addEventListener('click', deleteTag);
        };

        document.getElementById('business-unique').addEventListener('keypress', addGoogleBusiness);
        if (document.querySelector('.business-unique-tag-list') !== null) {
            document.querySelector('.business-unique-tag-list').addEventListener('click', deleteTag);
        };

        document.getElementById('google-competitors').addEventListener('keypress', addGoogleCompetitors);
        if (document.querySelector('.google-competitors-tag-list') !== null) {
            document.querySelector('.google-competitors-tag-list').addEventListener('click', deleteTag);
        };

        document.getElementById('google-extra-details').addEventListener('keypress', addGoogleExtraDetails);
        if (document.querySelector('.google-extra-details-tag-list') !== null) {
            document.querySelector('.google-extra-details-tag-list').addEventListener('click', deleteTag);
        };


        // document.getElementById('campaign-run-months').addEventListener('keypress', addCampaignRunMonth);
        // if (document.querySelector('.campaign-run-months-tag-list') !== null) {
        //     document.querySelector('.campaign-run-months-tag-list').addEventListener('click', deleteTag);
        // };

        document.getElementById('show-the-ad').addEventListener('keypress', addShowTheAd);
        if (document.querySelector('.show-the-ad-tag-list') !== null) {
            document.querySelector('.show-the-ad-tag-list').addEventListener('click', deleteTag);
        };
        if (document.querySelector('#slider-range') !== null) {
            document.querySelector('#slider-range').oninput = function () {
                document.querySelector('#amount').value = this.value;
                let db = ks.readPkg();
                db.budget = this.value;
                ks.savePkg(db);
            }
        }


    };
    var accorArrow = function(event) {
        let elementId = event.target.parentNode.parentNode.childNodes[2].id;
        toggleHideShow(elementId);
    }

    var toggleHideShow = function(elementId) {
        let checkStatus = document.getElementById(elementId).style.display;
        if (checkStatus.length === 0) {
            document.getElementById(elementId).style.display = 'none';
        } else if (checkStatus === 'block') {
            document.getElementById(elementId).style.display = 'none';
        } else if (checkStatus === 'none') {
            document.getElementById(elementId).style.display = 'block';
        }
    };
    var addKeywords = function(event) {
        if(event.which === 13) {
            // When user press enter add tag
            addTag(event);
            // Reload tags to show the added tag
            keywordTags();
        }
    };

    var addLanguage = function(event) {
        if(event.which === 13) {
            console.log('clicked langulage');

            addTag(event);
            // Reload tags to show the added tag
            languageTags();
        }
    };

    var addGoogleService = function(event) {
        if(event.which === 13) {
            addTag(event);
            googleServiceTags();
        }
    };

    var addGoogleBusiness = function(event) {
        if(event.which === 13) {
            addTag(event);
            GoogleBusinessTags();
        }
    };

    var addGoogleCompetitors = function(event) {
        if(event.which === 13) {
            addTag(event);
            GoogleCompetitorsTags();
        }
    };

    var addGoogleExtraDetails = function(event) {
        if(event.which === 13) {
            addTag(event);
            GoogleExtraDetailsTags();
        }
    };

    // var addCampaignRunMonth = function(event) {
    //     if(event.which === 13) {
    //         addTag(event);
    //         CampaignRunMonthTags();
    //     }
    // };

    var addShowTheAd = function(event) {
        if(event.which === 13) {
            addTag(event);
            ShowTheAdTags();
        }
    };


    var previous = function(event){
        event.preventDefault();
        switch (ks.readPkg().type) {
            case "web":
                var sections = ['packages','details','content','domain','summary'];
                break;
            case "seo":
                var sections = ['packages','details','recurring','summary'];
                break;
            case "facebook":
                var sections = ['packages', 'details', 'domain', 'summary'];
                break;
        }
        // let sections = ['page-1', 'page-2', 'page-3', 'page-4', 'page-5','packages','details','recurring','summary'];
        let currentSection = event.target.dataset.currentstatus;
        console.log(currentSection);
        let goTo = sections.indexOf(currentSection) - 1 ;
        displaySection(sections[goTo]);
        UI.getEle('previous-btn').dataset.currentstatus = sections[goTo] ;
    };

    var stepSection1 = function() {
        (!validation.require(ks.readPkg().keywords)) ? customAlert('require','keywords') : clearToGo = true ;
        (!validation.require(ks.readPkg().googleLocation)) ? customAlert('require','Location') : clearToGo = true ;
        if (clearToGo) {
            (document.getElementById('google-step-2')) ? document.getElementById('google-step-2').className += ' active': '';
            let goToSection = 'page-2';
            console.log('btn clicked');
            displaySection(goToSection);
            UI.getEle('previous-btn').dataset.currentstatus = goToSection;
        }
    };
    var stepSection2 = function() {
        (document.getElementById('google-step-3')) ? document.getElementById('google-step-3').className += ' active': '';
        let goToSection = 'page-3';
        console.log('btn clicked');
        displaySection(goToSection);
        UI.getEle('previous-btn').dataset.currentstatus = goToSection;
    };
    var stepSection3 = function() {
    	let clearToGo = true;
        let goToSection = 'page-4';
        let db = ks.readPkg();
        let campaignRunMonth = UI.getInputValue('campaign-run-months');
        validation.require(campaignRunMonth) ? db.campaignRunMonth = campaignRunMonth : customAlert('required',"Months") ;
        console.log('btn  3 clicked');
        displaySection(goToSection);

        UI.getEle('previous-btn').dataset.currentstatus = goToSection;
        let timeFrom = UI.getInputValue('campaign-timing-from');

        let timeTo = UI.getInputValue('campaign-timing-to');
        if (validation.require(timeFrom) && validation.require(timeTo)) {
            let campaignRunTime = timeFrom + '-' + timeTo;
        	db.campaignRunTime = campaignRunTime;
        	ks.savePkg(db);
        } else {
        	clearToGo = false;
        	alert('Please Insert from and to both time.');
        }

        if (clearToGo) {
            (document.getElementById('google-step-4')) ? document.getElementById('google-step-4').className += ' active': '';
        	console.log('go to next section');
        }

    };
    var stepSection4 = function() {
        let goToSection = 'page-5';
        console.log('btn clicked');
        (document.getElementById('google-step-4')) ? document.getElementById('google-step-4').className += ' active': '';
        (document.getElementById('google-step-5')) ? document.getElementById('google-step-5').className += ' active': '';
        displaySection(goToSection);
        if (UI.checkDom('summary-budget')) {
            UI.checkDom('summary-budget').textContent = ks.readPkg().budget;
        }
        if (UI.checkDom('language')) {
            UI.checkDom('language').textContent = ks.readPkg().language.toString();
        }
        if (UI.checkDom('summary-months')) {
            UI.checkDom('summary-months').textContent = ks.readPkg().campaignRunMonth;
        }
        if (UI.checkDom('summary-running-times')) {
            UI.checkDom('summary-running-times').textContent = ks.readPkg().campaignRunTime;
        }

        UI.getEle('previous-btn').dataset.currentstatus = goToSection;
    };


    return {
        initSeo: function() {
            //Validate
            console.log('App started');
            ks.setType("seo");
            ks.savePkg(ks.data);
            // let goToSection = 'fs_packages';
            // UI.getEle('previous-btn').dataset.currentstatus = goToSection;
            // UI.commonDomEle(ks.readPkg().type);
            seoSectionLoadOnInit();
            // Display Elements when app loads
            UI.displayCart();
            //console.log(validation.test());
            // seoProductTags();
            setupEventListner();
            // UI.displaySeoSummery();
            subNav();
            editSeoSummary();
            UI.addClick('previous-btn', previous);

            //console.log(cartCount());

        },
        initWeb: function() {
            UI.displayCart();
            ks.setType("web");
            ks.savePkg(ks.data);
            // UI.commonDomEle(ks.readPkg().type);
            webSectionLoadOnInit();
            UI.webDomEle();

            subNav();
            UI.addClick('previous-btn', previous);
            // UI.loadWebSummary();
            //loadWebSummary(); // Remove after test
            //console.log(ks.readPkg().type);

        },
        initFacebook: function() {
            ks.setType("facebook");
            ks.savePkg(ks.data);
            UI.displayCart();

            console.log('Facebook loading');
            facebookSectionLoadOnInit();

            subNav();
            UI.addClick('previous-btn', previous);
        },
        initGoogleAdWords: function() {
            UI.displayCart();
            ks.setType("google");
            ks.savePkg(ks.data);
            //console.log('loading Google');
            googleAdwordsLoadOnInit();

            let db = ks.readPkg();
            //ks.addToCart(db);
        }
    }
})(validation, KsController, KsUIController, $);

/* adding class in main menu */
$('.menu').click(function() {
    $(this).addClass('visible');
    $(this).removeClass('visible');
});

$('.menu-toggle').click(function(e) {
    $(this).parent('.menu').toggleClass('visible');
    e.stopPropagation();
});

//email model(example) shows in whole site
 $(".i-email-btn").click(function(){
    swal("test@gmail.com");

});


 function activatePlacesSearch() {
    var input = document.getElementById('target-city');
    var autocomplete = new google.maps.places.Autocomplete(input);
 }

document.write('<scr'+'ipt type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSycbdyBFnKsqOMriC0H9xY6A-JDTh85o&libraries=places&callback=activatePlacesSearch" ></scr'+'ipt>');
